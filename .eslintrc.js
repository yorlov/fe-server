module.exports = {
  root: true,

  env: {
    browser: true,
    es6: true,
    node: true,
  },

  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      modules: true,
      jsx: true,
    },
  },

  settings: {
    react: {
      version: 'detect',
    },
  },

  plugins: [
    'react',
    'jest',
    /* TODO: SPFE-895 Install a new version of "eslint-plugin-promise" and uncomment next line once https://github.com/xjamundx/eslint-plugin-promise/pull/219 is merged and released
    'promise',
    */
    'simple-import-sort',
    'sonarjs',
  ],
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:jest/recommended',
    /* TODO: SPFE-895 Install a new version of "eslint-plugin-promise" and uncomment next line once https://github.com/xjamundx/eslint-plugin-promise/pull/219 is merged and released
    'plugin:promise/recommended'
    */

    'plugin:sonarjs/recommended',
  ],

  overrides: [
    // TypeScript
    {
      files: ['**/*.ts'],
      parser: '@typescript-eslint/parser',
      plugins: ['@typescript-eslint'],

      extends: ['plugin:@typescript-eslint/eslint-recommended', 'plugin:@typescript-eslint/recommended'],
    },

    // TypeScript custom definitions
    {
      files: ['**/*.d.ts'],
      rules: {
        // The variables in definition files are never used...
        '@typescript-eslint/no-unused-vars': 'off',
      },
    },

    // Tests
    {
      files: ['**/*.test.ts', '**/*.test.js', '**/*.test.jsx'],
      env: {
        jest: true,
      },
    },
  ],

  rules: {
    // Sorting imports
    'simple-import-sort/imports': 'error',
    'simple-import-sort/exports': 'error',
  },
};
