module.exports = {
  '*.{ts,tsx,js,jsx}': 'eslint --cache --fix',
  '*.{ts,tsx,js,jsx,json,css,md,yml}': 'prettier --write',
};
