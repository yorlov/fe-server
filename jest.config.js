module.exports = {
  preset: 'ts-jest/presets/js-with-babel-esm', // https://kulshekhar.github.io/ts-jest/docs/getting-started/presets
  // projects: [] // We don't use "projects" since it slows downs the Jest and executes the same test multiple times
  testPathIgnorePatterns: ['/node_modules/', '/dist/', '/target/'],
  watchPlugins: ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname'],
  watchPathIgnorePatterns: ['/target/'],
  testTimeout: 45 * 1000, // 45 seconds timeout,
  clearMocks: true,
  silent: true, // Silent down the console.* output
  modulePathIgnorePatterns: ['/dist/'],
};
