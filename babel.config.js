// This file is required to collect coverage by Jest
module.exports = (api) => {
  /** this is just for minimal working purposes,
   * for testing larger applications it is
   * advisable to cache the transpiled modules in
   * node_modules/.bin/.cache/@babel/register* */
  api.cache(false);

  return {
    presets: [
      [
        '@babel/preset-env',
        {
          useBuiltIns: false,
          targets: {
            node: process.versions.node,
          },
        },
      ],
      '@babel/preset-react',
    ],
  };
};
