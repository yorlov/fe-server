const path = require('path');
const { stringifyRequest } = require('loader-utils');
const { escapeRegExp, generateUniqueName } = require('./util');

// Variables we want to avoid conflicting with when generating local variables
const KNOWN_SOY_GLOBALS = ['window', 'atl_soy', 'goog', 'soy', 'soydata', 'soyshim'];

// List of AUI namespaces is pulled from BBS usages and AUI docs.
// Probably missing some namespaces used in other products.
const DEFAULT_MAPPINGS = {
  aui: false,
  'aui.avatar': false,
  'aui.badges': false,
  'aui.buttons': false,
  'aui.dialog': false,
  'aui.dropdown2': false,
  'aui.expander': false,
  'aui.form': false,
  'aui.group': false,
  'aui.icons': false,
  'aui.inlineDialog2': false,
  'aui.labels': false,
  'aui.message': false,
  'aui.page': false,
  'aui.progressTracker': false,
  'aui.sidebar': false,
  'aui.table': false,
  'aui.toolbar2': false,
};

// Default path resolver should return undefined value
const defaultPathResolver = () => {};

/**
 * Return an array of objects that each describe a namespace and templates from it
 * that are depended on by this soy file.
 *
 * @param {Object} parsedSoy - the namespace of this file
 * @param {string} parsedSoy.namespace - the namespace of this file
 * @param {Object} parsedSoy.callsByNamespace - a map of namespace to { namespaceRoot: string, templates: Array<string> }
 *                                              E.g. if `name.space.tmpl` is called, then
 *                                              `{ 'name.space': { namespaceRoot: 'name', templates: ['tmpl'] }}`
 * @param {string} context - The directory of the soy file. Can be used as a context for resolving other stuff.
 * @param {Object} options
 * @param {Object} [options.customModuleMap] - map of namespace string to module string
 * @param {Function} [options.modulePathResolver] - function that maps namespace strings to module strings
 * @return {Array<{ namespace: string, namespaceRoot: string, templates: string[], module: string}>}
 */
function getNamespaceDependencies(parsedSoy, context, options) {
  const moduleMapper = makeModuleMapper(options.customModuleMap, options.modulePathResolver);

  const dependencies = [];

  for (const [namespace, call] of Object.entries(parsedSoy.callsByNamespace)) {
    // remove the local namespace
    if (namespace === parsedSoy.namespace) {
      continue;
    }

    const { namespaceRoot, templates } = call;
    const module = moduleMapper(namespace, context);

    dependencies.push({
      namespace,
      namespaceRoot,
      templates,
      module,
    });
  }

  return dependencies;
}

/**
 * Generate ES6 named import descriptions for each other Soy namespace.
 *
 * e.g. if we depend on nsOne.subNS.tmplA, nsOne.subNS.tmplB, and nsOne.tmplA, this would return:
 * ```
 * [
 *   {
 *     module: 'ns-one/sub-n-s.soy',
 *     namespace: 'nsOne.subNS',
 *     refs: [
 *       { name: 'tmplA', localName: 'tmplA' },
 *       { name: 'tmplB', localName: 'tmplB' },
 *     ]
 *   },
 *   {
 *     module: 'ns-one.soy',
 *     namespace: 'nsOne',
 *     refs: [
 *       { name: 'tmplA', localName: 'tmplA2' }, // note resolved conflict
 *     ]
 *   }
 * ]
 * ```
 *
 * which represents
 *
 * ```
 * import { tmplA, tmplB } from 'ns-one/sub-n-s.soy',
 * import { tmplA as templA2 } from 'ns-one.soy',
 * ```
 *
 * @param {Array<Object>} namespaceDependencies - output from getNamespaceDependencies.
 * @param {Set<string>} definedVariableSet - a Set of local variable names we shouldn't conflict with. WILL BE MUTATED.
 * @return {Array<{ namespace: string, module: string, refs: Array<{name: string, localName: string}> }>}
 */
function generateImports(namespaceDependencies, definedVariableSet) {
  // TODO: if two namespaces map to the same file (somebody doing something hacky),
  // the output JS will be malformed. Should instead combine those entries.
  // Cross that bridge when we get there.

  // TODO: Refactor to using for loops
  return namespaceDependencies
    .filter(({ module }) => module) // no module means leave it as a global - no import
    .map(({ namespace, module, templates }) => {
      // generate a local name for each imported template from this namespace.
      // E.g. to avoid naming conflicts on a variable name `template`, we might need to:
      // import { template as template2 } from 'name.space.template';
      // that is, generally
      // import { name as localName } from 'module';

      const exportsRefs = generateExports(templates, definedVariableSet);

      return {
        namespace,
        module,
        refs: exportsRefs,
      };
    })
    .filter(Boolean);
}

/**
 * Similar to asImports, but applied to the templates defined in this file.
 * Similarly attempts to map each template to a unique local name.
 * Returns an array of { name, localName } that represents
 * ```
 * const localName = (template function);
 * export { localName as name };
 * ```
 * @param {Set<string>} definitions - a collection of template names that are defined in this file
 * @param {Set<string>} definedVariableSet - a Set of local variable names we shouldn't conflict with. WILL BE MUTATED.
 * @returns {Array<{ name: string, localName: string }>}
 */
function generateExports(definitions, definedVariableSet) {
  const exports = [];

  for (const name of definitions) {
    const localName = generateUniqueName(name, definedVariableSet);

    definedVariableSet.add(localName);

    exports.push({ name, localName });
  }

  return exports;
}

/**
 * Default logic for module names from Soy namespaces
 * a.bCD becomes a/b-c-d.soy
 *
 * @param {string} ns - the namespace to be mapped
 */
function defaultModuleMapper(ns) {
  return ns.replace(/\.|[^.][A-Z]/g, (s) => (s[0] === '.' ? '/' : s[0] + '-' + s[1].toLowerCase())) + '.soy';
}

function isAbsolutePath(input) {
  return path.posix.isAbsolute(input) || path.win32.isAbsolute(input);
}

/**
 * First look for a mapping in options.customModuleMap,
 * then call options.modulePathResolver,
 * then fallback to the default module mapper.
 *
 * @param {Object} [customModuleMap]
 * @param {Function} [customModulePathResolver]
 * @returns {(modulePath: string, context: string) => (string|null)}
 */
function makeModuleMapper(customModuleMap, customModulePathResolver) {
  const modulesMappings = new Map(Object.entries({ ...DEFAULT_MAPPINGS, ...customModuleMap }));
  const pathResolver = customModulePathResolver || defaultPathResolver;

  /**
   * @param {string} namespace
   * @param {string} context
   */
  function moduleMapper(namespace, context) {
    let customModulePath = modulesMappings.has(namespace)
      ? modulesMappings.get(namespace)
      : pathResolver(namespace, context);

    if (customModulePath === false) {
      // ignored namespace
      return null;
    }

    // Fix module paths from absolute to relative
    if (customModulePath && isAbsolutePath(customModulePath)) {
      customModulePath = path.relative(context, customModulePath);

      // Makes the path relative when the resulted path is not really relative and points to the same directory
      if (!customModulePath.startsWith('.')) {
        customModulePath = `.${path.sep}${customModulePath}`;
      }
    }

    return customModulePath || defaultModuleMapper(namespace);
  }

  return moduleMapper;
}

// replace calls to template functions with calls to the imported variables
function replaceWithImports(soyJs, imports) {
  //replace longer namespaces first to avoid matching the wrong thing
  const importsByLength = imports.sort((a, b) => {
    return b.namespace.length - a.namespace.length;
  });

  // TODO: Refactor and convert to for loops
  return importsByLength.reduce((soyJs, imp) => {
    // TODO: Refactor and convert to for loops
    const variableLookups = imp.refs.reduce((variableLookups, ref) => {
      variableLookups[ref.name] = ref.localName;
      return variableLookups;
    }, {});

    const templateREs = imp.refs.map((ref) => escapeRegExp(ref.name));
    const fullRE = new RegExp(escapeRegExp(imp.namespace) + '\\.' + '(' + templateREs.join('|') + ')' + '\\b', 'g');

    return soyJs.replace(fullRE, (full, name) => variableLookups[name]);
  }, soyJs);
}

/**
 * Return the transformed Soy JS content with imports and exports defined and referenced
 *
 * @param {Object} options
 * @param {Array<{ namespace: string, module: string, refs: Array<{name: string, localName: string}> }>} options.imports
 * @param {Array<{ name: string, localName: string }>} options.exports
 * @param {string} options.namespace
 * @param {string} options.soyJs
 * @param {string} options.context
 * @return {string}
 */
function renderSoyJs({ imports, exports, namespace, soyJs, context }) {
  // Create a webpack loader-like context we can use with loader-utils
  const loaderContext = /** @type {import("webpack").loader.LoaderContext} */ {
    context,
  };

  const importString = imports
    .map((importMeta) => {
      const refStrings = importMeta.refs.map((ref) => {
        if (ref.name !== ref.localName) {
          return ref.name + ' as ' + ref.localName;
        }

        return ref.name;
      });

      return `import { ${refStrings.join(', ')} } from ${stringifyRequest(loaderContext, importMeta.module)};\n`;
    })
    .join('');

  const body = replaceWithImports(soyJs, imports);

  const exportString =
    exports.length &&
    exports.map((e) => `const ${e.localName} =  ${namespace}.${e.name};\n`).join('') +
      `export { ${exports
        .map((e) => (e.localName === e.name ? e.name : e.localName + ' as ' + e.name))
        .join(', ')} };\n`;

  return [importString, body, exportString].filter(Boolean).join('\n');
}

/**
 * Turns {call namespace.template} into `import { template } from 'namespace.soy';` and
 * turns {template .myTemplate} into `export const myTemplate = my.name.space.myTemplate;`
 *
 * By default namespaces match 1-to-1 with Soy file names.
 * camelCase becomes kebab-case and dot separators become directories.
 * So a.bCD becomes a/b-c-d.soy
 *
 * options.customModuleMap - an object mapping namespaces to module names.
 *     Explicit `false` ignores a module and leaves it as a global reference. Other falsy values fallback
 *         to default handling.
 * options.modulePathResolver - a function that takes a namespace and outputs a module name.
 *     Returning an explicit `false` ignores a module and leaves it as a global reference. Return undefined or null
 *        to fallback to default handling.
 * @param {Object} params
 * @param {Object} params.parsedSoy - see parse-soy.js . A description of the templates and namespaces defined and referenced in this file.
 * @param {string} params.soyJs - the compiled JS version of the Soy file. This will be the base that we modularize
 * @param {string} params.context - The directory of the soy file. Can be used as a context for resolving other stuff.
 * @param {Object} params.options - see README.md
 * @param {Object} params.options.customModuleMap - map of namespace string to module string
 * @param {Function} params.options.modulePathResolver - function that maps namespace strings to module strings
 */
function modularizeSoy({ parsedSoy, soyJs, context, options }) {
  const namespaceDependencies = getNamespaceDependencies(parsedSoy, context, options);

  // if a namespace is not transformed, then its root variable remains as a global after the transform and could conflict
  // with any variable names we try to generate
  const ignoredDependencyRoots = namespaceDependencies
    .filter((nsDescriptor) => !nsDescriptor.module)
    .map((nsDescriptor) => nsDescriptor.namespaceRoot);

  const definedVariableSet = new Set(KNOWN_SOY_GLOBALS.concat(parsedSoy.namespaceRoot, ignoredDependencyRoots));

  const exports = generateExports(parsedSoy.definitions, definedVariableSet);
  const imports = generateImports(namespaceDependencies, definedVariableSet);

  return renderSoyJs({
    imports,
    exports,
    namespace: parsedSoy.namespace,
    soyJs,
    context,
  });
}

module.exports = modularizeSoy;
