const { bundleEntryPoint } = require('../helpers');
const path = require('path');
const getWebpackConfig = require('./webpack.config');

const testSuiteDir = path.basename(__dirname);

describe('modularize', () => {
  it('should auto-import the transitive templates we are calling from the soy template', async () => {
    // given
    const { modulePath } = await bundleEntryPoint(getWebpackConfig(), {
      entrypointPath: './src/transitive-imports.js',
      testSuiteDir,
    });
    const { template } = require(modulePath);

    // when
    const result = template({});

    const match = result.match(/<p>transitive call worked<\/p>/g);

    // 5 calls in total
    expect(Array.from(match)).toEqual([
      // eslint-disable-next-line sonarjs/no-duplicate-string
      '<p>transitive call worked</p>',
      '<p>transitive call worked</p>',
      '<p>transitive call worked</p>',
      '<p>transitive call worked</p>',
      '<p>transitive call worked</p>',
    ]);
  });

  it('should allow to call JS modules configured with the `customModuleMap` option', async () => {
    // given
    const { modulePath } = await bundleEntryPoint(getWebpackConfig(), {
      entrypointPath: './src/custom-module-map-option.js',
      testSuiteDir,
    });
    const { template } = require(modulePath);

    // when
    const result = template({});

    // then
    expect(result).toContain('custom mapping call worked');
    expect(result).toContain('custom mapping global call worked');
  });

  it('should allow to call JS modules configured with the `modulePathResolver` option', async () => {
    // given
    const { modulePath } = await bundleEntryPoint(getWebpackConfig(), {
      entrypointPath: './src/module-path-resolver-option.js',
      testSuiteDir,
    });
    const { template } = require(modulePath);

    // when
    const result = template({});

    // then
    expect(result).toContain('module path call worked');
    expect(result).toContain('module path global call worked');
  });

  it('should allow to call JS modules that are absolute paths and are configured with the `customModuleMap` option', async () => {
    // given
    const webpackConfig = getWebpackConfig();
    webpackConfig.module.rules[0].use[0].options.customModuleMap = {
      'app1.myCustomModule': path.resolve(__dirname, './src/app1/my-custom-module.soy'),
      'app1.templateHelper': path.resolve(__dirname, './src/app1/template-helper.soy'),
    };

    const { modulePath } = await bundleEntryPoint(webpackConfig, {
      entrypointPath: './src/custom-module-map-with-absolute-paths.js',
      testSuiteDir,
    });
    const { template } = require(modulePath);

    // when
    const result = template({});

    // then
    expect(result).toContain('custom mapping call with absolute module path worked');
    expect(result).toContain('importing a template relative to a current template worked');
    expect(result).toContain('soy template rendered');
  });

  it('should allow to call JS modules that are absolute paths and are configured with the `modulePathResolver` option', async () => {
    // given
    const webpackConfig = getWebpackConfig();
    webpackConfig.module.rules[0].use[0].options.customModuleMap = {};
    webpackConfig.module.rules[0].use[0].options.modulePathResolver = (namespace) => {
      if (namespace === 'fooNamespace.barModule') {
        return path.resolve(__dirname, './src/foo/bar-module.soy');
      }

      return null;
    };

    const { modulePath } = await bundleEntryPoint(webpackConfig, {
      entrypointPath: './src/module-map-resolver-with-with-absolute-paths.js',
      testSuiteDir,
    });
    const { template } = require(modulePath);

    // when
    const result = template({});

    // then
    expect(result).toContain('custom path resolution call with absolute module path worked');
    expect(result).toContain('soy template rendered');
  });

  describe('deprecations', () => {
    const getBundlingWarnings = (stats) =>
      stats.toJson().warnings.map((warning) => {
        // In webpack 4 errors and warnings are only strings but in webpack 5 it's an object with a "details" property
        return typeof warning === 'string' ? warning : warning.details;
      });

    it('should not emmit any webpack warnings when not using the "modularize" option', async () => {
      // given
      const webpackConfig = getWebpackConfig();
      delete webpackConfig.module.rules[0].use[0].options.modularize;

      // when
      const { stats } = await bundleEntryPoint(webpackConfig, {
        entrypointPath: './src/deprecations/module.js',
        testSuiteDir,
      });

      // then
      expect(getBundlingWarnings(stats)).not.toContainEqual(
        expect.stringContaining('The "modularize" option is deprecated and will be removed in the next major release.'),
      );
    });

    it('should emmit a webpack warning when using the "modularize" option', async () => {
      // given, when
      const { stats } = await bundleEntryPoint(getWebpackConfig(), {
        entrypointPath: './src/deprecations/module.js',
        testSuiteDir,
      });

      // then
      expect(getBundlingWarnings(stats)).toContainEqual(
        expect.stringContaining('The "modularize" option is deprecated and will be removed in the next major release.'),
      );
    });
  });
});
