// Mimic the Soy runtime env in Node
global.goog = { DEBUG: false };
global.soy = { $$escapeHtml: (a) => a };
global.window = global;

// create globals that were explicitly left by the webpack config
global.test = {
  module: {
    path: {
      global: {
        tmpl: () => 'module path global call worked\n',
      },
    },
  },

  custom: {
    mapping: {
      global: {
        tmpl: () => 'custom mapping global call worked\n',
      },
    },
  },
};
