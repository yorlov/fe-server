const webpack = require('webpack');
const { name: packageName } = require('../package.json');

console.log(`[${packageName}]: Running Jest tests with webpack version: ${webpack.version}`);
