const { bundleEntryPoint } = require('../helpers');
const path = require('path');
const { promises: fs } = require('fs');

const testSuiteDir = path.basename(__dirname);
const getWebpackConfig = require('./webpack.config');

describe('soy-loader integration', () => {
  afterEach(() => {
    delete window.myNamespace;
  });

  it('should render the simple soy template', async () => {
    // given
    const { modulePath } = await bundleEntryPoint(getWebpackConfig(), {
      entrypointPath: './src/simple/module.js',
      testSuiteDir,
    });
    const { template } = require(modulePath);

    // when
    const result = template({
      name: 'Mike',
    });

    // then
    expect(result).toEqual(`<div>Hello Mike</div>`);
  });

  it('should expose namespace to window', async () => {
    // given
    const { modulePath } = await bundleEntryPoint(getWebpackConfig(), {
      entrypointPath: './src/simple/module.js',
      testSuiteDir,
    });
    require(modulePath);

    // then
    expect(window.myNamespace).toBeTruthy();
    expect(await fs.readFile(require.resolve(modulePath), 'utf-8')).toContain(
      `var myNamespace = window['myNamespace'] = window['myNamespace'] || {};`,
    );
  });

  it('should allow multiple templates with the same top namespace without overwriting them', async () => {
    // given
    const { modulePath } = await bundleEntryPoint(getWebpackConfig(), {
      entrypointPath: './src/multiple/module.js',
      testSuiteDir,
    });
    require(modulePath);

    // then
    expect(window.myNamespace).toBeTruthy();
    expect(window.myNamespace.templateA).toEqual(expect.any(Function));
    expect(window.myNamespace.templateB).toEqual(expect.any(Function));
  });

  const getBundlingErrorsAndWarnings = (stats) => {
    const { errors, warnings } = stats.toJson();

    const entries = [
      ['errors', errors],
      ['warnings', warnings],
    ].map(([key, value]) => [
      key,
      value.map((warning) => {
        // In webpack 4 errors and warnings are only strings but in webpack 5 it's an object with a "details" property
        return typeof warning === 'string' ? warning : warning.details;
      }),
    ]);

    return Object.fromEntries(entries);
  };

  it('should accept the "functions" options and not throw any errors', async () => {
    // given
    const webpackConfig = getWebpackConfig();
    webpackConfig.module.rules[0].use[0].options = {
      functions: path.resolve(__dirname, './src/customFunctions/my directory with spaces/soy-functions.jar'),
    };

    // when
    try {
      const { stats } = await bundleEntryPoint(webpackConfig, {
        entrypointPath: './src/customFunctions/module.js',
        testSuiteDir,
      });

      const { errors, warnings } = getBundlingErrorsAndWarnings(stats);

      expect(errors).toHaveLength(0);
      expect(warnings).toHaveLength(0);
    } catch (e) {
      // This shouldn't happen
      console.error(e);

      // eslint-disable-next-line jest/no-conditional-expect
      expect(false).toBeTruthy();
    }
  });
});
