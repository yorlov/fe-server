const path = require('path');
const webpack = require('webpack');

const ROOT = path.resolve(__dirname);
const SRC = path.join(ROOT, './src');
const DIST = path.join(ROOT, './dist');

const soyLoader = path.resolve(ROOT, '../../index');

module.exports = () => ({
  devtool: false,
  mode: 'development',
  entry: {},

  output: {
    path: DIST,
    filename: '[name].js',
    libraryTarget: 'commonjs2',
  },

  resolve: {
    modules: [SRC],
  },

  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: false, // Change me to enable debug mode
    }),
  ],

  module: {
    rules: [
      {
        test: /\.soy/,
        include: [SRC],
        use: [
          {
            loader: soyLoader,
          },
        ],
      },
    ],
  },
});
