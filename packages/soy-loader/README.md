# @atlassian/soy-loader

![node version](https://img.shields.io/node/v/@atlassian/soy-loader.svg)
![webpack peer dependency version](https://img.shields.io/npm/dependency-version/@atlassian/soy-loader/peer/webpack.svg)
![npm downloads](https://img.shields.io/npm/dt/@atlassian/soy-loader.svg)

Compiles [Soy Templates](https://developer.atlassian.com/confdev/tutorials/writing-soy-templates-in-your-plugin) templates with [@atlassian/soy-template-plugin-js](https://www.npmjs.com/package/@atlassian/soy-template-plugin-js) package and allows to load them with [webpack](https://webpack.js.org//)

## Installation

You can install the library using [**NPM**](https://www.npmjs.com):

```bash
npm install @atlassian/soy-loader
```

or by [**Yarn**](https://yarnpkg.com/):

```bash
yarn add @atlassian/soy-loader
```

## Usage example

```js
// webpack.config.js

module.exports = {
  module: {
    rules: [
      {
        test: /\.soy/,
        use: [
          {
            loader: '@atlassian/soy-loader',
            options: {
              // ... see Options below ...
            },
          },
        ],
      },
    ],
  },
};
```

### Options

#### `data` `<string`> (optional)

Example: `'foo:bar,moo:goo'`

Data to pass to the soy renderer (soy rendering only) in the form <key1>:<value1>,<key2>:<value2>

#### `functions` `<string>` (optional)

Example: `'/foo/bar/path'`

Locations of custom soy functions, usually a path to a `*.jar` file.

#### `expose` `<boolean>` (optional)

Default: `true`

Indicates whether the generated namespace should be exposed to the window or not. If not, the module must be required to call the templates.

#### `modularize` `<boolean>` (optional)

Default: `false`

Indicates whether to convert template calls and definitions to ES6 module imports and exports. Allows Webpack to track the chain of template calls, but assumes a naming convention that maps namespace strings to file paths. See [#Using-modularize](Using modularize) for details.

#### `customModuleMap` `Map<string, string | boolean>` (optional)

Example: `{ 'foo.bar.Template': 'foo/baz', 'moo.foo': false }`

To be used with the `modularize` option. An object mapping namespaces to module names when using modularize. Explicit `false` ignores a module and leaves it as a global reference. Other falsy values fallback to default resolution handling.

#### `modulePathResolver` : `<Function>` (optional)

Example:

```js
function customModulePathResolver(namespace, context) {
  return namespace.replace(/\./g, '/');
}

function customModuleRelativePathResolver(namespace, context) {
  return path.relative(context, namespace.replace(/\./g, '/'));
}
```

To be used with the `modularize` option. A function that takes a `namespace` [`string`], `context` [`string`], and outputs a module path when using modularize.
You can read more about the value of `context` here: [https://webpack.js.org/api/loaders/#thiscontext](https://webpack.js.org/api/loaders/#thiscontext)

Returning an explicit `false` ignores a module and leaves it as a global reference. Return other falsy or `undefined` values to fallback to default resolution handling.

## Recipes

### Using with `*.properties` webpack loader

This package can be used with the [`@atlassian/i18n-properties-loader`](https://www.npmjs.com/package/@atlassian/i18n-properties-loader)
when you run your code with webpack dev server:

```js
// webpack.config.js
const myI18nFiles = [
  'foo/i18n/my-translation-file.properties',
  'foo/bar/i18n/my-other-translation-file.properties',
  'bar/i18n/some-translation-file.properties',
];

module.exports = {
  module: {
    rules: [
      {
        test: /\.soy/,
        use: [
          {
            loader: '@atlassian/i18n-properties-loader',
            options: {
              i18nFiles: myI18nFiles,
            },
          },

          {
            loader: '@atlassian/soy-loader',
          },
        ],
      },
    ],
  },
};
```

### Using `modularize`

<a name="using-modularize"></a>

Modularize is a specialized case that can be used when your {namespace}s and Soy files are 1-to-1. It converts any external templates references to ES6 module imports (and conversely any defined templates become named exports from this module). This allows Webpack to trace dependencies between Soy files, rather than having to specify this manually.

By default, the `camelCase` namespace becomes the `kebab-case`, dot separators become directories, and `.soy` suffix is appended.

**Example**: The `{namespace a.bCD}` namespace is converted to `import 'a/b-c-d.soy';` module path.

This can be overridden using the `customModuleMap` option for one-off exceptions, and/or the `modulePathResolver` function to change the naming scheme entirely. NOTE: the modules MUST be unique per namespace.

Currently, the AUI-provided namespaces are ignored and left as global variable references, and thus dependencies on AUI can't be tracked. If you want to handle this differently, check out the `DEFAULT_MAPPINGS` in the `modular-soy.js` file, and replace each `false` with a module name.

## Additional links

- [https://www.npmjs.com/package/@atlassian/i18n-properties-loader](https://www.npmjs.com/package/@atlassian/i18n-properties-loader)
- [https://www.npmjs.com/package/@atlassian/wrm-react-i18n](https://www.npmjs.com/package/@atlassian/wrm-react-i18n)
- [https://www.npmjs.com/package/atlassian-webresource-webpack-plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin)

## Minimum requirements

This plugin is compatible with:

- webpack 4.0+ and 5.0+
- Node 12+
