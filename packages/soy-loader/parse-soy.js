function getDefinedTemplates(soy) {
  const definitions = soy.match(/\{template\s+[^\s/}]+/g) || [];
  return new Set(definitions.map((s) => s.substring(9).trim().substring(1)));
}

function getCalledTemplates(soy) {
  const calls = soy.match(/\{call\s+[^\s/}]+/g) || [];
  return new Set(calls.map((s) => s.substring(5).trim()));
}

// given a dot delimited string, return the first part. e.g. 'aui.form.checkboxField' => 'aui'
function rootVar(namespace) {
  return namespace.substring(0, namespace.indexOf('.')) || namespace;
}

function parseSoy(soy, options) {
  const namespace = soy
    .match(/\{namespace\s+[^\s/}]+/g)[0]
    .substring(10)
    .trim();
  const namespaceRoot = namespace.substring(0, namespace.indexOf('.')) || namespace;

  if (options && options.namespaceOnly) {
    return {
      namespace,
      namespaceRoot,
    };
  }

  const calls = getCalledTemplates(soy);
  const callsByNamespace = Array.from(calls).reduce((namespaces, tmpl) => {
    const nameI = tmpl.lastIndexOf('.');
    const ns = nameI === 0 ? namespace : tmpl.substring(0, nameI);
    const localName = tmpl.substring(nameI + 1);
    if (!namespaces[ns]) {
      namespaces[ns] = {
        namespaceRoot: rootVar(ns),
        templates: new Set(),
      };
    }
    namespaces[ns].templates.add(localName);
    return namespaces;
  }, {});

  return {
    namespace,
    namespaceRoot,
    calls,
    callsByNamespace,
    definitions: getDefinedTemplates(soy),
  };
}

module.exports = parseSoy;
