/**
 * Escape a string so it can be used as a literal match in a regex pattern.
 */
module.exports.escapeRegExp = function escapeRegExp(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
};

/**
 * Appends a number to the variable name, if necessary, to avoid conflicting names
 *
 * @param {String} name - the name to unique-ify
 * @param {Set<String>} takenNames - names that we might conflict with
 * @return {String} a generated name that will not conflict with any of the takenNames
 */
module.exports.generateUniqueName = function generateUniqueName(name, takenNames) {
  let localName = name;
  for (let i = 2; takenNames.has(localName); i++) {
    localName = name + i;
  }
  return localName;
};
