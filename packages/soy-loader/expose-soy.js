const exposeSoy = (parsedSoy, soyJs) => {
  // As we wrap the soy template in a module now a simple `var xyz` doesnt translate to a global variable anymore.
  // Therefore, we need to rewrite the template to explicitly expose the namespace to windows.
  return `
  var ${parsedSoy.namespaceRoot} = window['${parsedSoy.namespaceRoot}'] = window['${parsedSoy.namespaceRoot}'] || {};
  ${soyJs}`;
};

module.exports = exposeSoy;
