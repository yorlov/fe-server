# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### 5.3.6 (2022-02-10)

### Bug Fixes

- [SPFE-894](https://ecosystem.atlassian.net/browse/SPFE-894) refactor code and fix some ESLint Sonar rules ([fefec4a](https://bitbucket.org/atlassianlabs/fe-server/commits/fefec4a3b5e94347cd935cafffbaddfc4c8605d6))

### [5.3.5](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.3.5..@atlassian/soy-loader@5.3.4) (2021-11-17)

### Bug Fixes

- Update webpack depndency ([00fc73e](https://bitbucket.org/atlassianlabs/fe-server/commits/00fc73e95ad4a2f89ee4fd3561ab3fb60d1b9516))

### [5.3.4](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.3.4..@atlassian/soy-loader@5.3.3) (2021-10-26)

**Note:** Version bump only for package @atlassian/soy-loader

### [5.3.3](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.3.3..@atlassian/soy-loader@5.3.2) (2021-10-22)

### Bug Fixes

- **ci:** update webpack versions when running integration tests ([009dd61](https://bitbucket.org/atlassianlabs/fe-server/commits/009dd61f36a784d3fef7b6d6fa7214c5cc16b8b8))
- **soy-loader:** SOY-145 - ensure not to overwrite a potentially already existing namespace ([087dfd0](https://bitbucket.org/atlassianlabs/fe-server/commits/087dfd0277a3b70af46bf1bde288f8faccc931e1))

### [5.3.2](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.3.2..@atlassian/soy-loader@5.3.1) (2021-10-14)

### Bug Fixes

- **soy-loader:** SOY-145 - properly expose soy template to global namespace ([050f0c7](https://bitbucket.org/atlassianlabs/fe-server/commits/050f0c77a9de8d7e18d76e28b4ee422458c29961))

### [5.3.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.3.1..@atlassian/soy-loader@5.3.0) (2021-10-08)

**Note:** Version bump only for package @atlassian/soy-loader

## [5.3.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.3.0..@atlassian/soy-loader@5.2.1) (2021-09-28)

### Features

- **soy-loader:** update version of soy-template-plugin dependency ([1a6933c](https://bitbucket.org/atlassianlabs/fe-server/commits/1a6933c78fba5225fe7eafea19925974c3cbc6c9))
- **soy-template-plugin-js:** update version of the Maven soy dependencies ([59db2a6](https://bitbucket.org/atlassianlabs/fe-server/commits/59db2a6df50b6b9235294bcc81e79c56248df619))

### [5.2.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.2.1..@atlassian/soy-loader@5.3.0) (2021-09-27)

### Bug Fixes

- **soy-loader:** Fix broken release of soy-template-plugin-js package ([fa70460](https://bitbucket.org/atlassianlabs/fe-server/commits/fa70460778d57e41d658c6af12bb1b53fed94880))

## [5.3.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.3.0..@atlassian/soy-loader@5.2.0) (2021-09-24)

### Features

- Use new location of the jar file in soy-template-plugin-js ([2b2ce1f](https://bitbucket.org/atlassianlabs/fe-server/commits/2b2ce1fa46bc60b812aeb95923607ea70f71c921))

## [5.2.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.2.0..@atlassian/soy-loader@5.1.1) (2021-09-22)

- Update internal dependencies

### Features

- **soy-loader:** deprecate the "modularize" option ([047f393](https://bitbucket.org/atlassianlabs/fe-server/commits/047f3931785fce252d6722a259cadacecac747f6))
- [SPFE-774](https://ecosystem.atlassian.net/browse/SPFE-774) validate soy-loader options ([0eb05dc](https://bitbucket.org/atlassianlabs/fe-server/commits/0eb05dcbb4b99af3a9cec92c76d665ff17c6dd3b))

### Bug Fixes

- **soy-loader:** Issue #10 Fix the issues with wrong paths on Windows OS ([024663a](https://bitbucket.org/atlassianlabs/fe-server/commits/024663a14303a2e4193c215455337163bbb862fc))

## [5.1.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.1.1..@atlassian/soy-loader@5.1.0) (2021-08-30)

### Bug Fixes

- **soy-loader:** [SPFE-713](https://ecosystem.atlassian.net/browse/SPFE-713) fix usage of loader's context when using the `modularize` option ([ed38bb7](https://bitbucket.org/atlassianlabs/fe-server/commits/ed38bb75f8e04c446912e1584ace75942727314a))

## [5.1.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.1.0..@atlassian/soy-loader@5.0.0) (2021-08-26)

**Note:** We updated internal dependencies

### Features

- **soy-loader:** [Issue #9](https://bitbucket.org/atlassianlabs/fe-server/issues/9/atlassian-soy-loader-custommodulemap): Improve handling absolute path when using "modularize" option ([1257859](https://bitbucket.org/atlassianlabs/fe-server/commits/1257859aec7eb855624a8a35dc28a9a64c379ce3))

### Bug Fixes

- **soy-loader:** Fix stringifying the module imports ([0e658d1](https://bitbucket.org/atlassianlabs/fe-server/commits/0e658d1a778149fee7afc67cdd7668bdda9d6427))

## [5.0.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@5.0.0..@atlassian/soy-loader@4.3.1) (2021-07-01)

### ⚠ BREAKING CHANGES

- We removed support for Node 10. The minimum required version is Node 12 now.

### Features

- drop support for Node 10 ([2ef1e83](https://bitbucket.org/atlassianlabs/fe-server/commits/2ef1e831cc30f0ec78884d72815d773ae401cc7e))

### [4.3.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-loader@4.3.1..@atlassian/soy-loader@4.3.0) (2021-03-08)

### Bug Fixes

- **deps:** remove webpack cli dependency ([c957fb3](https://bitbucket.org/atlassianlabs/fe-server/commits/c957fb3741348478c885d4af443b14717ea02c95))
- **deps:** Update Node version ([1dadcca](https://bitbucket.org/atlassianlabs/fe-server/commits/1dadcca42ac461410dabd8e844249cdf80e516a9))

## 4.3.0 (2021-01-18)

**Note:** Version bump only for package @atlassian/soy-loader

## 4.3.0-alpha.0 (2021-01-14)

- chore: add pipelines cacheing ([132ca60](https://bitbucket.org/atlassianlabs/fe-server/commits/132ca60))
- feat: SPFE-275 Add support for webpack 5 to soy-loader ([edc7190](https://bitbucket.org/atlassianlabs/fe-server/commits/edc7190))

## 4.2.0 (2020-06-25)

- chore: update dependencies ([38f5a33](https://bitbucket.org/atlassianlabs/fe-server/commits/38f5a33))
- chore: update dependencies ([c3d3caa](https://bitbucket.org/atlassianlabs/fe-server/commits/c3d3caa))
- chore: migrate soy-loader tests to jest ([148dbef](https://bitbucket.org/atlassianlabs/fe-server/commits/148dbef))
- chore: split the integration test cases of soy-loader ([da85f87](https://bitbucket.org/atlassianlabs/fe-server/commits/da85f87))

## 4.1.0 (2020-05-11)

- chore: Add note about minimal requirements ([ee38cec](https://bitbucket.org/atlassianlabs/fe-server/commits/ee38cec))
- chore: Add prettier ([cf522c9](https://bitbucket.org/atlassianlabs/fe-server/commits/cf522c9))
- chore: downgrade to Node version 10 ([9fc7bab](https://bitbucket.org/atlassianlabs/fe-server/commits/9fc7bab))
- chore: Update minimal Node version to version 12 so we can support 'String.proptotype.matchall' ([cb0ed25](https://bitbucket.org/atlassianlabs/fe-server/commits/cb0ed25))
- chore(pipelines): Add pipelines script ([0ebd337](https://bitbucket.org/atlassianlabs/fe-server/commits/0ebd337))
- chore(tests): Fix running unit tests ([2bc7913](https://bitbucket.org/atlassianlabs/fe-server/commits/2bc7913))

# Changelog

## 4.0.0

- **[BREAKING]** The package was renamed from `@atlassian/atlassian-soy-loader` to `@atlassian/soy-loader`,

- **[BREAKING]** We are removing the `i18n` and `i18nBaseDir` options. The webpack loader shouldn't transform the usage of
  `getText('my.translation.key')` into translations when we are converting the SOY syntax into JS file. Instead, we should
  let the WRM transform keys into translations inside JS files with a help of `jsI18n` WR transformer during the product
  runtime.
  For more information on how to implement translations in Atlassian Server product with the help of webpack check the:
  - [Atlassian Web-Resource Webpack Plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin) and [`transoformationMap`](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin#transformationmap-optional) option
  - [@atlassian/i18n-properties-loader](https://www.npmjs.com/package/@atlassian/i18n-properties-loader)

## 3.0.0

- Added the `modularize`, `customModuleMap` and `modulePathResolver` options. Changed the `dontExpose` option to `expose`, but with the same default behavior.
- Updated dependency to Webpack 4. Webpack 3 might still work, but wasn't tested.
- Tested against Node v8.9.4 (but locally works against v10.10.0 too)
