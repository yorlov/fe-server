const { bundleEntryPoint } = require('./helpers');

describe('unescape messages', () => {
  it("should unescape double single-quotes ('') into single-quotes (')", async () => {
    const { modulePath } = await bundleEntryPoint('./src/unescape-single-quotes.js');
    const { getString, getStringWithParams } = require(modulePath);

    expect(getString()).toEqual("What's up?");
    expect(getStringWithParams('sun')).toEqual("I like 'sun' very much.");
  });

  it('should unescape colon', async () => {
    const { modulePath } = await bundleEntryPoint('./src/unescape-colon.js');
    const { getString } = require(modulePath);

    expect(getString()).toEqual('The answer is: 42');
  });

  it('should unescape backslash', async () => {
    const { modulePath } = await bundleEntryPoint('./src/unescape-backslash.js');
    const { getString } = require(modulePath);

    expect(getString()).toEqual(`\\Hi!`);
  });

  it('should unescape equals character', async () => {
    const { modulePath } = await bundleEntryPoint('./src/unescape-equals.js');
    const { getString, getUnicodeString } = require(modulePath);

    expect(getString()).toEqual(`1+2=3`);
    expect(getUnicodeString()).toEqual(`2+3=5`);
  });
});
