const { bundleEntryPoint } = require('./helpers');

describe('unicode', () => {
  it('should read the unicode chars', async () => {
    const { modulePath } = await bundleEntryPoint('./src/unicode.js');
    const { getString } = require(modulePath);

    expect(getString()).toEqual('more\u2026');
  });
});
