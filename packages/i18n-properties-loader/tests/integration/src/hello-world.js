import * as formatter from 'wrm/i18n';

export const getHelloWorld = () => formatter.I18n.getText('hello.world');
