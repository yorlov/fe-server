import { I18n } from 'wrm/i18n';

export const getString = () => I18n.getText('quotation.quoted.values');
