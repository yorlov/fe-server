import * as formatter from 'wrm/i18n';

export const getString = () => formatter.I18n.getText('unescape.backslash');
