import { I18n } from '@atlassian/wrm-react-i18n';

export const getString = () => I18n.getText('string.with.unicode');
