import * as formatter from 'wrm/i18n';

export const getHelloWorldWithParam = (myParam) => formatter.I18n.getText('hello.world.with.param', myParam);
