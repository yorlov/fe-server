import { I18n } from 'wrm/i18n';

export const getStringOne = () => I18n.getText('quotation.single.quote.one');

export const getStringTwo = () => I18n.getText('quotation.single.quote.two');
