import * as formatter from 'wrm/i18n';

export const getString = () => formatter.I18n.getText('undefined.key');

export const getStringWithParams = (param1, param2) =>
  formatter.I18n.getText('undefined.key.with.params', param1, param2);
