const { bundleEntryPoint } = require('./helpers');

describe('key with a spaces', () => {
  it('should return value', async () => {
    const { modulePath } = await bundleEntryPoint('./src/key-with-spaces.js');
    const { getString } = require(modulePath);

    expect(getString()).toEqual('Foo Bar');
  });
});
