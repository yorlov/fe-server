const format = require('./wrm-format-mock');

const I18n = {
  getText: function (key) {
    console.warn(
      'Call to "getText" function was not replaced with either raw translation or a call to the "format" function. Have you included the "jsI18n" transformation in your webresource configuration?',
    );

    if (arguments.length > 1) {
      return I18n.format.apply(null, arguments);
    }

    return key;
  },
};

module.exports = {
  I18n,
  format,
};
