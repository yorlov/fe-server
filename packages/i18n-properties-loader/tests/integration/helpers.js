const webpack = require('webpack');
const path = require('path');

const runWebpack = (config) => {
  const compiler = webpack(config);

  return new Promise((resolve, reject) => {
    compiler.run((err, stats) => {
      if (err) {
        reject(err);
        return;
      }

      if (stats.hasErrors()) {
        reject(new Error(stats.toJson().errors));
      }

      resolve(stats);
    });
  });
};

/**
 * Bundles given module and return bundled path and bundling stats
 *
 * @param {string} entrypointPath - a path to the entrypoint JS module (with extension)
 * @returns {Promise<{ modulePath: string, stats: Object }>} - Returns a relative path to a bundled module (without the JS extension) and
 *                                                             webpack bundling stats object
 */
const bundleEntryPoint = async (entrypointPath) => {
  const entryPointName = Date.now().toString(32);

  const webpackConfig = require('./src/webpack.config');
  webpackConfig.entry[entryPointName] = path.join(__dirname, entrypointPath);

  const stats = await runWebpack(webpackConfig);
  const bundledModulePath = path.join('dist', entryPointName);

  return {
    stats,
    modulePath: `./${bundledModulePath}`,
  };
};

module.exports = {
  bundleEntryPoint,
};
