const { bundleEntryPoint } = require('./helpers');

describe('undefined keys', () => {
  it('should return translation key when value is not defined', async () => {
    const { modulePath } = await bundleEntryPoint('./src/undefined-keys.js');
    const { getString, getStringWithParams } = require(modulePath);

    expect(getString()).toEqual('undefined.key');
    expect(getStringWithParams()).toEqual('undefined.key.with.params');
  });
});
