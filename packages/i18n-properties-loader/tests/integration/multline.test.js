const { bundleEntryPoint } = require('./helpers');

describe('multiline string', () => {
  it('should return transform multiline message', async () => {
    const { modulePath } = await bundleEntryPoint('./src/multiline.js');
    const { getMultilineString, getSingleLineString } = require(modulePath);

    expect(getMultilineString()).toEqual('I have a new line and even a multi line');

    expect(getSingleLineString()).toEqual("I'm single line");
  });
});
