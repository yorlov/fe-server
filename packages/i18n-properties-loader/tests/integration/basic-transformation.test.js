const { bundleEntryPoint } = require('./helpers');

describe('basic transformation', () => {
  it('should transform the "I18n.getText" into "Hello World" string', async () => {
    const { modulePath } = await bundleEntryPoint('./src/hello-world.js');
    const { getHelloWorld } = require(modulePath);

    expect(getHelloWorld()).toEqual('Hello World!');
  });

  it('should transform the "I18n.getText" with params', async () => {
    const { modulePath } = await bundleEntryPoint('./src/hello-world-with-param.js');
    const { getHelloWorldWithParam } = require(modulePath);

    expect(getHelloWorldWithParam('Maciej')).toEqual('Hello Maciej!');
  });
});
