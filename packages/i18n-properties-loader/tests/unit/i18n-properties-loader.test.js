const { _i18nRegExp: i18nRegExp, _namedEsmImportRegexp: namedEsmImportRegexp } = require('../../index');

describe('RegExp', () => {
  test.each([
    'var label = WRM.I18n.getText("foo");',
    'var label = WRM.I18n.getText("foo.bar");',
    'var label = WRM.I18n.getText("blah");',
    'var label = WRM.I18n.getText("blah");',
    'var label = WRM.I18n.getText("blah", yada);',
    "var label = WRM.I18n.getText('blah');",
    'var label = AJS.I18n.getText("blah");',
    'var label = AJS.I18n.getText("foo-bar");',
    "var label = AJS.I18n.getText( 'blah' );",
    'var label = WRM.I18n.getText("foo.bar", results, total);',
    "var label = WRM.I18n.getText('drink');",
    'var label = a.I18n.getText("foo");',
    'var label = HiMum.I18n.getText("foo", results, total);',
    'var label = _AJS["default"].I18n.getText("foo");',
    'var label = _AJS["default"].I18n.getText("foo", results, total);',
    "var label = ANY_MODULE['I18n'].getText('foo');",
    'var label = ANY_MODULE["I18n"].getText("foo", results, total);',
    'var label = "str"+AJS.I18n.getText("foo");',
    'var label = [].join(AJS.I18n.getText("foo"));',
    'var label = mymap[AJS.I18n.getText("foo")];',
    'var label = I18n.getText("foo");',
    'var label = I18n.getText("foo bar");',
    'var label = .I18n.getText("foo");', // Match even for syntax error
  ])(`should match given input "%s"`, (input) => {
    expect(input.match(i18nRegExp)).toBeTruthy();
  });

  test.each([
    'var label = AJS.I18n .getText("foo");', // Space between
    'var label = myI18n.getText("blah");', // Different function name
  ])(`should not match given input "%s"`, (input) => {
    expect(input.match(i18nRegExp)).toBeFalsy();
  });

  beforeEach(() => {
    // Reset RegEx state
    namedEsmImportRegexp.lastIndex = 0;
  });

  test.each([
    [' import { I18n} from "wrm/i18n"; ', 'wrm/i18n'],
    [" import {I18n} from 'wrm/i18n'; ", 'wrm/i18n'],
    [" import {I18n} from '@atlassian/wrm-react-i18n'; ", '@atlassian/wrm-react-i18n'],
    ['import {   I18n  }  \n from  \n "@atlassian/wrm-react-i18n"', '@atlassian/wrm-react-i18n'],
  ])('should match the named ESM import', (input, moduleName) => {
    const result = namedEsmImportRegexp.exec(input);

    expect(result).toBeTruthy();
    expect(result.groups.module).toEqual(moduleName);
  });
});
