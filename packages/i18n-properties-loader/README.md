# @atlassian/i18n-properties-loader

![node version](https://img.shields.io/node/v/@atlassian/i18n-properties-loader.svg)
![webpack peer dependency version](https://img.shields.io/npm/dependency-version/@atlassian/i18n-properties-loader/peer/webpack.svg)
![npm downloads](https://img.shields.io/npm/dt/@atlassian/i18n-properties-loader.svg)

A webpack loader for i18n `*.properties` files that can be used in Atlassian Server products and plugins.

## Motivation

If your work with the modern Front-End Server code there is a good chance you are already using the
[Atlassian Web-Resource webpack Plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin).
If you did configure your webpack to use development server you might be missing displaying translation phrases right
now.

The `@atlassian/i18n-properties-loader` can help you with solving that problem. It's a webpack loader that allows you
displaying translation phrases during your development workflow and at the same time uses the WRM.

For more information about the translations system check the Atlassian Development documentation page:

- [Internationalising your plugin](https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin/)

## Installation

```sh
npm install @atlassian/i18n-properties-loader --save-dev
```

## Usage example

```js
// webpack.config.js

const myI18nFiles = [
  'foo/i18n/my-translation-file.properties',
  'foo/bar/i18n/my-other-translation-file.properties',
  'bar/i18n/some-translation-file.properties',
];

module.exports = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: ['src'],

        use: [
          {
            loader: '@atlassian/i18n-properties-loader',
            options: {
              i18nFiles: myI18nFiles,
            },
          },

          {
            loader: 'babel-loader',
          },
        ],
      },
    ],
  },
};
```

## Options

- `i18nFiles` list of paths to your `*.properties` files (required)
- `disabled` disables the loader; can we used to disabled in production bundle (optional, default `false`)

### Creating production bundle

The loader is required only in development mode. You should remember to disable it when you are creaing production bundle.

With webpack and its [`mode` configuration](https://webpack.js.org/configuration/mode/) you can intercept the currently selected mode inside your webpack configuration and disable the loader accoridngly:

```js
// webpack.config.js

module.exports = (env, argv) => {
  const isDevelopmentMode = argv.mode === 'development'; // 1. Check if we are running webpack in "development" mode

  return {
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          include: ['src'],

          use: [
            {
              loader: '@atlassian/i18n-properties-loader',
              options: {
                i18nFiles,
                disabled: !isDevelopmentMode, // 2. Skip and disable loader when webpack is running in "production" mode
              },
            },

            {
              loader: 'babel-loader',
            },
          ],
        },
      ],
    },
  };
};
```

## React i18n helper

This package plays nice with the [`@atlassian/wrm-react-i18n`](https://www.npmjs.com/package/@atlassian/wrm-react-i18n)
when you want to use `I18n.getText()` translation helper with React components.

You can check the package description for more details and learn how to integrate it with your webpack configuration.

## Additional links

- [https://www.npmjs.com/package/@atlassian/i18n-properties-loader](https://www.npmjs.com/package/@atlassian/i18n-properties-loader)
- [https://www.npmjs.com/package/@atlassian/wrm-react-i18n](https://www.npmjs.com/package/@atlassian/wrm-react-i18n)
- [https://www.npmjs.com/package/atlassian-webresource-webpack-plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin)

## Minimum requirements

This plugin is compatible with:

- webpack 4.0+ and 5.0+
- Node 12+
