# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### 1.0.9 (2022-02-10)

**Note:** Version bump only for package @atlassian/i18n-properties-loader

### [1.0.8](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@1.0.8..@atlassian/i18n-properties-loader@1.0.7) (2021-11-17)

### Bug Fixes

- Update webpack depndency ([00fc73e](https://bitbucket.org/atlassianlabs/fe-server/commits/00fc73e95ad4a2f89ee4fd3561ab3fb60d1b9516))

### [1.0.7](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@1.0.7..@atlassian/i18n-properties-loader@1.0.6) (2021-10-26)

**Note:** Version bump only for package @atlassian/i18n-properties-loader

### [1.0.6](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@1.0.6..@atlassian/i18n-properties-loader@1.0.5) (2021-10-22)

### Bug Fixes

- **ci:** update webpack versions when running integration tests ([009dd61](https://bitbucket.org/atlassianlabs/fe-server/commits/009dd61f36a784d3fef7b6d6fa7214c5cc16b8b8))

### [1.0.5](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@1.0.5..@atlassian/i18n-properties-loader@1.0.4) (2021-10-14)

**Note:** Version bump only for package @atlassian/i18n-properties-loader

### [1.0.4](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@1.0.4..@atlassian/i18n-properties-loader@1.0.3) (2021-10-08)

**Note:** Version bump only for package @atlassian/i18n-properties-loader

### [1.0.3](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@1.0.3..@atlassian/i18n-properties-loader@1.0.2) (2021-09-24)

**Note:** Version bump only for package @atlassian/i18n-properties-loader

### [1.0.2](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@1.0.2..@atlassian/i18n-properties-loader@1.0.1) (2021-09-22)

- Update internal dependencies

### Bug Fixes

- rename configuration passed to schema validation util ([bb4b6a2](https://bitbucket.org/atlassianlabs/fe-server/commits/bb4b6a2791672a36b019ed7c99915cc8f77e308e))
- **release:** fix release script for the wrm package ([482efe2](https://bitbucket.org/atlassianlabs/fe-server/commits/482efe2d2262209926a775e56da3df6a89c54f95))

### [1.0.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@1.0.1..@atlassian/i18n-properties-loader@1.0.0) (2021-08-26)

**Note:** We updated internal dependencies

## [1.0.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@1.0.0..@atlassian/i18n-properties-loader@0.10.1) (2021-07-01)

### ⚠ BREAKING CHANGES

- We removed support for Node 10. The minimum required version is Node 12 now.

### Features

- drop support for Node 10 ([2ef1e83](https://bitbucket.org/atlassianlabs/fe-server/commits/2ef1e831cc30f0ec78884d72815d773ae401cc7e))

### [0.10.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@0.10.1..@atlassian/i18n-properties-loader@0.10.0) (2021-03-08)

### Bug Fixes

- **deps:** remove webpack cli dependency ([c957fb3](https://bitbucket.org/atlassianlabs/fe-server/commits/c957fb3741348478c885d4af443b14717ea02c95))
- **deps:** SPFE-346 Update ajv to fix vulnerabilities ([10cba18](https://bitbucket.org/atlassianlabs/fe-server/commits/10cba18c82fd7d4c368b8bb7cfab7d8eada56cf7))
- **deps:** update dependency schema-utils to v3 ([ce193c2](https://bitbucket.org/atlassianlabs/fe-server/commits/ce193c298dc28abf175939b8a1f86124e0e5f4cc))
- **deps:** Update Node version ([1dadcca](https://bitbucket.org/atlassianlabs/fe-server/commits/1dadcca42ac461410dabd8e844249cdf80e516a9))

## 0.10.0 (2021-01-18)

- feat: SPFE-275 Add support for webpack 5 to soy-loader ([edc7190](https://bitbucket.org/atlassianlabs/fe-server/commits/edc7190))

## 0.10.0-alpha.0 (2021-01-13)

- feat: SPFE-268 Add support for webpack 5 ([5a8b215](https://bitbucket.org/atlassianlabs/fe-server/commits/5a8b215))

## 0.9.0 (2020-06-25)

- chore: mark the exported RegExp as internals ([b487105](https://bitbucket.org/atlassianlabs/fe-server/commits/b487105))
- chore: migrate soy-loader tests to jest ([148dbef](https://bitbucket.org/atlassianlabs/fe-server/commits/148dbef))
- chore: run all tests with jest ([bd4f4e2](https://bitbucket.org/atlassianlabs/fe-server/commits/bd4f4e2))
- chore: update script commands ([d717342](https://bitbucket.org/atlassianlabs/fe-server/commits/d717342))
- fix(issue #4): add test cases that prove the quotations doesn't work well ([1012d1b](https://bitbucket.org/atlassianlabs/fe-server/commits/1012d1b)), closes [#4](https://bitbucket.org/atlassianlabs/fe-server/issue/4)
- fix(issue #4): Fix the message quotation that was inconsistent with Java MessageFormat spec https:// ([1572341](https://bitbucket.org/atlassianlabs/fe-server/commits/1572341))
- chore: update dependencies ([38f5a33](https://bitbucket.org/atlassianlabs/fe-server/commits/38f5a33))

## 0.8.0 (2020-05-11)

- chore: Add note about minimal requirements ([ee38cec](https://bitbucket.org/atlassianlabs/fe-server/commits/ee38cec))
- chore: downgrade to Node version 10 ([9fc7bab](https://bitbucket.org/atlassianlabs/fe-server/commits/9fc7bab))
- chore: Update minimal Node version to version 12 so we can support 'String.proptotype.matchall' ([cb0ed25](https://bitbucket.org/atlassianlabs/fe-server/commits/cb0ed25))

## [0.7.4](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@0.7.4..@atlassian/i18n-properties-loader@0.7.3) (2020-04-21)

**Note:** Version bump only for package @atlassian/i18n-properties-loader

## [0.7.3](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@0.7.3..@atlassian/i18n-properties-loader@0.7.1) (2020-02-07)

### Bug Fixes

- remove the console.log ([3d92754](https://bitbucket.org/atlassianlabs/fe-server/commits/3d9275465820339e6b8330ad3b6ec6cac023f34e))

## [0.7.2](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@0.7.2..@atlassian/i18n-properties-loader@0.7.1) (2020-01-28)

### Bug Fixes

- remove the console.log ([3d92754](https://bitbucket.org/atlassianlabs/fe-server/commits/3d9275465820339e6b8330ad3b6ec6cac023f34e))

## [0.7.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@0.7.1..@atlassian/i18n-properties-loader@0.7.0) (2020-01-27)

**Note:** Version bump only for package @atlassian/i18n-properties-loader

# [0.7.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@0.7.0..@atlassian/i18n-properties-loader@0.5.0) (2020-01-25)

### Features

- **webpack:** Add missing compatibility for native ESM and ts-loader ([afa4a1f](https://bitbucket.org/atlassianlabs/fe-server/commits/afa4a1f95ee74d6491a63699469bb54b3c515b55))

# [0.6.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/i18n-properties-loader@0.6.0..@atlassian/i18n-properties-loader@0.5.0) (2020-01-25)
