const supportedBrowsers = require('./index');

const specificTestingBrowsers = [
  'firefox 44', // Needed for Selenium 2 for older versions of atlassian-selenium
];

const testingBrowsers = [...supportedBrowsers, ...specificTestingBrowsers];

module.exports = testingBrowsers;
