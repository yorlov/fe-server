# Atlassian Server [Browserslist Configuration][browserslist]

This configuration reflects the supported browsers across Atlassian's Server products: [Bamboo][splat-bam], [Bitbucket][splat-bb], [Confluence][splat-conf], [Crowd][splat-cwd], [FECRU][splat-fecru], and [the Jira family][splat-jira].

## Warnings

FeCru 4.8 currently supports Edge18, use version 0.2.5 or earlier

Similarly, if targeting product versions - especially Long Term Support (LTS) Releases - check the supported browsers for each by [checking the references](#references). Edge 18, and potentially, IE11 may need to be supported.

## Usage

First add as a dev dependency with either `npm install -D @atlassian/browserslist-config-server` or `yarn add -D @atlassian/browserslist-config-server`

Then use the browserslist configuration within the project. The recommended way is to copy the below into the `package.json` file.

```json
{
  "browserslist": ["extends @atlassian/browserslist-config-server"]
}
```

This is desktop and mobile for all products.

### Desktop only

Use `"browserslist": ["extends @atlassian/browserslist-config-server/desktop"]`

### Testing

Supports older browsers deliberately for test builds. Intended for use with [atlassian-selenium](https://bitbucket.org/atlassian/atlassian-selenium/src/master/).

Use [multiple browserslists for each different environment](https://github.com/browserslist/browserslist#configuring-for-different-environments), e.g:

```json
"browserslist": [
  ...
  "testing": ["extends @atlassian/browserslist-config-server/testing"]
]
```

If you have already upgraded to Selenium 3.x, you don't need this.

### Mobile

Use `"browserslist": ["extends @atlassian/browserslist-config-server/mobile"]`

### References

- [Bamboo supported platforms][splat-bam]
- [Confluence supported platforms][splat-conf]
- [Crowd supported platforms][splat-cwd]
- [FECRU supported platforms][splat-fecru]
- [Jira supported platforms][splat-jira]

[browserslist]: https://github.com/browserslist/browserslist
[splat-bam]: https://confluence.atlassian.com/bamboo/supported-platforms-289276764.html#Supportedplatforms-Webbrowsers
[splat-bb]: https://confluence.atlassian.com/bitbucketserver/supported-platforms-776640981.html#Supportedplatforms-integrationsIntegrations
[splat-conf]: https://confluence.atlassian.com/doc/supported-platforms-207488198.html
[splat-cwd]: https://confluence.atlassian.com/crowd/supported-platforms-191851.html#SupportedPlatforms-Browsers
[splat-fecru]: https://confluence.atlassian.com/fisheye/supported-platforms-960155252.html
[splat-jira]: https://confluence.atlassian.com/adminjiraserver/supported-platforms-938846830.html#Supportedplatforms-Browsers
