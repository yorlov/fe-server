import assert from 'assert';
import { createHash } from 'crypto';
import flatMap from 'lodash/flatMap';
import isObject from 'lodash/isObject';
import once from 'lodash/once';
import unionBy from 'lodash/unionBy';
import uniq from 'lodash/uniq';
import path from 'path';
import { pd as PrettyData } from 'pretty-data';
import { SyncWaterfallHook } from 'tapable';
import urlJoin from 'url-join';
import { v4 as uuidv4Gen } from 'uuid';
import type { Compilation, Compiler, LoaderContext, NormalModule } from 'webpack';

import AppResourcesFactory from './AppResourcesFactory';
import { addBaseDependency } from './deps/base-dependencies';
import { builtInProvidedDependencies } from './deps/provided-dependencies';
import { webpack5or4 } from './helpers/conditional-logic';
import { writeFileSync } from './helpers/file-system';
import { extractPathPrefixForXml, toMap } from './helpers/options-parser';
import { renderWebResource } from './helpers/web-resource-generator';
import { error, log, setVerbose, warn } from './logger';
import requireEnsureShim from './shims/require-ensure-shim';
import runtimeLoadShim from './shims/runtime-load-shim';
import type {
  AssetNames,
  ChunkResourceDescriptor,
  ConsolidatedOptions,
  DataProvidersMap,
  Options,
  ProvidedDependenciesMap,
  ResourceParam,
  ResourceParamMap,
  TransformationMap,
} from './types/types';
import ProvidedExternalDependencyModule from './webpack-modules/ProvidedExternalDependencyModule';
import WrmDependencyModule from './webpack-modules/WrmDependencyModule';
import WrmResourceModule from './webpack-modules/WrmResourceModule';
import { extractLibraryDetailsFromWebpackConfig, isRunningInProductionMode } from './WebpackHelpers';
import { hookIntoCompleDoneToGenerateReports, hookIntoNormalModuleFactory } from './WebpackRuntimeHelpers';
import WrmManifestPlugin from './WrmManifestPlugin';

const defaultResourceParams = new Map().set('svg', [
  {
    name: 'content-type',
    value: 'image/svg+xml',
  },
]);

const defaultTransformations = new Map()
  .set('js', ['jsI18n'])
  .set('soy', ['soyTransformer', 'jsI18n'])
  .set('less', ['lessTransformer']);

const DEFAULT_DEV_ASSETS_HASH = 'DEV_PSEUDO_HASH';

type TransformerExtensions = {
  [key: string]: string[];
};

class WrmPlugin {
  static extendTransformations(values: TransformerExtensions) {
    return [defaultTransformations, toMap(values)].reduce((acc, map) => {
      for (const [key, val] of map.entries()) {
        const oldVals = acc.get(key);
        const newVals = [].concat(oldVals).concat(val).filter(Boolean);
        acc.set(key, newVals);
      }
      return acc;
    }, new Map());
  }

  private options: ConsolidatedOptions;

  /**
   * A Webpack plugin that takes the compilation tree and creates <web-resource> XML definitions that mirror the
   * dependency graph.
   *
   * This plugin will:
   *
   * - generate <web-resource> definitions for each entrypoint, along with additional <web-resource> definitions for
   *   and appropriate dependencies on all chunks generated during compilation.
   * - Add <dependency> declarations to each generated <web-resource> as appropriate, both for internal and external
   *   dependencies in the graph.
   * - Add appropriate metadata to the <web-resource> definition, such as appropriate <context>s,
   *   enabled/disabled state, and more.
   *   @param {Options} options
   */
  constructor(options: Options) {
    assert(
      options.pluginKey,
      `Option [String] "pluginKey" not specified. You must specify a valid fully qualified plugin key. e.g.: com.atlassian.jira.plugins.my-jira-plugin`,
    );
    assert(
      options.xmlDescriptors,
      `Option [String] "xmlDescriptors" not specified. You must specify the path to the directory where this plugin stores the descriptors about this plugin, used by the WRM to load your frontend code. This should point somewhere in the "target/classes" directory.`,
    );
    assert(path.isAbsolute(options.xmlDescriptors), `Option [String] "xmlDescriptors" must be absolute!`);

    // pull out our options
    this.options = Object.assign(
      {
        addAsyncNameAsContext: false,
        addEntrypointNameAsContext: false,
        noWRM: false,
        useDocumentWriteInWatchMode: false,
        verbose: false,
        watch: false,
        watchPrepare: false,

        conditionMap: new Map(),
        contextMap: new Map(),
        dataProvidersMap: new Map(),
        providedDependencies: new Map(),
        webresourceKeyMap: new Map(),

        resourceParamMap: defaultResourceParams,
        transformationMap: defaultTransformations,

        devAssetsHash: DEFAULT_DEV_ASSETS_HASH,
        locationPrefix: '',
      },
      options,
    );

    setVerbose(this.options.verbose);

    this.options.locationPrefix = extractPathPrefixForXml(this.options.locationPrefix);

    // convert various maybe-objects to maps
    this.options.conditionMap = toMap(this.options.conditionMap!);
    this.options.contextMap = toMap(this.options.contextMap!);
    this.options.webresourceKeyMap = toMap(this.options.webresourceKeyMap!);

    // make sure various maps contain only unique items
    this.options.resourceParamMap = this.ensureResourceParamsAreUnique(toMap(this.options.resourceParamMap!));
    this.options.transformationMap = this.ensureTransformationsAreUnique(toMap(this.options.transformationMap!));
    this.options.providedDependencies = this.ensureProvidedDependenciesAreUnique(
      toMap(this.options.providedDependencies!),
    );
    this.options.dataProvidersMap = this.ensureDataProvidersMapIsValid(toMap(this.options.dataProvidersMap!));

    this.getAssetsUUID = once(this.getAssetsUUID.bind(this));
  }

  /**
   * Generate an asset uuid per build - this is used to ensure we have a new "cache" for our assets per build.
   * As JIRA-Server does not "rebuild" too often, this can be considered reasonable.
   */
  getAssetsUUID(isProduction: boolean) {
    return isProduction ? uuidv4Gen() : this.options.devAssetsHash;
  }

  ensureTransformationsAreUnique(transformations: TransformationMap) {
    transformations.forEach((val, key, map) => {
      const values = ([] as string[]).concat(val).filter(Boolean);
      map.set(key, uniq(values));
    });
    return transformations;
  }

  ensureResourceParamsAreUnique(params: ResourceParamMap) {
    params.forEach((val, key, map) => {
      const values = ([] as ResourceParam[]).concat(val).filter(Boolean);
      map.set(key, unionBy(values.reverse(), 'name').reverse());
    });
    return params;
  }

  ensureProvidedDependenciesAreUnique(providedDependencies: ProvidedDependenciesMap) {
    const result: ProvidedDependenciesMap = new Map(builtInProvidedDependencies);

    for (const [name, providedDependency] of providedDependencies) {
      if (result.has(name)) {
        continue;
      }

      result.set(name, providedDependency);
    }

    log('Using provided dependencies', Array.from(result));

    return result;
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  ensureDataProvidersMapIsValid(dataProvidersMap: DataProvidersMap) {
    const map: DataProvidersMap = new Map();
    const requiredKeys = ['key', 'class'];

    for (const [entryPoint, dataProviders] of dataProvidersMap) {
      if (!Array.isArray(dataProviders)) {
        error(`The value of data providers for "${entryPoint}" entry point should be an array of data providers.`, {
          entryPoint,
          dataProviders,
        });

        continue;
      }

      const validDataProviders = [];

      for (const dataProvider of dataProviders) {
        const keys = isObject(dataProvider) ? Object.keys(dataProvider) : [];
        const isValidShape = requiredKeys.every((key) => keys.includes(key));

        if (!isValidShape) {
          error(
            `The data provider shape for "${entryPoint}" entry point doesn't include required keys: ${requiredKeys.concat(
              ', ',
            )}.`,
            { entryPoint, dataProvider },
          );

          continue;
        }

        const { key, class: providerClass } = dataProvider;

        if (!key || !providerClass) {
          error(`The data provider shape for "${entryPoint}" entry point contains missing or empty values.`, {
            entryPoint,
            key,
            class: providerClass,
          });

          continue;
        }

        validDataProviders.push({
          key,
          class: providerClass,
        });
      }

      if (validDataProviders.length) {
        map.set(entryPoint, validDataProviders);
      }
    }

    return map;
  }

  checkConfig(compiler: Compiler) {
    const ensureJsonPFunction = () => {
      const outputOptions = compiler.options.output;

      // @ts-expect-error TODO: Check if this is still valid with Webpack 5 as types suggest otherwise
      const { jsonpFunction } = outputOptions;
      if (!jsonpFunction || jsonpFunction === 'webpackJsonp') {
        const generatedJsonpFunction = `atlassianWebpackJsonp${createHash('md5')
          .update(this.options.pluginKey, 'utf8')
          .digest('hex')}`;
        warn(`
*********************************************************************************
The output.jsonpFunction is not specified. This needs to be done to prevent clashes.
An automated jsonpFunction name for this plugin was created:

"${generatedJsonpFunction}"
*********************************************************************************

`);
        // @ts-expect-error TODO: Check if this is still valid with Webpack 5 as types suggest otherwise
        outputOptions.jsonpFunction = generatedJsonpFunction;
      }
    };

    const ensureDevToolsAreDisabledForPrepare = () => {
      const devToolOption = compiler.options.devtool;
      if (this.options.watchPrepare && devToolOption) {
        error(
          `Having "devtool" option set to anything but "false" during the "watch-prepare" is invalid. It was set to: "${devToolOption}".`,
        );
        compiler.options.devtool = false;

        // report issue to webpack
        compiler.hooks.thisCompilation.tap('WarnNoDevToolInWatchPrepare', (compilation) => {
          compilation.warnings.push(
            // eslint-disable-next-line @typescript-eslint/no-var-requires
            new (require('webpack/lib/WebpackError'))(
              `Having "devtool" option set to anything but "false" during the "watch-prepare" is invalid. It was set to: "${devToolOption}".`,
            ),
          );
        });
      }
    };

    const ensureOutputPath = () => {
      const outputPath = compiler.options.output.path;
      if (!outputPath) {
        throw new Error(`No "output.path" specified in your webpack configuration. This is required! Stopping.`);
      }
    };

    compiler.hooks.afterEnvironment.tap('Check Config', () => {
      ensureJsonPFunction();
      ensureDevToolsAreDisabledForPrepare();
      ensureOutputPath();
    });
  }

  overwritePublicPath(compiler: Compiler) {
    const isProductionMode = isRunningInProductionMode(compiler);

    compiler.hooks.compilation.tap('OverwritePublicPath Compilation', (compilation) => {
      compilation.mainTemplate.hooks.requireExtensions.tap(
        'OverwritePublicPath Require-Extensions',
        (standardScript) => {
          // Ensure the `AJS.contextPath` function is available at runtime.
          addBaseDependency('com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path');
          const uuid = this.getAssetsUUID(isProductionMode);
          const assetWebresource = `${this.options.pluginKey}:assets-${uuid}`;

          // Add the public path extension to the webpack module runtime.
          return `${standardScript}
if (typeof AJS !== "undefined") {
    __webpack_require__.p = AJS.contextPath() + "/s/${uuid}/_/download/resources/${assetWebresource}/";
}
`;
        },
      );
    });
  }

  hookUpProvidedDependencies(compiler: Compiler) {
    hookIntoNormalModuleFactory('wrm plugin - provided dependencies', compiler, (factory) => (data, callback) => {
      const { target } = extractLibraryDetailsFromWebpackConfig(compiler);
      const request = data.dependencies[0].request;
      // get globally available libraries through wrm
      if (this.options.providedDependencies.has(request)) {
        log('plugging hole into request to %s, will be provided as a dependency through WRM', request);
        const p = this.options.providedDependencies.get(request)!;
        callback(null, new ProvidedExternalDependencyModule(p.import, p.dependency, target as 'var' | 'amd'));
        return;
      }
      return factory(data, callback);
    });
  }

  injectWRMSpecificRequestTypes(compiler: Compiler) {
    hookIntoNormalModuleFactory('wrm plugin - inject request types', compiler, (factory) => (data, callback) => {
      const { target } = extractLibraryDetailsFromWebpackConfig(compiler);
      const request = data.dependencies[0].request;
      // import web-resources we find static import statements for
      if (request.startsWith('wr-dependency!')) {
        const res = request.substr('wr-dependency!'.length);
        log('adding %s as a web-resource dependency through WRM', res);
        callback(null, new WrmDependencyModule(res, target!, this.options.pluginKey));
        return;
      }

      // import resources we find static import statements for
      if (request.startsWith('wr-resource!')) {
        const res = request.substr('wr-resource!'.length);
        log('adding %s as a resource through WRM', res);

        callback(null, new WrmResourceModule(res, target!, data.context, compiler.options.context!));
        return;
      }

      return factory(data, callback);
    });
  }

  /**
   * Ensure the WRM.require function is available at runtime and is used to load any code-split chunks.
   */
  enableAsyncLoadingWithWRM(compiler: Compiler) {
    compiler.hooks.compilation.tap('enable async loading with wrm - compilation', (compilation) => {
      webpack5or4(
        () => {
          compilation.mainTemplate.hooks.jsonpScript.tap(
            'enable async loading with wrm - jsonp-script',
            (source, chunk) => {
              // TODO: understand how to set this data on chunk "properly" so that
              //  our normalModuleFactory hook will pick it up and generate this dep for us.
              // @ts-expect-error We know we shouldn't set a random attribute...
              chunk.needsWrmRequire = true;

              // Add the WRM async loader in to webpack's loader function.
              return runtimeLoadShim(
                this.options.pluginKey,
                this.options.watch,
                compiler.options.output.publicPath as string,
              );
            },
          );
        },
        () => {
          // copy & pasted hack from webpack
          if (!compilation.mainTemplate.hooks.jsonpScript) {
            // @ts-expect-error It is read only, but 🤷‍♀️
            // noinspection JSConstantReassignment
            compilation.mainTemplate.hooks.jsonpScript = new SyncWaterfallHook(['source', 'chunk', 'hash']);
          }

          compilation.mainTemplate.hooks.requireEnsure.tap(
            'enable async loading with wrm - jsonp-script',
            (source, chunk) => {
              // TODO: understand how to set this data on chunk "properly" so that
              //  our normalModuleFactory hook will pick it up and generate this dep for us.
              // @ts-expect-error Same as above. We know we shouldn't set a random attribute...
              chunk.needsWrmRequire = true;

              // Add the WRM async loader in to the webpack jsonp loading snippet.
              return requireEnsureShim(this.options.pluginKey);
            },
          );
        },
      );
    });
  }

  shouldOverwritePublicPath() {
    if (this.options.watch) {
      return false;
    }
    if (this.options.noWRM) {
      return false;
    }

    return true;
  }

  shouldEnableAsyncLoadingWithWRM() {
    if (this.options.noWRM) {
      return false;
    }

    return true;
  }

  apply(compiler: Compiler) {
    // ensure settings make sense
    this.checkConfig(compiler);

    // hook up external dependencies
    this.hookUpProvidedDependencies(compiler);
    // allow `wr-dependency/wr-resource` require calls.
    this.injectWRMSpecificRequestTypes(compiler);

    if (this.shouldOverwritePublicPath()) {
      this.overwritePublicPath(compiler);
    }
    if (this.shouldEnableAsyncLoadingWithWRM()) {
      this.enableAsyncLoadingWithWRM(compiler);
    }

    const outputPath = compiler.options.output.path!;
    const isProductionMode = isRunningInProductionMode(compiler);
    const assetsUUID = this.getAssetsUUID(isProductionMode);
    const assetNames: AssetNames = new Map();

    // Generate a 1:1 mapping from original filenames to compiled filenames
    compiler.hooks.compilation.tap('wrm plugin setup phase', (compilation) => {
      const moduleLoader = webpack5or4(
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        () => require('webpack/lib/NormalModule').getCompilationHooks(compilation).loader,
        () => compilation.hooks.normalModuleLoader,
      );

      moduleLoader.tap(
        'wrm plugin - normal module',
        (loaderContext: LoaderContext<Record<string, unknown>>, module: NormalModule) => {
          const { emitFile } = loaderContext;
          loaderContext.emitFile = (name, content, sourceMap) => {
            const originalName = module.userRequest;
            assetNames.set(originalName, name);

            return emitFile.call(module, name, content, sourceMap);
          };
        },
      );
    });

    const appResourcesFactory = new AppResourcesFactory({
      assetsUUID,
      assetNames,
      options: this.options,
    });

    /**
     * Given a completed compilation, determine where each file generated
     * by webpack should be referenced in a web-resource.
     *
     * @param compilation the finalised compilation for the build
     * @returns a list of {@see ChunkResourceDescriptor} objects which describe a web-resource bundle
     */
    const getWebResourceDescriptors = (compilation: Compilation) => {
      const appResourceGenerator = appResourcesFactory.build(compiler, compilation);
      return appResourceGenerator.getResourceDescriptors();
    };

    /**
     * Given a list of web-resource descriptors, write their definitions
     * to XML, ready for the WRM to pick them up at product runtime.
     *
     * @param descriptors the list of web-resources to write definitions for
     */
    const generateWebResourceXmlReport = (descriptors: ChunkResourceDescriptor[]) => {
      const webResources = descriptors.map((descriptor) =>
        renderWebResource(
          descriptor,
          this.options.transformationMap,
          this.options.locationPrefix,
          this.options.resourceParamMap,
        ),
      );

      const xmlDescriptorsFilepath = this.options.xmlDescriptors;
      const xmlDescriptors = PrettyData.xml(`<bundles>${webResources.join('')}</bundles>`);

      writeFileSync(xmlDescriptorsFilepath, xmlDescriptors);
    };

    /**
     * Given a list of web-resource descriptors, for each that
     * references javascript files, overwrite them with new files that
     * will request content from the webpack-dev-server after the WRM
     * loads them at product runtime.
     *
     * @param descriptors  the list of web-resources to scan and find
     * javascript files within
     */
    const generateHotModuleRedirectFiles = (descriptors: ChunkResourceDescriptor[]) => {
      const redirectDescriptors = flatMap(descriptors, (c) => c.resources)
        .filter((res) => path.extname(res) === '.js')
        .map((r) => ({ fileName: r, writePath: path.join(outputPath, r) }));

      const overwriteFiles = () => {
        const generateAssetCall = (fileName: string) => {
          // TODO: should we just pretend `publicPath` is a string here?
          const pathName = urlJoin(compiler.options.output.publicPath as string, fileName);

          const appendScript = `
var script = document.createElement('script');
script.src = '${pathName}';
script.async = false;
script.crossOrigin = 'anonymous';
document.head.appendChild(script);`.trim();

          if (this.options.useDocumentWriteInWatchMode) {
            return `
!function(){
if (document.readyState === "loading" && 'initiallyRendered' in document.currentScript.dataset) {
    document.write('<script src="${pathName}"></script>')
} else {
    ${appendScript}
}
}();
`;
          }

          return `!function() { ${appendScript} }();`;
        };

        for (const { fileName, writePath } of redirectDescriptors) {
          writeFileSync(writePath, generateAssetCall(fileName));
        }
      };
      webpack5or4(
        () => {
          compiler.hooks.afterDone.tap('wrm plugin - add watch mode modules', overwriteFiles);
        },
        () => {
          overwriteFiles();
        },
      );
    };

    /**
     * After the compilation is complete, we analyse the result to produce our descriptors,
     * which we then use to generate various metadata files in the build output.
     */
    hookIntoCompleDoneToGenerateReports('wrm plugin - generate descriptors', compiler, (compilation, cb) => {
      const descriptors = getWebResourceDescriptors(compilation);

      // write the xml for web-resource module descriptors
      generateWebResourceXmlReport(descriptors);

      // write javascript files to enable hot-reloading at dev time
      if (this.options.watch && this.options.watchPrepare) {
        generateHotModuleRedirectFiles(descriptors);
      }

      cb();
    });

    // Enable manifest output if provided
    if (this.options.wrmManifestPath) {
      const filename = path.isAbsolute(this.options.wrmManifestPath)
        ? this.options.wrmManifestPath
        : path.resolve(path.join(outputPath, this.options.wrmManifestPath));
      new WrmManifestPlugin(appResourcesFactory, filename, this.options.pluginKey).apply(compiler);
    }
  }
}

export = WrmPlugin;
