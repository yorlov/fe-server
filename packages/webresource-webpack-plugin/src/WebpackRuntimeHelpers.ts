import type { Compilation, Compiler } from 'webpack';

import { webpack5or4 } from './helpers/conditional-logic';
import type { FactoryHandler, ResolveDataHandler, Webpack4FactoryHandler } from './types/extracted-webpack-types';

export const hookIntoNormalModuleFactory = (stageName: string, compiler: Compiler, cb: FactoryHandler) => {
  compiler.hooks.compile.tap(stageName, (params) => {
    const hooks = params.normalModuleFactory.hooks;
    webpack5or4(
      () => {
        const passThruFactory: ResolveDataHandler = (data, callback) => callback();
        hooks.factorize.tapAsync(
          {
            name: stageName,
            stage: 99,
          },
          cb(passThruFactory),
        );
      },
      () => {
        (hooks as unknown as Webpack4FactoryHandler).factory.tap(stageName, cb);
      },
    );
  });
};

export const hookIntoCompleDoneToGenerateReports = (
  stageName: string,
  compiler: Compiler,
  cb: (compilation: Compilation, callback: () => void) => void,
) => {
  compiler.hooks.done.tapAsync(stageName, (stats, callback) => {
    cb(stats.compilation, callback);
  });
};
