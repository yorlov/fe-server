import path from 'path';

import EmptyExportsModule from './EmptyExportsModule';

export default class WrmResourceModule extends EmptyExportsModule {
  private _resource: [string, string];

  constructor(resourceNameAndLocationPair: string, target: string, requestContext: string, rootContext: string) {
    super(resourceNameAndLocationPair, target);
    const resourcePair = resourceNameAndLocationPair.split('!');
    let resPath = resourcePair[1];

    // enable relative resource-paths. Still requires that this resource is correctly copied to the right location in target.
    if (resPath.startsWith('./') || resPath.startsWith('../')) {
      const fullResourcePath = path.join(requestContext, resPath);
      resPath = path.relative(rootContext, fullResourcePath);
    }

    this._resource = [resourcePair[0], resPath];
  }

  getResourcePair() {
    return this._resource;
  }

  getResource() {
    const [name, location] = this._resource;
    return { name, location };
  }
}
