import EmptyExportsModule from './EmptyExportsModule';

export default class WrmDependencyModule extends EmptyExportsModule {
  private _wrmDependency: string;

  constructor(dependency: string, type: string, pluginKey: string) {
    super(dependency, type);
    this._wrmDependency = dependency.includes(':') ? dependency : `${pluginKey}:${dependency}`;
  }

  getDependency() {
    return this._wrmDependency;
  }
}
