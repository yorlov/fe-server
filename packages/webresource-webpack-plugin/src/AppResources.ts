import flatMap from 'lodash/flatMap';
import uniq from 'lodash/uniq';
import type { Chunk, Compilation, Compiler, NormalModule } from 'webpack';

import { getBaseDependencies } from './deps/base-dependencies';
import { webpack5or4 } from './helpers/conditional-logic';
import {
  getConditionForEntry,
  getContextForEntry,
  getDataProvidersForEntry,
  getWebresourceAttributesForEntry,
} from './helpers/web-resource-entrypoints';
import type { Entrypoint } from './types/extracted-webpack-types';
import type { AssetNames, ChunkResourceDescriptor, ConsolidatedOptions } from './types/types';
import {
  getAllAsyncChunks,
  getDependenciesForChunks,
  getExternalResourcesForChunk,
  isSingleRuntime,
} from './WebpackHelpers';

const RUNTIME_WR_KEY = 'common-runtime';

type SplitChunkDependenciesKeyMap = Map<Chunk, { key: string; dependency: string }>;

export type AppResourceParams = {
  assetsUUID: string;
  assetNames: AssetNames;
  options: ConsolidatedOptions;
  compiler: Compiler;
  compilation: Compilation;
};

export default class AppResources {
  private readonly assetsUUID: string;
  private readonly compiler: Compiler;
  private readonly compilation: Compilation;

  private assetNames: AssetNames;
  private options: ConsolidatedOptions;

  /**
   * @param assetsUUID unique hash to identify the assets web-resource
   * @param assetNames full module filepaths -> relative filepath
   * @param options WrmPlugin configuration
   * @param compiler Webpack compiler
   * @param compilation Webpack compilation
   */
  constructor({ assetsUUID, assetNames, options, compiler, compilation }: AppResourceParams) {
    this.assetsUUID = assetsUUID;
    this.assetNames = assetNames;
    this.options = options;
    this.compiler = compiler;
    this.compilation = compilation;
  }

  getSingleRuntimeFiles(): string[] {
    const files = this.getEntryPoints()
      .map((entrypoint) => entrypoint.getRuntimeChunk()?.files)
      .find(Boolean);
    if (!files) {
      return [];
    }

    return Array.from(files);
  }

  getAssetResourceDescriptor(): ChunkResourceDescriptor {
    // remove anything that we know is handled differently
    const assetFiles = Object.keys(this.compilation.assets).filter((p) => !/\.(js|css|soy)(\.map)?$/.test(p));

    return {
      attributes: { key: `assets-${this.assetsUUID}` },
      resources: assetFiles,
    };
  }

  getDependencyResourcesFromChunk(chunk: Chunk): string[] {
    return webpack5or4(
      () => Array.from(chunk.auxiliaryFiles),
      () => {
        const ownDepsSet = new Set<string>();
        const fileDepsSet = new Set<string>();
        chunk.getModules().forEach((m) => {
          ownDepsSet.add((m as NormalModule).resource);
          if (m.buildInfo.fileDependencies) {
            (m.buildInfo.fileDependencies as string[]).forEach((filepath) => fileDepsSet.add(filepath));
          }
        });
        return Array.from(fileDepsSet)
          .filter((filename) => this.assetNames.has(filename))
          .filter((filename) => !ownDepsSet.has(filename))
          .filter((filename) => !/\.(?:js|css|soy)$/.test(filename))
          .map((dep) => this.assetNames.get(dep))
          .filter((assetName): assetName is string => Boolean(assetName));
      },
    );
  }

  /**
   * Every entrypoint has an attribute called "chunks".
   * This contains all chunks that are needed to successfully "load" this entrypoint.
   * Usually every entrypoint only contains one chunk - the bundle that is build for that entrypoint.
   * If more than one chunk is present that means they are split-chunks that contain code needed by the entrypoint to function.
   * To get all split chunks we need to get all but the entrypoints "runtimeChunk" which is the chunk solely containing code for this entrypoint and its runtime.
   *
   * IMPORTANT-NOTE: async-chunks required by this entrypoint are not specified in these chunks but in the childGroups of the entry and/or split chunks.
   */
  getSyncSplitChunks() {
    const syncSplitChunks = flatMap(this.getEntryPoints(), (e) => e.chunks.filter((c) => c !== e.getRuntimeChunk()));

    return uniq(syncSplitChunks);
  }

  /**
   * Create a key and the fully-qualified web-resource descriptor for every split chunk.
   * This is needed to point to reference these chunks as dependency in the entrypoint chunks
   *
   * <web-resource>
   *   ...
   *   <dependency>this-plugin-key:split_some_chunk</dependency>
   *   ...
   * </web-resource>
   */
  getSyncSplitChunkDependenciesKeyMap(syncSplitChunks: Chunk[]) {
    const syncSplitChunkDependencyKeyMap: SplitChunkDependenciesKeyMap = new Map();

    for (const c of syncSplitChunks) {
      const webResourceKey = `split_${c.name || c.id}`;
      syncSplitChunkDependencyKeyMap.set(c, {
        key: webResourceKey,
        dependency: `${this.options.pluginKey}:${webResourceKey}`,
      });
    }

    return syncSplitChunkDependencyKeyMap;
  }

  getSyncSplitChunksResourceDescriptors(): ChunkResourceDescriptor[] {
    const syncSplitChunks = this.getSyncSplitChunks();
    const syncSplitChunkDependencyKeyMap = this.getSyncSplitChunkDependenciesKeyMap(syncSplitChunks);

    /**
     * Create descriptors for the split chunk web-resources that have to be created.
     * These include - like other chunk-descriptors their assets and external resources etc.
     */
    return syncSplitChunks.map((c) => {
      const additionalFileDeps = this.getDependencyResourcesFromChunk(c);
      return {
        attributes: syncSplitChunkDependencyKeyMap.get(c)!,
        externalResources: getExternalResourcesForChunk(this.compilation, c),
        resources: uniq([...c.files, ...additionalFileDeps]),
        dependencies: uniq([...getBaseDependencies(), ...getDependenciesForChunks(this.compilation, c)]),
      };
    });
  }

  getAsyncChunksResourceDescriptors(): ChunkResourceDescriptor[] {
    return getAllAsyncChunks(this.getEntryPoints()).map((c) => {
      const additionalFileDeps = this.getDependencyResourcesFromChunk(c);
      return {
        attributes: { key: `${c.id}` },
        externalResources: getExternalResourcesForChunk(this.compilation, c),
        resources: uniq([...c.files, ...additionalFileDeps]),
        dependencies: uniq([...getBaseDependencies(), ...getDependenciesForChunks(this.compilation, c)]),
        contexts: this.options.addAsyncNameAsContext && c.name ? [`async-chunk-${c.name}`] : undefined,
      };
    });
  }

  getEntryPoints(): Entrypoint[] {
    return Array.from(this.compilation.entrypoints.values());
  }

  getEntryPointsResourceDescriptors(): ChunkResourceDescriptor[] {
    const singleRuntime = isSingleRuntime(this.compiler);

    const syncSplitChunks = this.getSyncSplitChunks();
    const syncSplitChunkDependencyKeyMap = this.getSyncSplitChunkDependenciesKeyMap(syncSplitChunks);

    const singleRuntimeWebResourceKey = this.options.singleRuntimeWebResourceKey || RUNTIME_WR_KEY;

    // Used in prod
    const prodEntryPoints: ChunkResourceDescriptor[] = this.getEntryPoints().map((entrypoint) => {
      const name = entrypoint.name!;
      const webResourceAttrs = getWebresourceAttributesForEntry(name, this.options.webresourceKeyMap);
      const entrypointChunks = entrypoint.chunks;
      const runtimeChunk = entrypoint.getRuntimeChunk()!;

      // Retrieve all split chunks this entrypoint depends on. These must be added as "<dependency>"s to the web-resource of this entrypoint
      const sharedSplitDeps = entrypointChunks
        .map((c) => syncSplitChunkDependencyKeyMap.get(c))
        .filter(Boolean)
        .map((val) => val?.dependency);

      // Construct the list of resources to add to this web-resource
      const resourceList = flatMap(entrypointChunks, (c) => this.getDependencyResourcesFromChunk(c));

      const dependencyList = [
        ...getBaseDependencies(),
        ...getDependenciesForChunks(this.compilation, runtimeChunk),
        ...sharedSplitDeps,
      ];

      if (singleRuntime) {
        dependencyList.unshift(`${this.options.pluginKey}:${singleRuntimeWebResourceKey}`);
      } else {
        resourceList.unshift(...runtimeChunk.files);
      }

      return {
        attributes: webResourceAttrs,
        contexts: getContextForEntry(name, this.options.contextMap, this.options.addEntrypointNameAsContext),
        conditions: getConditionForEntry(name, this.options.conditionMap),
        dataProviders: getDataProvidersForEntry(name, this.options.dataProvidersMap),
        externalResources: getExternalResourcesForChunk(this.compilation, runtimeChunk),
        resources: uniq(resourceList),
        dependencies: uniq(dependencyList).filter(Boolean) as string[],
      };
    });

    if (singleRuntime) {
      prodEntryPoints.push({
        attributes: { key: singleRuntimeWebResourceKey },
        dependencies: getBaseDependencies(),
        resources: this.getSingleRuntimeFiles(),
      });
    }

    return prodEntryPoints;
  }

  getResourceDescriptors() {
    return ([] as ChunkResourceDescriptor[])
      .concat(this.getSyncSplitChunksResourceDescriptors())
      .concat(this.getAsyncChunksResourceDescriptors())
      .concat(this.getEntryPointsResourceDescriptors())
      .concat(this.getAssetResourceDescriptor());
  }
}
