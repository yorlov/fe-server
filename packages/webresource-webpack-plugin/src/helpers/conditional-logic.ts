import { version } from 'webpack';

import type { Callable } from '../types/types';

const majorVersion = parseInt(version.replace(/\..*$/, ''));
const isWebpack5 = majorVersion > 4;

export const webpack5or4 = <T extends Callable>(inV5: T, inV4: T): ReturnType<T> => {
  return isWebpack5 ? inV5 && inV5() : inV4 && inV4();
};
