import escapeRegExp from 'lodash/escapeRegExp';
import path from 'path';

import type { ObjectOrMap } from '../types/types';

const escapedSeparator = escapeRegExp(path.sep);
const pathSeparatorRegex = new RegExp(`^\\${escapedSeparator}|\\${escapedSeparator}$`, 'g');

export const toMap = <T>(original: ObjectOrMap): Map<string, T> => {
  if (original instanceof Map) {
    return original as Map<string, T>;
  }
  return original && typeof original === 'object' ? new Map(Object.entries(original)) : new Map();
};

export const extractPathPrefixForXml = (pathPrefix: string | undefined) => {
  if (!pathPrefix || pathPrefix === '' || pathPrefix === '/') {
    return '';
  }

  // remove leading/trailing path separator
  const withoutLeadingTrailingSeparators = pathPrefix.replace(pathSeparatorRegex, '');
  // readd trailing slash - this time OS independent always a "/"
  return withoutLeadingTrailingSeparators + '/';
};
