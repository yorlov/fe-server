export const toTrimmedString = (val: string | unknown) => {
  const maybeStr = typeof val === 'string' ? val : '';
  return maybeStr.trim();
};
