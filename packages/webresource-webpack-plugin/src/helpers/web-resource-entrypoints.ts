import type { ConditionsMap, ContextMap, DataProvidersMap, WebresourceMap } from '../types/types';
import { parseWebResourceAttributes } from './web-resource-parser';

export const getContextForEntry = (
  entry: string,
  contextMap: ContextMap,
  addEntrypointNameAsContext: boolean,
): string[] => {
  const initialArray = addEntrypointNameAsContext ? [entry] : [];
  if (!contextMap.has(entry)) {
    return initialArray;
  }

  // TODO: verify if we need the typecheck here, as Intellij is rightfully complaining about the typecheck here as `contextMap` should only return "strings"
  // noinspection SuspiciousTypeOfGuard
  return initialArray
    .concat(contextMap.get(entry)!)
    .filter((context): context is string => typeof context === 'string' && context.trim() !== '');
};

export const getWebresourceAttributesForEntry = (entry: string, webresourceKeyMap: WebresourceMap) => {
  const wrKey = webresourceKeyMap.get(entry);

  // Create the default attribute values
  let attrs = { key: `entrypoint-${entry}`, moduleId: entry };

  // Extend the attributes with parsed, valid values
  if (typeof wrKey === 'object') {
    attrs = Object.assign(attrs, parseWebResourceAttributes(wrKey));
  }

  // Override the key if a non-empty string is provided
  if (typeof wrKey === 'string') {
    attrs = Object.assign(attrs, parseWebResourceAttributes({ key: wrKey }));
  }

  return attrs;
};

export const getConditionForEntry = (entry: string, conditionMap: ConditionsMap) => {
  return conditionMap.get(entry);
};

export const getDataProvidersForEntry = (entrypoint: string, dataProvidersMap: DataProvidersMap) => {
  return dataProvidersMap.get(entrypoint) || [];
};
