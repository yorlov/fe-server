import type { WebresourceObject } from '../types/types';
import { toTrimmedString } from './string';

// eslint-disable-next-line sonarjs/cognitive-complexity
export const parseWebResourceAttributes = (data: WebresourceObject) => {
  const attributes: WebresourceObject = {};
  // Only parse objects.
  if (data && typeof data === 'object') {
    // define the key if a non-empty string was provided
    if (Object.prototype.hasOwnProperty.call(data, 'key')) {
      const customKey = toTrimmedString(data.key);
      if (customKey) {
        attributes.key = customKey;
      }
    }

    // define the state as either enabled or disabled
    if (Object.prototype.hasOwnProperty.call(data, 'state')) {
      let isEnabled = true;
      if (typeof data.state === 'boolean') {
        isEnabled = data.state;
      } else if (typeof data.state === 'string') {
        isEnabled = data.state !== 'disabled';
      }
      attributes.state = isEnabled ? 'enabled' : 'disabled';
    }

    // define the name if a non-empty string was provided
    if (Object.prototype.hasOwnProperty.call(data, 'name')) {
      const customName = toTrimmedString(data.name);
      if (customName) {
        attributes.name = customName;
      }
    }
  }

  return attributes;
};
