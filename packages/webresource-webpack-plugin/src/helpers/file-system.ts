import fs from 'fs';
import path from 'path';

function writeFileSync(outputPath: string, contents: string): boolean {
  try {
    // will throw a TypeError if the path is non-normal
    path.parse(outputPath);
    fs.mkdirSync(path.dirname(outputPath), { recursive: true });
    fs.writeFileSync(outputPath, contents, 'utf8');
    return true;
  } catch (e) {
    console.error(`Failed to output file at path ${outputPath}`, e);
    return false;
  }
}

export { writeFileSync };
