import path from 'path';

import type {
  ChunkResourceDescriptor,
  DataProvider,
  ResourceParam,
  ResourceParamMap,
  TransformationMap,
  WrmResource,
} from '../types/types';
import renderCondition from './renderCondition';
import renderTransforms from './renderTransformations';
import { parseWebResourceAttributes } from './web-resource-parser';
import { renderElement } from './xml';

/**
 * Renders list of data providers {@see DataProvider} as <data key="provider-key" class="data.provider.Class" /> elements
 */
const renderDataProviders = (dataProviders: DataProvider[] | undefined) => {
  if (!Array.isArray(dataProviders) || dataProviders.length === 0) {
    return [];
  }

  return dataProviders.map((dataProvider) =>
    renderElement('data', {
      key: dataProvider.key,
      class: dataProvider.class,
    }),
  );
};

function renderContexts(contexts: string[] | undefined) {
  return contexts ? contexts.map((context) => `<context>${context}</context>`) : [];
}

function renderDependencies(dependencies: string[] | undefined) {
  return dependencies ? dependencies.map((dependency) => `<dependency>${dependency}</dependency>`) : [];
}

const generateResourceElement = (resource: WrmResource, parameterMap: ResourceParamMap) => {
  const { name, location } = resource;
  const assetContentType = path.extname(location).substr(1);
  const parameters = parameterMap.get(assetContentType) || [];
  const children: string[] = [];
  const renderParameters = (attributes: ResourceParam) => children.push(renderElement('param', attributes));
  parameters.forEach(renderParameters);

  return renderElement(
    'resource',
    {
      type: 'download',
      name,
      location,
    },
    children,
  );
};

const renderResources = (parameterMap: ResourceParamMap, resources: WrmResource[]) => {
  return resources
    ? resources
        .filter(Boolean)
        // ignore all `.map` files, since the WRM finds them of its own accord.
        .filter((resource) => !resource.location.endsWith('.map'))
        .map((resource) => generateResourceElement(resource, parameterMap))
    : [];
};

export const renderWebResource = (
  webresource: ChunkResourceDescriptor,
  transformations: TransformationMap,
  pathPrefix: string,
  parameterMap: ResourceParamMap,
) => {
  const { resources = [], externalResources = [], contexts, dependencies, conditions, dataProviders } = webresource;
  const attributes = parseWebResourceAttributes(webresource.attributes);
  const allResources: WrmResource[] = [];
  const children = [];

  const prependPathPrefix = (location: string) => pathPrefix + location;

  // add resources for direct dependencies (e.g., JS and CSS files)
  allResources.push(...resources.map((res) => ({ name: res, location: prependPathPrefix(res) })));

  // add resources for indirect dependencies (e.g., images extracted from CSS)
  allResources.push(...externalResources.map((wr) => ({ name: wr.name, location: prependPathPrefix(wr.location) })));

  children.push(
    ...renderTransforms(transformations, allResources),
    ...renderContexts(contexts),
    ...renderDependencies(dependencies),
    ...renderResources(parameterMap, allResources),
    ...renderDataProviders(dataProviders),
    renderCondition(conditions),
  );

  return renderElement('web-resource', attributes, children);
};
