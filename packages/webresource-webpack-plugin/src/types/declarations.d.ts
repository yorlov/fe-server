declare module 'webpack/lib/DllModule' {
  // eslint-disable-next-line import/no-duplicates
  import { Module } from 'webpack';

  class DllModule extends Module {
    // TODO: fix with v4 deprecation
    // in v5 the 4th param "type" no longer exists - but we need to stay compatible with v4 for now
    // and it will just be ignored in v5 anyway
    constructor(context: string | null, dependencies: string[], name: string, type: string);
  }

  export default DllModule;
}

declare module 'webpack/lib/ExternalModule' {
  // eslint-disable-next-line import/no-duplicates
  import { Module } from 'webpack';

  class ExternalModule extends Module {
    // Funny enough this constructor actually has 3 params. Not sure why we only pass two.
    constructor(request: { [key: string]: unknown } | string, request: string);
  }

  export default ExternalModule;
}
