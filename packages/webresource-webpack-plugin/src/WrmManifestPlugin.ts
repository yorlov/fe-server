import type { Compiler } from 'webpack';

import type AppResourceFactory from './AppResourcesFactory';
import { buildProvidedDependency } from './deps/provided-dependencies';
import { webpack5or4 } from './helpers/conditional-logic';
import { writeFileSync } from './helpers/file-system';
import { error } from './logger';
import type { PathData } from './types/extracted-webpack-types';
import type { ProvidedDependency } from './types/types';
import { extractLibraryDetailsFromWebpackConfig } from './WebpackHelpers';
import { hookIntoCompleDoneToGenerateReports } from './WebpackRuntimeHelpers';

type WrmManifestMapping = Record<string, ProvidedDependency>;

export default class WrmManifestPlugin {
  private appResourcesFactory: AppResourceFactory;
  private outputPath: string;
  private pluginKey: string;

  constructor(appResourcesFactory: AppResourceFactory, outputPath: string, pluginKey: string) {
    this.pluginKey = pluginKey;
    this.appResourcesFactory = appResourcesFactory;
    this.outputPath = outputPath;
  }

  apply(compiler: Compiler) {
    const { outputPath, appResourcesFactory, pluginKey } = this;
    const { name, target } = extractLibraryDetailsFromWebpackConfig(compiler);
    if (!name || !target) {
      error('Can only use wrmManifestPath in conjunction with output.library and output.libraryTarget');
      return;
    }

    if (target !== 'amd') {
      error(`Could not create manifest mapping. LibraryTarget '${target}' is not supported. Use 'amd'`);
      return;
    }

    hookIntoCompleDoneToGenerateReports('wrm manifest - generate deps', compiler, (compilation, cb) => {
      const appResourceGenerator = appResourcesFactory.build(compiler, compilation);
      const wrmManifestMapping = appResourceGenerator
        .getEntryPointsResourceDescriptors()
        .filter(({ attributes }) => attributes.moduleId)
        .reduce<WrmManifestMapping>((result, { attributes: { key: resourceKey, moduleId } }) => {
          const getAssetPath = webpack5or4(
            () => (name: string, opts: PathData) => compilation.getAssetPath(name, opts),
            () => (name, opts) => compilation.mainTemplate.getAssetPath(name, opts),
          );
          const libraryName = getAssetPath(name!, {
            chunk: { name: moduleId, id: moduleId!, hash: moduleId! },
          });

          result[moduleId!] = buildProvidedDependency(pluginKey, resourceKey, `require('${libraryName}')`, libraryName);

          return result;
        }, {});

      const wrmManifestJSON = JSON.stringify({ providedDependencies: wrmManifestMapping }, null, 4);

      writeFileSync(outputPath, wrmManifestJSON);

      cb();
    });
  }
}
