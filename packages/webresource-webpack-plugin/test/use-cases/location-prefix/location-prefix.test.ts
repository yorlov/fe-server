import fs from 'fs';
import path from 'path';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKey, getResources } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('location-prefix', () => {
  let contextEntryNode: Node;

  beforeEach((done) => {
    webpack(config, () => {
      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      contextEntryNode = getByKey(results.root.children, 'entrypoint-simple-entry');
      done();
    });
  });

  it("add prefix to direct and external dependencies' location value", () => {
    const nodes = getResources(contextEntryNode);
    const locations = nodes.map((node) => node.attributes.location);
    expect(locations).toEqual(['js/simple-entry.js', 'js/template.soy', 'js/styles.less']);
  });
});
