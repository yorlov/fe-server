import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKeyStarsWith, getContent, getDependencies } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('provided-modules-replacement', () => {
  let stats: Stats;
  let entry: Node;
  let dependencies: string[];

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      entry = getByKeyStarsWith(results.root.children, 'entry');
      dependencies = getContent(getDependencies(entry));
      done();
    });
  });

  it('should create a webresource with dependencies for each async chunk', () => {
    expect(entry).toBeTruthy();
    expect(dependencies).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  it('add a dependency for the provided module to the webresource', () => {
    expect(dependencies).toContain('jira.webresources:jquery');
    expect(dependencies).toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
    expect(dependencies).toContain('a.plugin.key:webresource-key');
  });

  it('should specify the provided dependencies as proper amd dependencies', () => {
    // setup
    const bundleFile = fs.readFileSync(path.join(targetDir, 'app.js'), 'utf-8');
    const [bundleLine] = bundleFile.split('\n');

    expect(
      bundleLine.startsWith(`define(["underscore","jquery"],`) ||
        bundleLine.startsWith(`define(["jquery","underscore"],`),
    ).toBeTruthy();
  });
});
