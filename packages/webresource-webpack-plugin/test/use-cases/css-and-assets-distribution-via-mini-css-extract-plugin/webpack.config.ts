import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import path from 'path';
import type { Configuration } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');
const ASSET_PATH = process.env.ASSET_PATH || '/';

const config: Configuration = {
  mode: 'development',
  devtool: false,
  entry: {
    app: path.join(FRONTEND_SRC_DIR, 'app.js'),
    app2: path.join(FRONTEND_SRC_DIR, 'app2.js'),
    notransitives: path.join(FRONTEND_SRC_DIR, 'app-notransitives.js'),
  },
  module: {
    rules: [
      {
        test: /\.(css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
            },
          },
        ],
      },
      {
        test: /\.(jpg|png|svg|ttf|eot|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
        },
      },
    ],
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'com.atlassian.plugin.test',
      xmlDescriptors: path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
      verbose: false,
    }),
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore this might have incompatible types - but can be ignored - cant use ts-expect-error here as this only occures on some webpack versions
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
  ],
  output: {
    filename: '[name].js',
    path: OUTPUT_DIR,
    publicPath: ASSET_PATH,
  },
  optimization: {
    chunkIds: 'size',
    moduleIds: 'size',
  },
};

export default config;
