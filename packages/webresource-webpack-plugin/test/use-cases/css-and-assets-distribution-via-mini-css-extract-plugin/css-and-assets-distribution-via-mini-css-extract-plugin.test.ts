import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKey, getResources } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('css-and-assets-distribution-via-mini-css-extract-plugin', () => {
  let stats: Stats;
  let allExpectedWrs: { [key: string]: Node };

  function getResourceNames(node: Node) {
    return getResources(node).map((r) => r.attributes.name);
  }

  beforeEach((done) => {
    allExpectedWrs = {};

    webpack(config, (err, st) => {
      if (err) {
        throw err;
      }

      stats = st!;

      if (stats.hasErrors()) {
        throw new Error('Webpack compilation failed');
      }

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      const wrs = results.root.children;
      allExpectedWrs.app1 = getByKey(wrs, 'entrypoint-app');
      allExpectedWrs.app2 = getByKey(wrs, 'entrypoint-app2');
      allExpectedWrs.appNotransitives = getByKey(wrs, 'entrypoint-notransitives');
      allExpectedWrs.asyncChunkForApp1 = getByKey(wrs, '0');
      allExpectedWrs.asyncChunkForApp2 = getByKey(wrs, '1');
      allExpectedWrs.assets = getByKey(wrs, 'assets-DEV_PSEUDO_HASH');
      done();
    });
  });

  it('should build without problems', () => {
    expect(stats.hasWarnings()).toEqual(false);
    Object.entries(allExpectedWrs).forEach(([, node]) => {
      expect(node).toBeDefined();
    });
  });

  it('should add all directly-referenced assets to app1 entry', () => {
    const app1ResourceNames = getResourceNames(allExpectedWrs.app1);
    expect(app1ResourceNames).toEqual(expect.arrayContaining(['app.css', 'app.js', 'ice.png']));
  });

  it('should add all directly-referenced assets to the async chunk for app1', () => {
    const resourceNames = getResourceNames(allExpectedWrs.asyncChunkForApp1);
    expect(resourceNames).toEqual(
      expect.arrayContaining([
        '0.css',
        '0.js',
        'rect2.svg',
        'rect.svg',
        'fake-font.eot',
        'fake-font.ttf',
        'fake-font.svg',
      ]),
    );
  });

  // todo SPFE-281
  it.skip('should add all directly-referenced assets to app2 entry', () => {
    const app1ResourceNames = getResourceNames(allExpectedWrs.app2);
    expect(app1ResourceNames).toEqual(expect.arrayContaining(['app2.css', 'app2.js', 'ice2.jpg']));
  });

  // todo SPFE-281
  it.skip('should add all directly-referenced assets to the async chunk for app2', () => {
    const resourceNames = getResourceNames(allExpectedWrs.asyncChunkForApp1);
    expect(resourceNames).toEqual(expect.arrayContaining(['1.js', '1.css', 'rect.svg', 'rect2.svg']));
  });

  // todo SPFE-281
  it.skip('should only add font files to web-resources that directly reference them', () => {
    const fontFiles = ['fake-font.ttf', 'fake-font.eot', 'fake-font.svg'];
    const shouldHave = [allExpectedWrs.asyncChunkForApp1, allExpectedWrs.app2];
    const shouldNotHave = [allExpectedWrs.app1, allExpectedWrs.asyncChunkForApp2];

    shouldHave.forEach((wr) => {
      const resourceNames = getResourceNames(wr);
      expect(resourceNames).toEqual(expect.arrayContaining(fontFiles));
    });

    shouldNotHave.forEach((wr) => {
      const resourceNames = getResourceNames(wr);
      console.log(wr.attributes.key, resourceNames);
      // notIncludeMembers exists, just doesn't seem to be in the types package
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      expect(resourceNames).not.toEqual(expect.arrayContaining(fontFiles));
    });
  });

  // todo: this could be optimised. we only need the assets web-resource to contain references to
  // files loaded via JavaScript and the publicPath, which in this example is nothing (they're all loaded via CSS).
  it('should create an asset resource containing all "other" assets', () => {
    const assetResourceNames = getResourceNames(allExpectedWrs.assets);
    expect(assetResourceNames).toHaveLength(7);
    expect(assetResourceNames).toEqual(
      expect.arrayContaining([
        'ice.png',
        'ice2.jpg',
        'fake-font.eot',
        'fake-font.ttf',
        'fake-font.svg',
        'rect2.svg',
        'rect.svg',
      ]),
    );
  });
});
