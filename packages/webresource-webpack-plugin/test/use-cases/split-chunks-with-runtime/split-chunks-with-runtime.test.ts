import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKey, getContent, getDependencies, getResources } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('split-chunks-with-runtime', () => {
  let stats: Stats;
  let error: Error;
  let entryApp: Node;
  let entryApp2: Node;
  let splitChunkApp: Node;
  let splitChunkApp2: Node;
  let splitChunkShared: Node;
  let testEntryApp: Node;
  let testEntryApp2: Node;

  beforeEach((done) => {
    webpack(config, (err, st) => {
      error = err!;
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      entryApp = getByKey(results.root.children, 'entrypoint-app');
      entryApp2 = getByKey(results.root.children, 'entrypoint-app2');
      splitChunkApp = getByKey(results.root.children, 'split_app');
      splitChunkApp2 = getByKey(results.root.children, 'split_app2');
      splitChunkShared = getByKey(results.root.children, 'split_app~app2');
      testEntryApp = getByKey(results.root.children, '__test__entrypoint-app');
      testEntryApp2 = getByKey(results.root.children, '__test__entrypoint-app2');
      done();
    });
  });

  it('should create a webresources with dependencies and resources as appropriate', () => {
    expect(entryApp).toBeTruthy();
    expect(entryApp2).toBeTruthy();
    expect(splitChunkApp).toBeTruthy();
    expect(splitChunkApp2).toBeTruthy();
    expect(splitChunkShared).toBeTruthy();

    expect(error).toEqual(null);
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  describe('split chunk for shared modules', () => {
    it('should create a web-resource for the split chunk', () => {
      expect(splitChunkShared).toBeTruthy();
      expect(getResources(splitChunkShared)).toHaveLength(1);
    });

    it('should contain all dependencies specified in at least 2 entry-points', () => {
      const deps = getContent(getDependencies(splitChunkShared));
      expect(deps).toContain('jira.webresources:jquery');
      expect(deps).toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
    });
  });

  describe('entry points', () => {
    let depsApp: string[];
    let depsApp2: string[];
    beforeEach(() => {
      depsApp = getContent(getDependencies(entryApp));
      depsApp2 = getContent(getDependencies(entryApp2));
    });
    it('should not have direct dependency to shared deps', () => {
      expect(depsApp).not.toContain('jira.webresources:jquery');
      expect(depsApp).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(depsApp2).not.toContain('jira.webresources:jquery');
      expect(depsApp2).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
    });

    it('should have dependency to split chunks', () => {
      expect(depsApp).toContain('com.atlassian.plugin.test:split_app');
      expect(depsApp).toContain('com.atlassian.plugin.test:split_app~app2');
      expect(depsApp2).toContain('com.atlassian.plugin.test:split_app2');
      expect(depsApp2).toContain('com.atlassian.plugin.test:split_app~app2');
    });
  });

  describe('test web-resources', () => {
    it('should not have any test entries', () => {
      expect(testEntryApp).toEqual(undefined);
      expect(testEntryApp2).toEqual(undefined);
    });
  });
});
