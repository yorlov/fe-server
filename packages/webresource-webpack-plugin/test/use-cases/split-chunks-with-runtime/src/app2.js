import $ from 'jquery';

import bar from './bar';
import foo2 from './foo2';

$(() => {
  $('body').html(`hello world, ${bar} ${foo2}`);
});
