import $ from 'jquery';

import bar from './bar';
import foo from './foo';

$(() => {
  $('body').html(`hello world, ${bar} ${foo}`);
});
