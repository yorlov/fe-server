import crypto from 'crypto';
import path from 'path';
import type { Chunk, Configuration, Module } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

const delimiter = '--';

const hashed = (name: string) => {
  return crypto.createHash('md4').update(name).digest('hex').slice(0, 10);
};

const nameSplitChunk = (module: Module, chunks: Array<Chunk>, cacheGroup: string | null) => {
  const prefix = cacheGroup && cacheGroup !== 'default' ? cacheGroup + delimiter : '';
  const names = chunks.map((c) => c.name);
  if (!names.every(Boolean)) {
    console.error('one chunk had an empty name...', names);
    return;
  }
  names.sort();
  return 'custom.splitchunk.' + prefix + hashed(names.join(delimiter));
};

const nameChunk = `custom.chunk.[name]${delimiter}[contenthash].js`;

const config: Configuration = {
  mode: 'production',
  devtool: false,
  context: FRONTEND_SRC_DIR,
  entry: {
    'app-1': './app.js',
    'app-2': './app2.js',
  },
  output: {
    path: OUTPUT_DIR,
    publicPath: '',
    filename: nameChunk,
    chunkFilename: nameChunk,
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      minSize: 0,
      chunks: 'all',
      maxAsyncRequests: Infinity,
      maxInitialRequests: Infinity,
      name: nameSplitChunk,
    },
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'com.atlassian.plugin.test',
      xmlDescriptors: path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
      verbose: false,
    }),
  ],
};

export default config;
