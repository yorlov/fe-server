import './common';
import foo from './foo';
import bar from './bar';

$(async () => {
  const first = await import('./async-first');
  $('body').html(`hello world, ${bar} ${foo}`, first);
});
