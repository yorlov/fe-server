console.log('an async module!');

import('./async-second').then((second) => console.log('second loaded', second));

export default 'first';
