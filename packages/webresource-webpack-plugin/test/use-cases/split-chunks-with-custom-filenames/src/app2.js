import './common';
import foo2 from './foo2';
import bar from './bar';

$(async () => {
  const first = await import('./async-first');
  $('body').html(`hello world, ${bar} ${foo2}`, first);
});
