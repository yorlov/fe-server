import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getResources, getWebResources } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('split-chunks', function () {
  jest.setTimeout(10000);

  let stats: Stats;
  let error: Error;
  let resources: Node[];

  const runWebpack = (config: webpack.Configuration, done: jest.DoneCallback) =>
    webpack(config, (err, st) => {
      error = err!;
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      resources = getWebResources(results.root).flatMap((node) => getResources(node));

      done();
    });

  const runTheTestsFor = (config: webpack.Configuration) => {
    beforeEach((done) => {
      runWebpack(config, done);
    });

    it('should run without error', () => {
      expect(error).toBeNull();
      expect(stats.hasErrors()).toEqual(false);
      expect(stats.hasWarnings()).toEqual(false);
    });

    it('references all files generated', () => {
      const referencedFiles = resources.map((r) => r.attributes.location);
      const actualFiles = fs
        .readdirSync(targetDir)
        .filter((filepath) => fs.statSync(path.join(targetDir, filepath)).isFile());

      referencedFiles.sort();
      actualFiles.sort();

      expect(actualFiles).toEqual(expect.arrayContaining(referencedFiles));
    });
  };

  describe('in development mode', () => {
    const cfg = Object.assign({}, config, { mode: 'development' });
    runTheTestsFor(cfg);
  });

  describe('in production mode', () => {
    const cfg = Object.assign({}, config, { mode: 'production' });
    runTheTestsFor(cfg);
  });
});
