import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKeyEndsWith } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('specify-explicit-webresource-name', () => {
  let stats: Stats;
  let wrNodes: Node[];

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      wrNodes = results.root.children.filter((n) => n.name === 'web-resource');
      done();
    });
  });

  it('should work without error', () => {
    expect(wrNodes).not.toHaveLength(0);
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  it('should create a webresource with an explicit name when it is mapped in config as string', () => {
    const node = getByKeyEndsWith(wrNodes, 'mapped-with-string');
    expect(node).toEqual(expect.anything());
    expect(node.attributes.key).toBe('customkey-mapped-with-string');
  });

  it('should create a webresource with an explicit key when it is mapped in config as object', () => {
    const node = getByKeyEndsWith(wrNodes, 'mapped-with-object');
    expect(node).toEqual(expect.anything());
    expect(node.attributes.key).toBe('customkey-mapped-with-object');
  });

  it('should create a webresource with an explicit name when it is mapped in config as object without a key', () => {
    const node = getByKeyEndsWith(wrNodes, 'mapped-with-object-with-only-name');
    expect(node).toEqual(expect.anything());
    expect(node.attributes.key).toBe('entrypoint-app-good-mapped-with-object-with-only-name');
    expect(node.attributes.name).toBe('Legacy Name for App 2');
  });

  it('should create a webresource with an explicit key and name when it is mapped in config as object with key and a name', () => {
    const node = getByKeyEndsWith(wrNodes, 'mapped-with-object-with-name');
    expect(node).toEqual(expect.anything());
    expect(node.attributes.key).toBe('customkey-mapped-with-object-with-name');
    expect(node.attributes.name).toBe('Legacy Name for App 1');
  });

  it('should auto-generate the name if there is no config provided', () => {
    const node = getByKeyEndsWith(wrNodes, 'app-good-autonamed');
    expect(node).toEqual(expect.anything());
    expect(node.attributes.key).toBe('entrypoint-app-good-autonamed');
  });

  it('should auto-generate the name when the supplied value is not a string', () => {
    const badNodes = wrNodes.filter((n) => n.attributes.key.startsWith('entrypoint-app-bad'));
    expect(badNodes).toHaveLength(2);
    expect(badNodes[0].attributes.key).toBe('entrypoint-app-bad-objectlike');
    expect(badNodes[1].attributes.key).toBe('entrypoint-app-bad-falsy');
  });
});
