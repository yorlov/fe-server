import fs from 'fs';
import path from 'path';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKey, getContext, getResources } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('simple', () => {
  it('compiles an xml file', (done) => {
    webpack(config, (err, stats) => {
      if (err) {
        throw err;
      }
      expect(stats!.hasErrors()).toEqual(false);
      expect(stats!.hasWarnings()).toEqual(false);
      expect(fs.existsSync(webresourceOutput)).toEqual(true);
      done();
    });
  });

  describe('a web-resource for a webpack entry point', () => {
    let contextEntryNode: Node;

    beforeEach((done) => {
      webpack(config, () => {
        const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
        const results = parse(xmlFile);
        contextEntryNode = getByKey(results.root.children, 'entrypoint-simple-entry');
        done();
      });
    });

    it('exists', () => {
      expect(contextEntryNode).toEqual(expect.any(Object));
    });

    it('has an i18n transformation', () => {
      const node = contextEntryNode.children[0];
      expect(node).toHaveProperty('name', 'transformation');
      expect(node).toHaveProperty('attributes.extension', 'js');
      expect(node.children[0]).toHaveProperty('name', 'transformer');
      expect(node.children[0].attributes).toHaveProperty('key', 'jsI18n');
    });

    // @deprecated
    it('has a context named after the entry point', () => {
      const node = getContext(contextEntryNode).find((n) => n.content === 'simple-entry');
      expect(node).toHaveProperty('name', 'context');
      expect(node).toHaveProperty('content', 'simple-entry');
    });

    it('has a resource that references the generated bundle file', () => {
      const node = getResources(contextEntryNode)[0];
      expect(node).toHaveProperty('name', 'resource');
      expect(node).toHaveProperty('attributes.type', 'download');
      expect(node).toHaveProperty('attributes.name', 'simple-entry.js');
      expect(node).toHaveProperty('attributes.location', 'simple-entry.js');
    });
  });

  describe('a web-resource for the (web-resource) deps of an entry point', () => {
    let contextDepsNode: Node;

    beforeEach((done) => {
      webpack(config, () => {
        const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
        const results = parse(xmlFile);
        contextDepsNode = getByKey(results.root.children, 'entrypoint-simple-entry');
        done();
      });
    });

    it('exists', () => {
      expect(contextDepsNode).toEqual(expect.any(Object));
    });
  });
});
