import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { byExtension, getByKeyStarsWith, getResources } from '../../util/xml-parser';
import config from './webpack.config';
import svgConfig from './webpack.svg.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('resource-parameters', () => {
  let stats: Stats;
  let assets: Node;
  let resources: Node[];

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      assets = getByKeyStarsWith(results.root.children, 'assets');
      resources = getResources(assets);
      done();
    });
  });

  it('should add a param to a resource', () => {
    expect(assets).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
    expect(resources).toHaveLength(3);

    const png = byExtension(resources, '.png');
    expect(path.extname(png.attributes.name)).toEqual('.png');
    expect(png.children).toHaveLength(1);
    // eslint-disable-next-line sonarjs/no-duplicate-string
    expect(png.children[0].attributes.name).toEqual('bar');
    // eslint-disable-next-line sonarjs/no-duplicate-string
    expect(png.children[0].attributes.value).toEqual('baz');
    const svg = byExtension(resources, '.svg');
    expect(svg.attributes.type).toEqual('download');
    expect(svg.children).toHaveLength(2);
    expect(svg.children[0].attributes.name).toEqual('content-type');
    expect(svg.children[0].attributes.value).toEqual('image/svg+xml');
    expect(svg.children[1].attributes.name).toEqual('foo');
    expect(svg.children[1].attributes.value).toEqual('bar');
  });

  it('should include the last defined value for a given parameter', () => {
    const jpg = byExtension(resources, '.jpg');
    expect(jpg.children).toHaveLength(2);
    expect(jpg.children[0].attributes.name).toEqual('bar');
    expect(jpg.children[1].attributes.name).toEqual('duped');
    expect(jpg.children[1].attributes.value).toEqual('second');
  });

  it('should add image/svg+xml by default for svg files', (done) => {
    webpack(svgConfig, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      assets = getByKeyStarsWith(results.root.children, 'assets');
      resources = getResources(assets);

      const svg = byExtension(resources, '.svg');
      expect(svg.attributes.type).toEqual('download');
      expect(svg.children).toHaveLength(1);
      expect(svg.children[0].attributes.name).toEqual('content-type');
      expect(svg.children[0].attributes.value).toEqual('image/svg+xml');
      done();
    });
  });
});
