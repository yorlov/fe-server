import fs from 'fs';
import path from 'path';
import type { Configuration, Stats } from 'webpack';
import webpack from 'webpack';

import { webpack5or4 } from '../../../src/helpers/conditional-logic';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const appOutput = path.join(targetDir, 'app.js');

const jsonpFragmentv4 = /jsonpArray = window\["(.*?)"] = window\["(.*?)"] \|\| \[];/;
const jsonpFragmentv5 = /chunkLoadingGlobal = self\["(.*?)"] = self\["(.*?)"] \|\| \[];/;

describe('jsonp-function-naming', function () {
  let appCode: string;
  let stats: Stats;

  const run = (config: Configuration) => {
    return new Promise((resolve) => {
      webpack(config, (err, st) => {
        stats = st!;

        appCode = fs.readFileSync(appOutput, 'utf-8');
        resolve(null);
      });
    });
  };

  function v4tests() {
    describe('webpack 4 defaults', () => {
      beforeEach(async () => {
        // we want to see webpack 4's behaviour when this is not set.
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        delete config.output.jsonpFunction;
        await run(config);
      });

      it('should create a webresources without errors', () => {
        expect(stats.hasErrors()).toEqual(false);
        expect(stats.hasWarnings()).toEqual(false);
      });

      it('should rename the webpack jsonp function to a unique name for the plugin', () => {
        const matches = jsonpFragmentv4.exec(appCode)!;

        expect(matches).toBeTruthy();
        expect(matches[2]).toEqual(matches[1]);

        const jsonpFnName = matches[1];
        expect(jsonpFnName).not.toEqual('webpackJsonp');

        // our code should rename the function
        expect(jsonpFnName.startsWith('atlassianWebpackJsonp')).toBeTruthy();
        expect(jsonpFnName.match(/[0-9a-f]{32}$/)).toBeTruthy();
      });
    });

    describe('webpack 4 configured', () => {
      beforeEach(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        config.output.jsonpFunction = 'someFooBar';
        await run(config);
      });

      it('affects the chunk name', () => {
        const matches = jsonpFragmentv4.exec(appCode)!;
        expect(matches[1]).toEqual('someFooBar');
      });
    });
  }

  function v5tests() {
    describe('webpack 5 defaults', () => {
      beforeEach(async () => {
        // remove invalid config options in webpack 5
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        delete config.output.jsonpFunction;
        // we want to see webpack 5's behaviour when this is not set.
        delete config.output!.uniqueName;
        await run(config);
      });

      it('should create a webresources without errors', () => {
        expect(stats.hasErrors()).toEqual(false);
        expect(stats.hasWarnings()).toEqual(false);
      });

      it('should output a jsonp fragment', () => {
        const matches = jsonpFragmentv5.exec(appCode)!;
        expect(matches).toBeTruthy();

        const jsonpFnName = matches[1];

        // we cannot affect the name in webpack5.
        // config is parsed and jsonpFunction is always non-null beforeEach the environment hooks run.
        // This is slightly "stupid" webpack uses the closes "package.json" to define the name.
        // based on where this test is executed it might be "webpackChunkatlassian_fe_server" or "webpackChunkatlassian_webresource_webpack_plugin"
        expect(jsonpFnName).toMatch(/webpackChunk.+/);
      });
    });

    describe('webpack 5 configured', () => {
      beforeEach(async () => {
        config.output!.uniqueName = 'someFooBar';
        await run(config);
      });

      it('affects the chunk name', () => {
        const matches = jsonpFragmentv5.exec(appCode)!;
        expect(matches[1]).toEqual('webpackChunksomeFooBar');
      });
    });
  }

  webpack5or4(v5tests, v4tests);
});
