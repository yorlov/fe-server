import { sync } from 'glob';
import path from 'path';
import webpack from 'webpack';

import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');

describe('PLUGWEB-626 - source maps', () => {
  let mapFilesPrepareFalse: string[] = [];
  let mapFilesPrepareTrue: string[] = [];
  let warningPrepareTrue = '';
  let errorPrepareFalse = '';
  let errorPrepareTrue = '';

  beforeEach((done) => {
    Promise.all([
      new Promise<void>((resolve) => {
        webpack(config({ watchPrepare: false }), (err, st) => {
          if (st?.hasErrors()) {
            errorPrepareFalse = JSON.stringify(st!.toJson({ errorDetails: true }), null, 4);
            resolve();
            return;
          }

          mapFilesPrepareFalse = sync('*.map', {
            cwd: path.join(targetDir, 'false'),
          });

          resolve();
        });
      }),
      new Promise<void>((resolve) => {
        webpack(config({ watchPrepare: true }), (err, st) => {
          if (st?.hasErrors()) {
            errorPrepareTrue = JSON.stringify(st!.toJson({ errorDetails: true }), null, 4);
            resolve();
            return;
          }

          warningPrepareTrue = JSON.stringify(st?.toJson({ warnings: true }).warnings);

          mapFilesPrepareTrue = sync('*.map', {
            cwd: path.join(targetDir, 'true'),
          });
          resolve();
        });
      }),
    ])
      .then(() => done())
      .catch(() => done());
  });

  it('should not create source-maps in watch-prepare mode - even if devtools settings in the webpack config demand so.', () => {
    expect(errorPrepareTrue).toEqual('');
    expect(errorPrepareFalse).toEqual('');
    expect(mapFilesPrepareFalse).not.toHaveLength(0); // 'should generate .map files if not in watch-prepare'
    expect(mapFilesPrepareTrue).toHaveLength(0); // 'should not generate .map files in watch-prepare'
    expect(warningPrepareTrue).toContain(
      `Having \\"devtool\\" option set to anything but \\"false\\" during the \\"watch-prepare\\" is invalid. It was set to: \\"source-map\\"`,
    );
  });
});
