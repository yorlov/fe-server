import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { findByExtension, findKeyLike, getTransformations } from '../../util/xml-parser';
import config from './webpack.specify-transformations.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('specify-transformation', () => {
  let stats: Stats;
  let wrNodes: Node[];

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      wrNodes = results.root.children;
      done();
    });
  });

  it('should run without error', () => {
    expect(wrNodes).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  describe('add transformation for file extensions', () => {
    let entryJsTrans: Node;
    let entrySoyTrans: Node;
    let entryLessTrans: Node;
    let jsTrans: Node;
    let htmlTrans: Node;
    let lessTrans: Node;
    let soyTrans: Node;
    let svgTrans: Node;
    let txtTrans: Node;

    beforeEach(() => {
      const entrypointTransformations = getTransformations(findKeyLike(wrNodes, 'app-one'));
      const assetTransformations = getTransformations(findKeyLike(wrNodes, 'assets'));

      if (!entrypointTransformations) {
        throw new Error("Webpack compilation failed. Can't find entrypoints transformations.");
      }

      if (!assetTransformations) {
        throw new Error("Webpack compilation failed. Can't find assets transformations.");
      }

      entryJsTrans = findByExtension(entrypointTransformations, 'js');
      entrySoyTrans = findByExtension(entrypointTransformations, 'soy');
      entryLessTrans = findByExtension(entrypointTransformations, 'less');

      jsTrans = findByExtension(assetTransformations, 'js');
      htmlTrans = findByExtension(assetTransformations, 'html');
      lessTrans = findByExtension(assetTransformations, 'less');
      soyTrans = findByExtension(assetTransformations, 'soy');
      svgTrans = findByExtension(assetTransformations, 'svg');
      txtTrans = findByExtension(assetTransformations, 'txt');
    });

    it('should remove default transformations from web-resources', () => {
      expect(entrySoyTrans).toBeUndefined();
      expect(entryLessTrans).toBeUndefined();
      expect(entryJsTrans.children.map((c) => c.attributes.key)).not.toContain('jsI18n');

      // the defaults should not end up on the assets web-resource either,
      // since there aren't any assets of those types in the graph.
      expect(jsTrans).toBeUndefined();
      expect(lessTrans).toBeUndefined();
      expect(soyTrans).toBeUndefined();
    });

    it('should add custom transformations to web-resources', () => {
      expect(entryJsTrans.children.map((c) => c.attributes.key)).toContain('foo');
      expect(entryJsTrans.children.map((c) => c.attributes.key)).toContain('bar');

      expect(txtTrans.children.map((c) => c.attributes.key)).toContain('bar');

      expect(htmlTrans.children.map((c) => c.attributes.key)).toContain('stuff');
      expect(htmlTrans.children.map((c) => c.attributes.key)).toContain('n stuff');

      expect(svgTrans).toBeUndefined();
    });

    it('should not produce duplicated transformations', () => {
      const transformationNames = txtTrans.children.map((c) => c.attributes.key);

      expect(transformationNames).toHaveLength(1);
    });
  });
});
