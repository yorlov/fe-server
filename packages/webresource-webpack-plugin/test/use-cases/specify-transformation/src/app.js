import $ from 'jquery';

import rect from './rect.svg';
import body from './test.html';
import styles from './test.less';
import text from './test.txt';

$(() => {
  $('body').text(text).html(body).css(styles).append(rect);
});
