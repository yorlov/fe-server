import fs from 'fs';
import path from 'path';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKey } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const libraryDescriptor = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'lib.xml');
const featureDescriptor = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'feat.xml');

describe('specify-asset-dev-hash', () => {
  let results;
  let libDevNode: Node;
  let featDevNode: Node;

  beforeEach((done) => {
    webpack(config, () => {
      const libXMLFile = fs.readFileSync(libraryDescriptor, 'utf-8');
      const featXMLFile = fs.readFileSync(featureDescriptor, 'utf-8');
      results = parse(libXMLFile);
      libDevNode = getByKey(results.root.children, 'assets-libDevAssets');
      results = parse(featXMLFile);
      featDevNode = getByKey(results.root.children, 'assets-featDevAssets');
      done();
    });
  });

  it('uses custom dev asset hashes provided in webpack config', () => {
    expect(libDevNode).toEqual(expect.anything());
    expect(featDevNode).toEqual(expect.anything());
  });
});
