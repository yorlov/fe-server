import fs from 'fs';
import path from 'path';
import type { Configuration, Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getBaseDependencies } from '../../../src/deps/base-dependencies';
import { getByKeyStarsWith, getContent, getTransformations } from '../../util/xml-parser';
import configAmd from './webpack.config.amd';
import configEsm from './webpack.config.es6';
import configRelative from './webpack.config.relative';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('wrm-resource-loading', () => {
  let stats: Stats;
  let entry: Node;

  function runWebpack(config: Configuration, done: () => void) {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      entry = getByKeyStarsWith(results.root.children, 'entry');
      done();
    });
  }

  function runTheTestsFor(config: Configuration, context: string) {
    beforeEach((done) => runWebpack(config, done));

    it('should run without error', () => {
      expect(entry).toBeTruthy();
      expect(stats.hasErrors()).toEqual(false);
      expect(stats.hasWarnings()).toEqual(false);
    });

    it('has a Soy transformation', () => {
      const transformNodes = getTransformations(entry);
      const soyTransformNode = transformNodes.find((n) => n.attributes.extension === 'soy');
      expect(soyTransformNode).not.toEqual(null);
      expect(soyTransformNode?.children[0]).toHaveProperty('name', 'transformer');
      expect(soyTransformNode?.children[0].attributes).toHaveProperty('key', 'soyTransformer');
    });

    it('has a LESS transformation', () => {
      const transformNodes = getTransformations(entry);
      const lessTransformNode = transformNodes.find((n) => n.attributes.extension === 'less');
      expect(lessTransformNode).not.toEqual(null);
      expect(lessTransformNode?.children[0]).toHaveProperty('name', 'transformer');
      expect(lessTransformNode?.children[0].attributes).toHaveProperty('key', 'lessTransformer');
    });

    it('has the appropriate external resources', () => {
      const resourceNodes = entry.children.filter((n) => n.name === 'resource');
      const resources = resourceNodes.map((n) => n.attributes);
      expect(resources).toEqual(
        expect.arrayContaining([
          { name: 'ultimate/name/at/runtime.js', location: `${context}/template.soy`, type: 'download' },
          { name: 'ultimate/name/at/runtime.css', location: `${context}/styles.less`, type: 'download' },
        ]),
      );
    });

    it('has no additional web-resource dependencies', () => {
      const dependencyNodes = entry.children.filter((n) => n.name === 'dependency');
      const dependencies = getContent(dependencyNodes);
      expect(dependencies).toEqual(getBaseDependencies());
    });
  }

  describe('in ES6 modules', () => {
    runTheTestsFor(configEsm, 'src-es6');
  });

  describe('in AMD', () => {
    runTheTestsFor(configAmd, 'src-amd');
  });

  describe('with relative paths', () => {
    runTheTestsFor(configRelative, 'src-relative');
  });
});
