import fs from 'fs';
import path from 'path';
import type { Configuration, Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKeyStarsWith, getContent, getDependencies } from '../../util/xml-parser';
import configAmd from './webpack.config.amd';
import configEsm from './webpack.config.es6';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('wrm-dependency-loading', () => {
  let stats: Stats;
  let entry: Node;
  let dependencies: string[];

  function runWebpack(config: Configuration, done: () => void) {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      entry = getByKeyStarsWith(results.root.children, 'entry');
      dependencies = getContent(getDependencies(entry));
      done();
    });
  }

  function runTheTestsFor(config: Configuration) {
    beforeEach((done) => runWebpack(config, done));

    it('should create a webresource with dependencies', () => {
      expect(entry).toBeTruthy();
      expect(dependencies).toBeTruthy();
      expect(stats.hasErrors()).toEqual(false);
      expect(stats.hasWarnings()).toEqual(false);
    });

    it('add a dependency for each requested web-resource', () => {
      expect(dependencies).toContain('some.weird:web-resource');
      expect(dependencies).toContain('foo-bar:baz');
    });
  }

  describe('in ES6 modules', () => {
    runTheTestsFor(configEsm);
  });

  describe('in AMD', () => {
    runTheTestsFor(configAmd);
  });
});
