import 'wr-dependency!some.weird:web-resource';
import 'wr-dependency!foo-bar:baz';

import $ from 'jquery';

$(() => {
  $('body').html('hello world');
});
