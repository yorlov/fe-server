import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKeyEndsWith } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('specify-explicit-webresource-state', () => {
  let stats: Stats;
  let wrNodes: Node[];

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      wrNodes = results.root.children.filter((node) => node.name === 'web-resource');
      done();
    });
  });

  it('should work without error', () => {
    expect(wrNodes).not.toHaveLength(0);
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  describe('providing a plain string', () => {
    it('should create a webresource with an implicit state', () => {
      const node = getByKeyEndsWith(wrNodes, 'plain-string');
      expect(node).toEqual(expect.anything());
      expect(node.attributes).not.toHaveProperty('state');
    });
  });

  describe('providing only a key', () => {
    it('should create a webresource with an implicit state', () => {
      const node = getByKeyEndsWith(wrNodes, 'only-key');
      expect(node).toEqual(expect.anything());
      expect(node.attributes).not.toHaveProperty('state');
    });
  });

  describe('providing only a state', () => {
    it('should create a webresource with an explicit state', () => {
      const node = getByKeyEndsWith(wrNodes, 'only-state');
      expect(node).toEqual(expect.anything());
      expect(node.attributes.state).toBe('disabled');
    });

    it('should interpret a boolean of "true" as enabled', () => {
      const node = getByKeyEndsWith(wrNodes, 'only-state-boolean-enabled');
      expect(node).toEqual(expect.anything());
      expect(node.attributes.state).toBe('enabled');
    });

    it('should interpret a boolean of "false" as disabled', () => {
      const node = getByKeyEndsWith(wrNodes, 'only-state-boolean-disabled');
      expect(node).toEqual(expect.anything());
      expect(node.attributes.state).toBe('disabled');
    });
  });

  describe('providing an explicit key and state', () => {
    it('should create a webresource with explicit values when the state is "enabled"', () => {
      const node = getByKeyEndsWith(wrNodes, 'key-state-enabled');
      expect(node).toEqual(expect.anything());
      expect(node.attributes.key).toBe('customkey-key-state-enabled');
      expect(node.attributes.state).toBe('enabled');
    });

    it('should create a webresource with explicit values when the state is "disabled"', () => {
      const node = getByKeyEndsWith(wrNodes, 'key-state-disabled');
      expect(node).toEqual(expect.anything());
      expect(node.attributes.key).toBe('customkey-key-state-disabled');
      expect(node.attributes.state).toBe('disabled');
    });
  });
});
