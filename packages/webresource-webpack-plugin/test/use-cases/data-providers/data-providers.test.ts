import fs from 'fs';
import path from 'path';
import type { Configuration } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKey, getDataProviders } from '../../util/xml-parser';
import config from './webpack.config';
import configWithMaps from './webpack.config.with-map';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('data-providers', () => {
  let entrypoints: Node[];
  type ConfigFn = () => Configuration;
  function runWebpack(config: Configuration | ConfigFn, done: () => void) {
    const options = typeof config === 'function' ? config() : config;

    webpack(options, () => {
      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      entrypoints = results.root.children.filter((n) => n.attributes.key.startsWith('entry'))!;
      done();
    });
  }

  function runTheTestsFor(config: Configuration | ConfigFn) {
    beforeEach((done) => runWebpack(config, done));

    it('should generate one data provider for first entry point', () => {
      const entryPointNode = getByKey(entrypoints, 'entrypoint-my-first-entry-point');

      expect(entryPointNode).toBeTruthy();

      const dataProviders = getDataProviders(entryPointNode!);
      expect(Array.isArray(dataProviders)).toBe(true);
      expect(dataProviders).toHaveLength(1);

      const dataProvider = getByKey(dataProviders, 'first-data-provider')!;

      expect(dataProvider).toBeTruthy();

      expect(dataProvider.attributes).toEqual(
        expect.objectContaining({ key: 'first-data-provider', class: 'data.provider.JavaClass' }),
      );
    });

    it('should generate two data providers for second entry point', () => {
      const entryPointNode = getByKey(entrypoints, 'entrypoint-my-second-entry-point');

      expect(entryPointNode).toBeTruthy();

      const dataProviders = getDataProviders(entryPointNode!);
      expect(Array.isArray(dataProviders)).toBe(true);
      expect(dataProviders).toHaveLength(2);

      const fooDataProvider = getByKey(dataProviders, 'foo-data-provider')!;
      const barDataProvider = getByKey(dataProviders, 'bar-data-provider')!;

      expect(fooDataProvider).toBeTruthy();
      expect(barDataProvider).toBeTruthy();

      expect(fooDataProvider.attributes).toEqual(
        expect.objectContaining({ key: 'foo-data-provider', class: 'data.provider.FooClass' }),
      );

      expect(barDataProvider.attributes).toEqual(
        expect.objectContaining({ key: 'bar-data-provider', class: 'data.provider.BarClass' }),
      );
    });

    it('should not generate any data providers for third entry point', () => {
      const entryPointNode = getByKey(entrypoints, 'entrypoint-my-third-entry-point')!;

      expect(entryPointNode).toBeTruthy();

      const dataProviders = getDataProviders(entryPointNode);
      expect(dataProviders).toHaveLength(0);
    });
  }

  describe('using a map', () => {
    runTheTestsFor(configWithMaps);
  });

  describe('using a plain object', () => {
    runTheTestsFor(config);
  });
});
