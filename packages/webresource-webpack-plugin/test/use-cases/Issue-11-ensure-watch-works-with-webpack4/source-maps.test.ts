import type { spawnSync } from 'child_process';
import { execSync } from 'child_process';
import { promises } from 'fs';

describe('Issue-11 - Watch mode is broken in webpack 4', () => {
  it('Should not throw a runtime error when trying to run watchmode in webpack 4.', () => {
    expect(() => {
      try {
        execSync(`node -r esbuild-register ${require.resolve('./run-webpack.ts')}`);
      } catch (e: unknown) {
        throw new Error((e as ReturnType<typeof spawnSync>).stdout.toString('utf-8'));
      }
    }).not.toThrow();
  });

  it('should overwrite the normal output file with redirects to a dev-server', async () => {
    // eslint-disable-next-line node/no-missing-require
    expect(await promises.readFile(require.resolve('./target/first.js'), 'utf8')).toMatchInlineSnapshot(`
      "!function() { var script = document.createElement('script');
      script.src = 'first.js';
      script.async = false;
      script.crossOrigin = 'anonymous';
      document.head.appendChild(script); }();"
    `);
    // eslint-disable-next-line node/no-missing-require
    expect(await promises.readFile(require.resolve('./target/second.js'), 'utf8')).toMatchInlineSnapshot(`
      "!function() { var script = document.createElement('script');
      script.src = 'second.js';
      script.async = false;
      script.crossOrigin = 'anonymous';
      document.head.appendChild(script); }();"
    `);
    // eslint-disable-next-line node/no-missing-require
    expect(await promises.readFile(require.resolve('./target/third.js'), 'utf8')).toMatchInlineSnapshot(`
      "!function() { var script = document.createElement('script');
      script.src = 'third.js';
      script.async = false;
      script.crossOrigin = 'anonymous';
      document.head.appendChild(script); }();"
    `);
  });
});
