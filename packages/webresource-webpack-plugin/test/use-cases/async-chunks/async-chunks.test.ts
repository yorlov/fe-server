import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { asyncChunkLoader } from '../../fixtures/webpack-runtime-chunks';
import { getByKey, getByKeyStarsWith, getContent, getDependencies } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('async-chunks', () => {
  let stats: Stats;
  let runtime: Node;
  let app: Node;
  let asyncChunk1: Node;
  let asyncChunk2: Node;

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      runtime = getByKeyStarsWith(results.root.children, 'entry');
      app = getByKey(results.root.children, 'split_app');
      asyncChunk1 = getByKey(results.root.children, '2');
      asyncChunk2 = getByKey(results.root.children, '3');
      done();
    });
  });

  it('should create a webresource for each async chunk', () => {
    expect(app).toBeTruthy();
    expect(asyncChunk1).toBeTruthy();
    expect(asyncChunk2).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  it('should inject a WRM pre-condition checker into the webpack runtime', () => {
    // setup
    const bundleFile = fs.readFileSync(path.join(targetDir, 'runtime~app.js'), 'utf-8');
    const expectedRuntimeAdjustment = asyncChunkLoader('com.atlassian.plugin.test');

    expect(bundleFile).toContain(expectedRuntimeAdjustment);
  });

  describe('web-resource dependencies', () => {
    let entryDeps: string[];
    let appDeps: string[];
    let async1Deps: string[];
    let async2Deps: string[];

    beforeEach(() => {
      entryDeps = getContent(getDependencies(runtime));
      appDeps = getContent(getDependencies(app));
      async1Deps = getContent(getDependencies(asyncChunk1));
      async2Deps = getContent(getDependencies(asyncChunk2));
    });

    it('adds required WRM dependency only to the web-resource with the webpack runtime', () => {
      const WRM_KEY = 'com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager';
      expect(entryDeps.includes(WRM_KEY)).toEqual(true);
      expect(appDeps.includes(WRM_KEY)).not.toEqual(true);
      expect(async1Deps.includes(WRM_KEY)).not.toEqual(true);
      expect(async2Deps.includes(WRM_KEY)).not.toEqual(true);
    });

    it('adds shared provided dependencies only to the app', () => {
      const UNDERSCORE_KEY = 'com.atlassian.plugin.jslibs:underscore-1.4.4';
      expect(entryDeps.includes(UNDERSCORE_KEY)).not.toEqual(true);
      expect(appDeps.includes(UNDERSCORE_KEY)).toEqual(true);
      expect(async1Deps.includes(UNDERSCORE_KEY)).not.toEqual(true);
      expect(async2Deps.includes(UNDERSCORE_KEY)).not.toEqual(true);
    });

    it('adds async-chunk-only deps only to the async-chunk-webresource', () => {
      const JQUERY_KEY = 'jira.webresources:jquery';
      expect(entryDeps.includes(JQUERY_KEY)).not.toEqual(true);
      expect(appDeps.includes(JQUERY_KEY)).not.toEqual(true);
      expect(async1Deps.includes(JQUERY_KEY)).toEqual(true);
      expect(async2Deps.includes(JQUERY_KEY)).not.toEqual(true);
    });
  });
});
