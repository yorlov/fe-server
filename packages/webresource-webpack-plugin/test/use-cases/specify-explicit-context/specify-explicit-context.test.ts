import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { findKeyLike, getContent, getContext } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('specify-explicit-context', () => {
  let stats: Stats;
  let wrNodes: Node[];

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      wrNodes = results.root.children.filter((n) => n.attributes.key.startsWith('entry'));
      done();
    });
  });

  it('should run without error', () => {
    expect(wrNodes).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  it('should add additional contexts as specified in the contextMap', () => {
    const wrWithGoodConfig = findKeyLike(wrNodes, 'good-newcontexts');
    const contexts = getContent(getContext(wrWithGoodConfig));
    expect(contexts).toBeTruthy();
    expect(contexts).toHaveLength(3);
    expect(contexts).toContain('some:weird:context');
    expect(contexts).toContain('foo.bar');
    expect(contexts).toContain('app-good-newcontexts');
  });

  it('should add the entrypoint name as a context when there is no contextMap config for it', () => {
    const wrWithImplicitConfig = findKeyLike(wrNodes, 'good-implicit');
    const contexts = getContent(getContext(wrWithImplicitConfig));
    expect(contexts).toBeTruthy();
    expect(contexts).toHaveLength(1);
    expect(contexts).toContain('app-good-implicit');
  });

  it('should ignore non-array configuration values gracefully', () => {
    const wrWithBadConfig = findKeyLike(wrNodes, 'bad-objectlike');
    const contexts = getContent(getContext(wrWithBadConfig));
    expect(contexts).toBeTruthy();
    expect(contexts).toHaveLength(1);
    expect(contexts).toContain('app-bad-objectlike');
  });

  it('should ignore falsy configuration values gracefully', () => {
    const wrWithBadConfig = findKeyLike(wrNodes, 'bad-falsy');
    const contexts = getContent(getContext(wrWithBadConfig));
    expect(contexts).toBeTruthy();
    expect(contexts).toHaveLength(1);
    expect(contexts).toContain('app-bad-falsy');
  });

  it('should ignore non-string context names gracefully', () => {
    const wrWithBadValues = findKeyLike(wrNodes, 'bad-emptyvalues');
    const contexts = getContent(getContext(wrWithBadValues));
    expect(contexts).toBeTruthy();
    expect(contexts).toHaveLength(2);
    expect(contexts).toContain('app-bad-emptyvalues');
    expect(contexts).toContain('foo.bar');
  });
});
