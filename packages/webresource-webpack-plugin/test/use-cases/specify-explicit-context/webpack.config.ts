import path from 'path';
import type { Configuration } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

const config: Configuration = {
  mode: 'development',
  entry: {
    'app-good-newcontexts': path.join(FRONTEND_SRC_DIR, 'app.js'),
    'app-good-implicit': path.join(FRONTEND_SRC_DIR, 'app.js'),
    'app-bad-emptyarray': path.join(FRONTEND_SRC_DIR, 'app.js'),
    'app-bad-emptyvalues': path.join(FRONTEND_SRC_DIR, 'app.js'),
    'app-bad-objectlike': path.join(FRONTEND_SRC_DIR, 'app.js'),
    'app-bad-falsy': path.join(FRONTEND_SRC_DIR, 'app.js'),
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'com.atlassian.plugin.test',
      xmlDescriptors: path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
      addEntrypointNameAsContext: true,
      contextMap: {
        'app-good-newcontexts': ['some:weird:context', 'foo.bar'],
        'app-bad-emptyarray': [],
        // @ts-expect-error Expected error due to malformed types
        'app-bad-emptyvalues': [false, '', undefined, 'foo.bar'],
        // @ts-expect-error Expected error due to malformed types
        'app-bad-objectlike': {},
        // @ts-expect-error Expected error due to malformed types
        'app-bad-falsy': '',
      },
      verbose: false,
    }),
  ],
  output: {
    filename: '[name].js',
    path: OUTPUT_DIR,
  },
};

export default config;
