import fs from 'fs';
import path from 'path';
import webpack from 'webpack';

import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('cyclic', () => {
  it('compiles an xml file', (done) => {
    webpack(config, (err, stats) => {
      if (err) {
        throw err;
      }
      expect(stats!.hasErrors()).toEqual(false);
      expect(stats!.hasWarnings()).toEqual(false);
      expect(fs.existsSync(webresourceOutput)).toEqual(true);
      done();
    });
  });
});
