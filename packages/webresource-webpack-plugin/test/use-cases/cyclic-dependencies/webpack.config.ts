import path from 'path';
import type { Configuration } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.resolve(__dirname, 'src');
const OUTPUT_DIR = path.resolve(__dirname, 'target');

const config: Configuration = {
  mode: 'development',
  context: FRONTEND_SRC_DIR,
  entry: {
    'cyclic-entry': path.join(FRONTEND_SRC_DIR, 'root.js'),
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'com.atlassian.plugin.test',
      contextMap: { 'cyclic-entry': [''] },
      xmlDescriptors: path.join(__dirname, 'target', 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
      verbose: false,
    }),
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(OUTPUT_DIR),
  },
};

export default config;
