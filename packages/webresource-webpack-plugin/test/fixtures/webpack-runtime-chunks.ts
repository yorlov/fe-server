import { webpack5or4 } from '../../src/helpers/conditional-logic';
import requireEnsureShim from '../../src/shims/require-ensure-shim';
import runtimeLoadShim from '../../src/shims/runtime-load-shim';

const linePrefix = '/******/ ';
const templateShim = webpack5or4(
  () => ({
    tabs: 4,
    generate: runtimeLoadShim,
  }),
  () => ({
    tabs: 2,
    generate: requireEnsureShim,
  }),
);

export const asyncChunkLoader = (pluginKey: string) => {
  const { generate, tabs } = templateShim;
  const templateCode = generate(pluginKey, false, '').trim();
  return (
    templateCode
      .split('\n')
      // webpack indents empty lines with only have the tabs of non-empty lines
      .map((line) => linePrefix + '\t'.repeat(line !== '' ? tabs : tabs / 2) + line)
      .join('\n')
  );
};
