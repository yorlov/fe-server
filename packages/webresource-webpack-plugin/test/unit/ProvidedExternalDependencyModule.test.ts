import ProvidedExternalDependencyModule from '../../src/webpack-modules/ProvidedExternalDependencyModule';

describe('ProvidedExternalDependencyModule', () => {
  describe('libIdent', () => {
    it("should specify a 'libIdent' method used by webpack to create a unique id", () => {
      const pedm = new ProvidedExternalDependencyModule(
        { var: 'something', amd: 'something' },
        'some-dependency',
        'amd',
      );
      expect(pedm.libIdent()).toBeTruthy();
    });

    it('should create deterministic ids based on specified params', () => {
      const firstProvidedModule = new ProvidedExternalDependencyModule(
        { var: 'something', amd: 'something' },
        'some-dependency',
        'amd',
      );
      const secondProvidedModule = new ProvidedExternalDependencyModule(
        { var: 'something', amd: 'something' },
        'some-dependency',
        'amd',
      );
      expect(firstProvidedModule.libIdent()).toBe(secondProvidedModule.libIdent());
    });

    it('should return a unique value for unique constructor params', () => {
      const firstProvidedModule = new ProvidedExternalDependencyModule(
        { var: 'something', amd: 'something' },
        'some-dependency',
        'amd',
      );
      const secondProvidedModule = new ProvidedExternalDependencyModule(
        { var: 'something-else', amd: 'something-else' },
        'some-dependency',
        'amd',
      );
      const thirdProvidedModule = new ProvidedExternalDependencyModule(
        { var: 'something', amd: 'something' },
        'some-other-dependency',
        'amd',
      );
      expect(firstProvidedModule.libIdent()).not.toBe(secondProvidedModule.libIdent());
      expect(firstProvidedModule.libIdent()).not.toBe(thirdProvidedModule.libIdent());
      expect(secondProvidedModule.libIdent()).not.toBe(thirdProvidedModule.libIdent());
    });
  });
});
