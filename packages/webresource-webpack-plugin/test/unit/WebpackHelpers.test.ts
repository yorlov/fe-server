import type { Compiler } from 'webpack';

import { isRunningInProductionMode } from '../../src/WebpackHelpers';

const env = Object.assign({}, process.env);

const stubCompiler = (mode: string): Compiler => ({ options: { mode } } as Compiler);

describe('isRunningInProductionMode', () => {
  afterEach(() => {
    process.env = env;
  });

  it('determines production mode from compiler settings', function () {
    expect(isRunningInProductionMode(stubCompiler('production'))).toEqual(true);
  });

  it('returns false if mode is different than production', function () {
    expect(isRunningInProductionMode(stubCompiler(''))).toEqual(false);
  });

  it('fallbacks to node.env when mode set to none', function () {
    process.env.NODE_ENV = 'production';
    expect(isRunningInProductionMode(stubCompiler('none'))).toEqual(true);
  });
});
