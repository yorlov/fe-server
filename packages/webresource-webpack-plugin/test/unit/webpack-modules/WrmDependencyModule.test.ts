import WrmDependencyModule from '../../../src/webpack-modules/WrmDependencyModule';

describe('WrmDependencyModule', () => {
  const type = 'static';
  const resourceKey = 'my.atlassian.plugin';

  it('should accept valid web-resource key', () => {
    const dependency = `${resourceKey}:my-web-resource`;
    const module = new WrmDependencyModule(dependency, type, resourceKey);
    expect(module.getDependency()).toEqual(dependency);
  });

  it('should add resource key when ":" is missing', () => {
    const firstDependency = 'my-web-resource';
    const firstModule = new WrmDependencyModule(firstDependency, type, resourceKey);
    expect(firstModule.getDependency()).toEqual(`${resourceKey}:${firstDependency}`);

    const secondDependency = 'my.web.resource';
    const secondModule = new WrmDependencyModule(secondDependency, type, resourceKey);
    expect(secondModule.getDependency()).toEqual(`${resourceKey}:${secondDependency}`);
  });
});
