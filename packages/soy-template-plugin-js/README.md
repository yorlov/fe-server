# @atlassian/soy-template-plugin-js

[Atlassian soy-template-plugin][source] JAR's JavaScript as a NPM module.

## Prerequisites

JDK 1.8+

## Versioning of this package

The version of the NPM package is matching the version of the [Maven Soy CLI package][mvn-soy-cli].

[source]: https://bitbucket.org/atlassian/atlassian-soy-templates/src/master/
[mvn-soy-cli]: https://packages.atlassian.com/content/repositories/atlassian-public/com/atlassian/soy/atlassian-soy-cli-support/
