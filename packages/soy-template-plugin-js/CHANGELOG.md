# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### 5.3.2 (2022-02-10)

**Note:** Version bump only for package @atlassian/soy-template-plugin-js

### [5.3.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-template-plugin-js@5.3.1..@atlassian/soy-template-plugin-js@5.3.0) (2021-10-14)

**Note:** Version bump only for package @atlassian/soy-template-plugin-js

## [5.3.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-template-plugin-js@5.3.0..@atlassian/soy-template-plugin-js@5.0.2) (2021-09-28)

### Features

- **soy-template-plugin-js:** Update to the latest version of soy CLI support ([d3e698a](https://bitbucket.org/atlassianlabs/fe-server/commits/d3e698ab72d09fbd6404aefff2578c01d92928aa))
- **soy-template-plugin-js:** update version of the Maven soy dependencies ([59db2a6](https://bitbucket.org/atlassianlabs/fe-server/commits/59db2a6df50b6b9235294bcc81e79c56248df619))

### Bug Fixes

- use "prepare" lifecycle script rather than "prepublishOnly" ([0a54d40](https://bitbucket.org/atlassianlabs/fe-server/commits/0a54d40e06673b57b3d2c5bd16fa2de9c834ce5c))

### [5.0.2](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/soy-template-plugin-js@5.0.2..@atlassian/soy-template-plugin-js@5.0.0) (2021-09-27)

### Bug Fixes

- **soy-loader:** Fix broken release of soy-template-plugin-js package ([fa70460](https://bitbucket.org/atlassianlabs/fe-server/commits/fa70460778d57e41d658c6af12bb1b53fed94880))

## 5.0.0 (2021-09-24)

### ⚠ BREAKING CHANGES

- The _.jar and _.js files were moved from the `/src` to `/dist` directory

### Features

- use "/dist" directory instead of "src" to store \*.jar files ([8d72444](https://bitbucket.org/atlassianlabs/fe-server/commits/8d72444a9be88d2967189cc62b2aa4735b8aef85))
