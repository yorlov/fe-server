const execa = require('execa');
const path = require('path');

const root = path.resolve(__dirname, '../..');

/**
 * @return {import("execa").ExecaChildPromise}
 */
function compileProject() {
  return execa('npm', ['run', 'build'], {
    cwd: root,
  });
}

module.exports = { compileProject };
