/**
 * @jest-environment jsdom
 *
 * This test suite checks the backward-compatibility with WRM < 4.2.0
 */
const React = require('react');
const { render, screen } = require('@testing-library/react');
const { compileProject } = require('./helpers/project');

// Mock the runtime module
jest.mock(
  'wrm/i18n',
  () => {
    throw new Error('wrm/i18n module is not available');
  },
  {
    virtual: true,
  },
);

jest.mock('wrm/format', () => require('../format'), {
  virtual: true,
});

describe('wrm-react-i18n integration', () => {
  let consoleWarnSpy;

  beforeAll(async () => {
    console.debug('Compiling project...');
    await compileProject();
    console.debug('Project compiled');

    consoleWarnSpy = jest.spyOn(console, 'warn');
  });

  afterEach(() => {
    consoleWarnSpy.mockReset();
  });

  describe('compatibility with WRM < 4.2.0 where "wrm/i18n" is not available', () => {
    it('should show a warning and not throw', () => {
      // given
      const module = require('../wrm-react-i18n');

      // when
      module.I18n.getText('foo');

      // then
      expect(console.warn).toHaveBeenCalledWith(
        'Call to "getText" function was not replaced with either raw translation or call to "format" function. Have you included the "jsI18n" transformation in Web Resource Manager?',
      );
    });

    it('should replace the translation with React components', () => {
      // given
      const greetings = <em>Alice</em>;
      const name = <strong>Bob</strong>;
      const phrase = '{0} and {1}';

      // when
      const module = require('../wrm-react-i18n');
      const result = module.I18n.getText(phrase, greetings, name);
      render(result);

      // then
      // Find the exact HTML output
      const element = screen.getByText(
        (content, element) => element.innerHTML === '<em>Alice</em> and <strong>Bob</strong>',
      );

      expect(element).toBeDefined();
      expect(element.textContent).toEqual('Alice and Bob');
    });
  });
});
