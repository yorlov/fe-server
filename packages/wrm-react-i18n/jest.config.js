const pkg = require('./package.json');
const baseConfig = require('../../jest.config.js');

module.exports = {
  ...baseConfig,
  projects: ['.'],
  displayName: pkg.name,

  modulePathIgnorePatterns: [
    '/dist/',

    // Ignore the changes on the modules that can be generated during the tests run
    // while we run the `npm run build`.
    '<rootDirectory>/format.js',
    '<rootDirectory>/i18n.js',
    '<rootDirectory>/wrm-react-i18n.js',
  ],
};
