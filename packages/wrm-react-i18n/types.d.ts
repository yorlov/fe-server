declare module '@atlassian/wrm-react-i18n' {
  export function format(message: string, ...args: ReactElement[]): ReactElement;

  export const I18n = {
    getText: (message: string, ...args: Array<ReactElement | string>) => ReactElement,
  };
}
