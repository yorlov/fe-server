module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        modules: false,
        targets: {
          node: process.versions.node,
        },
      },
    ],
    '@babel/preset-react',
  ],
};
