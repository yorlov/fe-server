# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [2.0.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@2.0.1..@atlassian/wrm-react-i18n@2.0.0) (2022-02-10)

### Bug Fixes

- [SPFE-900](https://ecosystem.atlassian.net/browse/SPFE-900): relax requirement on WRM version for wrm-react-i18n pkg ([a14fae3](https://bitbucket.org/atlassianlabs/fe-server/commits/a14fae33540965f06eae6aebc5128deb1281d2f3))

## 2.0.0 (2022-02-10)

### ⚠ BREAKING CHANGES

- use wrm/i18n instead of wrm/format
- make clear that min. supported version of WRM is 4.2

### Features

- [SPFE-867](https://ecosystem.atlassian.net/browse/SPFE-867) Update rollup dependency ([0d6a0a9](https://bitbucket.org/atlassianlabs/fe-server/commits/0d6a0a9156869b8551d35a8a3072e33b391a8e49))

### Bug Fixes

- [SPFE-867](https://ecosystem.atlassian.net/browse/SPFE-867) Fix rollup warnings about missing external modules ([e8a7998](https://bitbucket.org/atlassianlabs/fe-server/commits/e8a7998bbd2931bbaae18b159dccc3e8b49595fb))
- [SPFE-900](https://ecosystem.atlassian.net/browse/SPFE-900) Support WRM 5.3+ two-phase transforms i18n in wrm-react-i18n package ([033f0d3](https://bitbucket.org/atlassianlabs/fe-server/commits/033f0d33c58d88729b37ddeb501192a40f948021))

### 1.0.11 (2021-11-17)

**Note:** Version bump only for package @atlassian/wrm-react-i18n

### [1.0.10](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@1.0.10..@atlassian/wrm-react-i18n@1.0.9) (2021-10-14)

**Note:** Version bump only for package @atlassian/wrm-react-i18n

### [1.0.9](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@1.0.9..@atlassian/wrm-react-i18n@1.0.8) (2021-10-08)

**Note:** Version bump only for package @atlassian/wrm-react-i18n

### [1.0.8](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@1.0.8..@atlassian/wrm-react-i18n@1.0.7) (2021-09-24)

**Note:** Version bump only for package @atlassian/wrm-react-i18n

### [1.0.7](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@1.0.7..@atlassian/wrm-react-i18n@1.0.6) (2021-09-22)

- Update internal dependencies

### [1.0.6](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@1.0.6..@atlassian/wrm-react-i18n@1.0.4) (2021-08-27)

**Note:** Version bump only for package @atlassian/wrm-react-i18n

### [1.0.2](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@1.0.2..@atlassian/wrm-react-i18n@1.0.1) (2021-08-26)

### Bug Fixes

- **release:** fix release script for the wrm package ([482efe2](https://bitbucket.org/atlassianlabs/fe-server/commits/482efe2d2262209926a775e56da3df6a89c54f95))

### [1.0.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@1.0.1..@atlassian/wrm-react-i18n@1.0.0) (2021-08-26)

**Note:** We updated internal dependencies

## [1.0.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@1.0.0..@atlassian/wrm-react-i18n@0.8.1) (2021-07-01)

### ⚠ BREAKING CHANGES

- We removed support for Node 10. The minimum required version is Node 12 now.

### Features

- drop support for Node 10 ([2ef1e83](https://bitbucket.org/atlassianlabs/fe-server/commits/2ef1e831cc30f0ec78884d72815d773ae401cc7e))

### [0.8.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@0.8.1..@atlassian/wrm-react-i18n@0.8.0) (2021-03-08)

### Bug Fixes

- **deps:** Update Node version ([1dadcca](https://bitbucket.org/atlassianlabs/fe-server/commits/1dadcca42ac461410dabd8e844249cdf80e516a9))

## 0.8.0 (2021-01-18)

**Note:** Version bump only for package @atlassian/wrm-react-i18n

## 0.8.0-alpha.0 (2021-01-13)

- feat: SPFE-267 Add support for webpack 5 ([f73b853](https://bitbucket.org/atlassianlabs/fe-server/commits/f73b853))

## 0.7.0 (2020-06-25)

- chore: update dependencies ([38f5a33](https://bitbucket.org/atlassianlabs/fe-server/commits/38f5a33))
- chore: update dependencies ([c3d3caa](https://bitbucket.org/atlassianlabs/fe-server/commits/c3d3caa))

## 0.6.0 (2020-05-11)

- chore: Add note about minimal requirements ([ee38cec](https://bitbucket.org/atlassianlabs/fe-server/commits/ee38cec))
- chore: downgrade to Node version 10 ([9fc7bab](https://bitbucket.org/atlassianlabs/fe-server/commits/9fc7bab))
- chore: fix running prettier ([09867f3](https://bitbucket.org/atlassianlabs/fe-server/commits/09867f3))
- chore: Update minimal Node version to version 12 so we can support 'String.proptotype.matchall' ([cb0ed25](https://bitbucket.org/atlassianlabs/fe-server/commits/cb0ed25))

## [0.5.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@0.5.1..@atlassian/wrm-react-i18n@0.5.0) (2020-04-21)

### Bug Fixes

- fix missing types when creating production bundle ([157de4c](https://bitbucket.org/atlassianlabs/fe-server/commits/157de4c40c2933be96d47e0582060ad027ad16cd))

# [0.5.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@0.5.0..@atlassian/wrm-react-i18n@0.4.5) (2020-04-21)

### Features

- TS types for wrm-react-i18n ([eda617c](https://bitbucket.org/atlassianlabs/fe-server/commits/eda617c1c78e9085f7b46ff1836a9e6e87b9d38e))

## [0.4.5](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@0.4.5..@atlassian/wrm-react-i18n@0.4.4) (2020-02-07)

### Bug Fixes

- Publish package again since version 0.4.4 didn't include JS files

## [0.4.4](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-react-i18n@0.4.4..@atlassian/wrm-react-i18n@0.4.3) (2020-01-27)

**Note:** Version bump only for package @atlassian/wrm-react-i18n
