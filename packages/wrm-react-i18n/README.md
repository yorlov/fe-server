# @atlassian/wrm-react-i18n

![node version](https://img.shields.io/node/v/@atlassian/wrm-react-i18n.svg)
![webpack peer dependency version](https://img.shields.io/npm/dependency-version/@atlassian/wrm-react-i18n/peer/webpack.svg)
![atlassian-webresource-webpack-plugin peer dependency version](https://img.shields.io/npm/dependency-version/@atlassian/wrm-react-i18n/peer/atlassian-webresource-webpack-plugin.svg)
![react peer dependency version](https://img.shields.io/npm/dependency-version/@atlassian/wrm-react-i18n/peer/react.svg)
![npm downloads](https://img.shields.io/npm/dt/@atlassian/wrm-react-i18n.svg)

An internationalization i18n helper for WRM and React that can be used in Atlassian Server products and plugins.

## Motivation

If your work with the modern Front-End Server and [React](https://reactjs.org/) library there is a good chance you are
already using `I18n.getText()` helper to internationalize your application. The bad news is, you can't fully use React
components when you want to substitute the phrase arguments.

The `@atlassian/wrm-react-i18n` can help you with solving that problem. It's a small library build on top of the
`I18n.getText()` function that adds the missing gap between WRM translation system and React.

For more information about the translations system check the Atlassian Development documentation page:

- [Internationalising your plugin](https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin/)

## Installation

```sh
npm install @atlassian/wrm-react-i18n --save
```

## Usage example

### `my-translations.properties`

```properties
my.phrase.id=Hello {0} {1}!
```

### `MyFooComponent.jsx`

```jsx
import { I18n } from '@atlassian/wrm-react-i18n';
import BarComponent from './BarComponent.jsx';

class MyFooComponent extends React.Component {
  render() {
    return <p>{I18n.getText('my.phrase.id', <strong>React</strong>, <BarComponent>World</BarComponent>)}</p>;
  }
}
```

## webpack config

### Atlassian Web-Resource webpack Plugin

The `@atlassian/wrm-react-i18n` package depends on the browser runtime dependency called `wrm/i18n`. This is not the
NPM package but AMD module defined in the browser runtime of a server product.

You need to use webpack and [Atlassian Web-Resource webpack Plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin)
in order to bundle your Javascript code. Take a look at the example how to provide the require dependency:

```js
// webpack.config.js
module.exports = {
  plugins: [
    new WRMPlugin({
      // your WRM config

      providedDependencies: {
        'wrm/i18n': {
          dependency: 'com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n',
          import: {
            var: 'require("wrm/i18n")',
            amd: 'wrm/i18n',
          },
        },
      },
    }),
  ],
};
```

For more information about the WRM Plugin please check [the plugin page](https://bitbucket.org/atlassianlabs/fe-server/src/webresource-webpack-plugin).

### Minification of production bundle

You need to tell webpack not to minimize the usage of `I18n.getText()`. During the runtime of the server product WRM
will transform your phrase keys into proper translations.

[Starting from version **4.26.0**](https://twitter.com/wsokra/status/1064441984532848641?lang=en) webpack is using new
default minimizer called [`Terser`](https://github.com/terser-js/terser).

Please check you webpack version before adding the required mangle exception.

#### Terser

If you are using default webpack `Terser` plugin:

```js
// webpack.config.js
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          mangle: {
            // Don't mangle usage of I18n.getText() function
            reserved: ['I18n', 'getText'],
          },
        },
      }),
    ],
  },
};
```

#### Uglify

If you are using `Uglify` use this snippet:

```js
// webpack.config.js
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          mangle: {
            // Don't mangle usage of I18n.getText() function
            reserved: ['I18n', 'getText'],
          },
        },
      }),
    ],
  },
};
```

## FAQ

### Why do I need to use this package?

There is a good chance that you actually don't need to use this package at all. If you are not using, React neither you
are not using the substitution in your translation phrases that are a React components, then you don't need to us it.

If you only want to translate a regular string like e.g. `I18n.getText('foo.bar.key)` then you can import the built-in
runtime package called `wrm/i18n`:

```js
import { i18n } from 'wrm/i18n';

console.log(I18n.getText('my.phrase.key'));
```

The `@atlassian/wrm-react-i18n` package is compatible with the `wrm/i18n` package and you should use only one of them
inside a single ESM module.

## Recipes

### Writing unit test with Jest

If you will start using `I18n.getText()` function, then you will find that your unit tests will start failing with this
error message:

> Cannot find module 'wrm/format' from 'wrm-react-i18n.js'

or

> Cannot find module 'wrm/i18n' from 'wrm-react-i18n.js'

In order to fix that you need to provide a missing module that is only available in the browser runtime when you run the
Atlassian server product:

1. Locate and edit your Jest config file e.g. `jest.config.js`
2. Add module mapping for the missing modules:

```js
module.exports = {
  moduleNameMapper: {
    '^wrm/format$': '<rootDir>/node_modules/@atlassian/wrm-react-i18n/format.js',
    '^wrm/i18n$': '<rootDir>/node_modules/@atlassian/wrm-react-i18n/i18n.js',
  },
};
```

The correct path to your `node_modules` directory might depend on the location where you store the `jest.config.js`
file. For more information about the `moduleNameMapper` options visit the [Jest documentation page](https://jestjs.io/docs/en/configuration.html#modulenamemapper-object-string-string).

### Using with `*.properties` webpack loader

This package can be used with the [`@atlassian/i18n-properties-loader`](https://www.npmjs.com/package/@atlassian/i18n-properties-loader)
when you run your code with webpack dev server.

You can check the package description for more details and learn how to integrate it with your webpack configuration.

## Additional links

- [https://www.npmjs.com/package/@atlassian/i18n-properties-loader](https://www.npmjs.com/package/@atlassian/i18n-properties-loader)
- [https://www.npmjs.com/package/@atlassian/wrm-react-i18n](https://www.npmjs.com/package/@atlassian/wrm-react-i18n)
- [https://www.npmjs.com/package/atlassian-webresource-webpack-plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin)

## Minimum requirements

This plugin is compatible with:

- webpack 4.0+ and 5.0+
- Node 12+
- React 16.0+
- [Atlassian Web-Resource webpack plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin) 4.0+ and 5.0+
- WRM 4.0+ (see [WRM usage in products](https://bitbucket.org/atlassian/platform-module-usage/wiki/atlassian-plugins-webresource.md))
