import { format, getText } from './react-i18n';

// We need to export the "format" function since it's used during the runtime as `<<namespace>>.format()`
export { format };

// We need to re-export the fake "getText" since "<<namespace>>.I18n.getText()" is used in the source code and then
// replaced to "<<namespace>>.format()" by either jsI18n transformer or @atlassian/i18n-properties-loader webpack loader.
// However, when two-phases i18n is used (since WRM 5.3+/AMPS 8.3+) "<<namespace>>.I18n.getText()" is not replaced and
// "getText" should call the original "getText" function from WRM.
export const I18n = {
  getText,
};
