import React, { Fragment, isValidElement } from 'react';
import wrmFormat from 'wrm/format'; // A module provided in browser runtime by WRM

// backward-compatibility with WRM < 4.2 where wrm/i18n is not available yet
const WrmI18n = (function () {
  try {
    return require('wrm/i18n'); // A module provided in browser runtime by WRM >= 4.2
  } catch (e) {
    return null;
  }
})();

export function format(translation, ...args) {
  let i = 0;

  const mapping = {};

  args = args.map((arg) => {
    if (isValidElement(arg)) {
      const str = `!PLACEHOLDER_${++i}!`;

      mapping[str] = arg;

      return str;
    }

    return arg;
  });

  const strParts = wrmFormat(translation, ...args).split(/(!PLACEHOLDER_\d+!)/g);

  if (strParts.length === 1) {
    return strParts.join('');
  }

  const asReactEls = strParts.map((str, i) => {
    return <Fragment key={i}>{i % 2 ? mapping[str] : str}</Fragment>;
  });

  return <Fragment>{asReactEls}</Fragment>;
}

export const getText = (...args) => {
  /**
   * What's going on here? There are two scenarios to consider:
   * 1) Regular i18n mechanism replacing "<>.getText(...)" at run-time on the back-end side;
   *    it's an error when the execution enters this function and a message in the console
   *    will be printed (coming from WRM code), in the UI raw i18n key will be displayed.
   * 2) When WRM two-phases i18n mechanism is used, it's expected that execution enters
   *    this function and we need to call the original WRM's getText function, because
   *    it holds the i18n->translation Map (the translation is done at the client-side);
   *    however this package 'wrm-react-i18n' adds a support for React components by
   *    providing a different 'format' function; hence a problem in 2) scenario, because
   *    by calling the original WRM's getText function, the original WRM's format function
   *    is also called. That's why we're swapping the WRM's format function to the
   *    React-enhanced version of it, due to the lack of a better alternative atm.
   *    The swap is done just for a single execution of WRM's getText function;
   *    all the code is synchronous, hence there's no chance of affecting other places;
   *    in other words the swap is atomic / completely transparent to the outside world.
   * 3) At run-times with WRM < 4.2, "wrm/i18n" module is not available yet, in this case
   *    falling back to the previous implementation: calling enhanced 'format' function.
   */
  // for WRM < 4.2
  if (!WrmI18n) {
    // The warning should be kept in production bundle
    if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'test') {
      console.warn(
        'Call to "getText" function was not replaced with either raw translation or call to "format" function. Have you included the "jsI18n" transformation in Web Resource Manager?',
      );
    }
    return format(...args);
  }

  // for WRM >= 4.2
  let result;
  try {
    WRM.I18n.format = format; //eslint-disable-line no-undef
    result = WrmI18n.getText.apply(null, args);
  } finally {
    WRM.I18n.format = wrmFormat; //eslint-disable-line no-undef
  }

  return result;
};
