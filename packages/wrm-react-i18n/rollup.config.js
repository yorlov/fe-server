const { nodeResolve } = require('@rollup/plugin-node-resolve');
const { babel } = require('@rollup/plugin-babel');

const { name: pkgName } = require('./package.json');

const commonConfig = {
  plugins: [
    nodeResolve(),
    babel({
      exclude: 'node_modules/**', // only transpile our source code
      babelHelpers: 'bundled',
    }),
  ],

  external: ['react', 'wrm/i18n'],

  output: {
    globals: {
      // We don't really know if `window.React` will be available in the browser runtime but that's our best guess
      react: 'React',
      'wrm/i18n': 'WRM.I18n',
    },
  },
};

module.exports = [
  {
    ...commonConfig,
    input: 'src/index.js',
    output: {
      ...commonConfig.output,
      file: 'wrm-react-i18n.js',
      format: 'umd',
      name: pkgName,
      sourcemap: true,
    },
  },

  {
    ...commonConfig,
    input: 'src/format.js',
    output: {
      ...commonConfig.output,
      file: 'format.js',
      format: 'umd',
      name: 'wrm/format',
      sourcemap: true,
    },
  },

  {
    ...commonConfig,
    input: 'src/i18n.js',
    output: {
      ...commonConfig.output,
      file: 'i18n.js',
      format: 'umd',
      name: 'wrm/i18n',
      sourcemap: true,
    },
  },
];
