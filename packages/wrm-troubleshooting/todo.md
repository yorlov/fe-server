# TODO

## Project

- [ ] check if we are using any "sync" code that we shouldn't e.g. `fs` module

- [ ] A new line splitting should use the `EOL`: https://nodejs.org/api/os.html#os_os_eol

- [ ] Improve running webpack CLI when there is an import statement
