# Decisions log

# Checking and running `webpack` CLI command instead of `webpack-cli`

The `webpack` NPM packages provides the CLI wrapper for the `webpack-cli` command.

With `webpack` version >= 5 the supported CLI packages are extended to:

- `webpack-cli`
- `webpack-command`

However, there are bigger differences if you compare `webpack-cli` version 3 and 4:

- `webpack-cli` v3: [Commands](https://github.com/webpack/webpack-cli/blob/v3.3.12/README.md#commands)
- `webpack-cli` v4: [Available commands](https://github.com/webpack/webpack-cli/blob/master/packages/webpack-cli/README.md#available-commands)

With this knowledge we should always check for the existence of `webpack-cli` executable file.
When we want to call any `webpack` command we should also use only `webpack-cli` executable file.
