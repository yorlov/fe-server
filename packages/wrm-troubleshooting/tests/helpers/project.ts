import type { ExecaChildPromise } from 'execa';
import execa from 'execa';
import * as path from 'path';

const root = path.resolve(__dirname, '../..');

export function compileProject(): ExecaChildPromise<Error> {
  return execa('npm', ['run', 'build', '--', '--incremental'], {
    cwd: root,
  });
}
