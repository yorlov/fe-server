import { spawn } from 'child_process';
import concat from 'concat-stream';
import { EOL } from 'os';

type EnvironmentalVariables = Record<string, string>;

function createProcess(processPath: string, args: string[] = [], cwd: string, env?: EnvironmentalVariables) {
  args = [processPath].concat(args);

  return spawn('node', args, {
    cwd,
    env: Object.assign(
      {
        NODE_ENV: 'test',
        CI: process.env.CI, // Forward the env variables
        PATH: process.env.PATH, // https://maxschmitt.me/posts/error-spawn-node-enoent-node-js-child-process/
      },
      env,
    ),
  });
}

interface ExecuteOpts {
  cwd: string;
  env?: EnvironmentalVariables;
}

export function execute(processPath: string, args: string[] = [], opts: ExecuteOpts): Promise<string> {
  const { cwd, env } = opts;

  const childProcess = createProcess(processPath, args, cwd, env);
  childProcess.stdin.setDefaultEncoding('utf-8');

  return new Promise((resolve, reject) => {
    childProcess.stderr.once('data', (err) => {
      reject(err.toString());
    });

    childProcess.on('error', reject);

    childProcess.stdout.pipe(
      concat((result) => {
        resolve(result.toString());
      }),
    );
  });
}

export function getLastLine(output: string): string {
  const lines: string[] = output
    .trim()
    .split(EOL)
    .filter((line) => Boolean(line));

  return lines && lines.length > 0 ? (lines.pop() as string) : '';
}
