import type ServerMock from 'mock-http-server';

import type { IResourcesResponse, IWrmResource } from '../../src/wrm/wrmTypes';

export function configureFetchingWebResource(server: ServerMock, resources: IWrmResource[]): void {
  const response: IResourcesResponse = {
    resources,
    unparsedData: {},
    unparsedErrors: {},
  };

  // Fake WRM REST API for fetching resources meta
  const url = '/rest/webResources/1.0/resources';
  const method = 'POST';
  debugAddingResource(url, method);

  server.on({
    path: url,
    method,
    reply: {
      status: 200,
      headers: { 'content-type': 'application/json' },
      body: () => {
        debugReply(url, method);

        return JSON.stringify(response);
      },
    },
  });

  // Fake WRM REST API for fetching resource by URL
  for (const resource of resources) {
    const resourceJsResponse = 'const helloWorld = true;';

    const method = 'GET';
    debugAddingResource(resource.url, method);

    server.on({
      path: resource.url,
      method,
      reply: {
        status: 200,
        body: () => {
          debugReply(resource.url, method);

          return resourceJsResponse;
        },
      },
    });
  }
}

function debugAddingResource(url: string, method: string) {
  console.debug(`[Mock HTTP Server]: Adding ${method} resource with "${url}" url`);
}

function debugReply(url: string, method: string) {
  console.debug(`[Mock HTTP Server]: Received ${method} request to "${url}" url`);
}
