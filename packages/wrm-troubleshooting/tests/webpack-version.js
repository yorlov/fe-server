const webpack = require('webpack');
const webpackCliPkg = require('webpack-cli/package.json');

console.log(
  `Running Jest tests with webpack version ${webpack.version} and webpack-cli version ${webpackCliPkg.version}`,
);
