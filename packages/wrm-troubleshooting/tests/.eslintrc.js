module.exports = {
  rules: {
    // We don't care about those rules in tests directory
    'node/no-unpublished-import': 'off',
    'node/no-extraneous-require': 'off',
  },
};
