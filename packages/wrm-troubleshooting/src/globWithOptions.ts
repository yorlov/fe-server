import type { IOptions } from 'glob';
import glob from 'glob-promise';

const DEFAULT_IGNORED = ['**/node_modules/**', '**/target/**', '**/dist/**'];

export async function globWithOptions(pattern: string, options: IOptions = {}): Promise<string[]> {
  return glob(pattern, {
    nodir: true,
    ignore: DEFAULT_IGNORED,
    absolute: true,
    ...options,
  });
}
