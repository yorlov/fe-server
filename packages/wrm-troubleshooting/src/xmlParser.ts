import chalk from 'chalk';
import { parse as parseXml } from 'fast-xml-parser';
import { promises as fs } from 'fs';

import { getRelativePath } from './paths';
import type { ParsedXmlWithAttrs } from './types';

const readFileContent = (filePath: string) => {
  return fs.readFile(filePath, { encoding: 'utf8' });
};

export const parseXmlContent = (xmlContent: string): ParsedXmlWithAttrs | Error => {
  const xmlParseOptions = {
    arrayMode: true,
    ignoreAttributes: false,
    attrNodeName: 'attrs',
    attributeNamePrefix: '',
  };

  let root: ParsedXmlWithAttrs;
  try {
    root = parseXml(xmlContent, xmlParseOptions);
  } catch (error) {
    return new Error(`Can't parse the XML content`);
  }

  return root;
};

export async function parseXmlFromFile(filePath: string): Promise<ParsedXmlWithAttrs | Error> {
  let xmlContent: string;
  try {
    xmlContent = await readFileContent(filePath);
  } catch (e) {
    return new Error(`Can't read the content of "${chalk.green(getRelativePath(filePath))}"`);
  }

  return parseXmlContent(xmlContent);
}
