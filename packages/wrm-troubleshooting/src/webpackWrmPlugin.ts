import chalk from 'chalk';
import type { Stats } from 'fs';
import { promises as fs } from 'fs';

import { getGeneratedModuleDescriptorPaths } from './atlassianPluginXml';
import { getRelativePath, isSubDirectory } from './paths';
import { getAbsoluteScanFoldersConfig, getAtlassianPluginKeyFromPom, SCAN_FOLDERS_KEY } from './pom';
import type { PluginInstanceWithName, PomXml, PropertyOf } from './types';

export const WRM_PLUGIN_NAME = 'WrmPlugin';
export const NPM_PACKAGE_NAME = 'atlassian-webresource-webpack-plugin';

export type AtlassianWebpackWrmPluginOptions = {
  pluginKey: string;
  xmlDescriptors: string;
};

export type AtlassianWebpackWrmPluginInstanceWithOptions = PluginInstanceWithName & {
  options: AtlassianWebpackWrmPluginOptions;
};

export async function checkWrmPluginHasValidConfig(
  pomXml: PomXml,
  wrmWebpackPlugin: AtlassianWebpackWrmPluginInstanceWithOptions,
): Promise<boolean | Error> {
  const pomPluginKey = getAtlassianPluginKeyFromPom(pomXml);

  const { pluginKey: wrmPluginPluginKey } = wrmWebpackPlugin.options;

  if (pomPluginKey !== wrmPluginPluginKey) {
    const pluginKeyOption: PropertyOf<AtlassianWebpackWrmPluginOptions, 'pluginKey'> = 'pluginKey';

    const pomFile = 'pom.xml';

    // TODO: the pom.xml should be atlassian-plugin.xml file I guess
    return new Error(
      `The value of atlassian plugin key provided to the ${chalk.bold(
        WRM_PLUGIN_NAME,
      )} webpack plugin doesn't match the plugin key defined in "${chalk.green(pomFile)}" file:
 - ${chalk.bold(WRM_PLUGIN_NAME)} [${chalk.bold(pluginKeyOption)}]: "${chalk.bold.red(wrmPluginPluginKey)}"
 - ${chalk.bold(pomFile)}  [${chalk.bold('project.groupId')}].[${chalk.bold('project.artifactId')}]: "${chalk.bold.red(
        pomPluginKey,
      )}"`,
    );
  }

  return true;
}

export async function checkWrmScanFoldersIsValid(
  pomXml: PomXml,
  pomFile: string,
  wrmWebpackPlugin: AtlassianWebpackWrmPluginInstanceWithOptions,
): Promise<Error | boolean> {
  const { xmlDescriptors: wrmXmlDescriptorsFile } = wrmWebpackPlugin.options;

  // Get Scan Folders value
  const pomScanFolders = getAbsoluteScanFoldersConfig(pomXml, pomFile);

  // Check if wrmXmlDescriptorsFile is a child of Scan Folders directory
  const isInFolder = isSubDirectory(pomScanFolders, wrmXmlDescriptorsFile);

  if (!isInFolder) {
    const wrmXmlDescriptorOption: PropertyOf<AtlassianWebpackWrmPluginOptions, 'xmlDescriptors'> = 'xmlDescriptors';

    return new Error(
      `The configuration value of "${chalk.bold(SCAN_FOLDERS_KEY)}" entry in ${chalk.green(
        getRelativePath(pomFile),
      )} file doesn't overlap with the value of "${chalk.bold(wrmXmlDescriptorOption)}" in your webpack configuration:
 - ${chalk.bold(getRelativePath(pomFile))} [${chalk.bold(SCAN_FOLDERS_KEY)}]: ${chalk.bold.red(
        getRelativePath(pomScanFolders),
      )}
 - ${chalk.bold(WRM_PLUGIN_NAME)} [${chalk.bold(wrmXmlDescriptorOption)}]: "${chalk.bold.red(
        getRelativePath(wrmXmlDescriptorsFile),
      )}"`,
    );
  }

  return true;
}

export async function verifyGeneratedWebResources(
  pomFile: string,
  pomXml: PomXml,
  preBundleTimestamp: number,
): Promise<boolean | Error> {
  const pomScanFolders = getAbsoluteScanFoldersConfig(pomXml, pomFile);
  const generatedXmlPaths = await getGeneratedModuleDescriptorPaths(pomFile, pomXml);

  return verifyXmlFiles(generatedXmlPaths, preBundleTimestamp, pomScanFolders);
}

async function verifyXmlFiles(
  xmlPaths: string[],
  preBundleTimestamp: number,
  pomScanFolders: string,
): Promise<true | Error> {
  // Check if any files were generated in the directory
  if (!xmlPaths.length) {
    return new Error(
      `No *.xml files were created by webpack under the "${chalk.green(
        getRelativePath(pomScanFolders),
      )}" directory. Check if running the "webpack" CLI command shows you any errors or warnings that might cause the issue.`,
    );
  }

  // Check if the XML files were generated during the webpack bundle
  const fileStats: Stats[] = await Promise.all(xmlPaths.map<Promise<Stats>>((filePath) => fs.stat(filePath)));
  const filesAreNewer = fileStats.some((stats) => stats.mtimeMs > preBundleTimestamp);

  if (!filesAreNewer) {
    return new Error(
      `The *.xml files under the "${chalk.green(
        getRelativePath(pomScanFolders),
      )}" directory are generated by webpack CLI but are coming from the older webpack bundle. Check if running the "webpack" CLI command shows you any errors or warnings that might cause the issue.`,
    );
  }

  return true;
}
