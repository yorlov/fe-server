import path = require('path');
import chalk from 'chalk';
import { promises as fs } from 'fs';
import inquirer from 'inquirer';
import inquirerAutocompletePrompt from 'inquirer-autocomplete-prompt';
import get from 'lodash.get';

import { globWithOptions } from './globWithOptions';
import { getRelativePath } from './paths';
import type { ParsedXmlWithAttrs, PomXml } from './types';
import { parseXmlContent } from './xmlParser';

inquirer.registerPrompt('autocomplete', inquirerAutocompletePrompt);

const POM_FILE_PATTERN = 'pom.xml';
const POM_FILE_GLOB_PATTERN = `**/${POM_FILE_PATTERN}`;

export const SCAN_FOLDERS_KEY = 'Atlassian-Scan-Folders';

export async function findRootPomXmlFile(): Promise<string | undefined> {
  const [rootPomFile] = await globWithOptions(POM_FILE_PATTERN);

  return rootPomFile;
}

export async function confirmRootPomFile(pomFile: string, rootPomFileFound: boolean): Promise<string | undefined> {
  interface Answers {
    confirmRootPomFile: boolean;
  }

  const answers: Answers = await inquirer.prompt({
    type: 'confirm',
    name: 'confirmRootPomFile',
    message: `We have found ${rootPomFileFound ? 'a root ' : ''}pom file at "${chalk.green(
      getRelativePath(pomFile),
    )}". Is it the file we should be using now?`,
  });

  if (answers.confirmRootPomFile) {
    return pomFile;
  }

  return undefined;
}

async function askToSelectPomFile(fileLocations: string[]): Promise<string> {
  interface Answers {
    pomLocation: string;
  }

  const answers: Answers = await inquirer.prompt({
    // @ts-expect-error We are using inquirer plugin that is causing TS error
    type: 'autocomplete',
    name: 'pomLocation',
    message: 'Select a pom.xml file you want to use:',
    source: (answersSoFar: string[], input?: string) => {
      const userInput = input || '';

      return fileLocations
        .filter((fileLocation) => fileLocation.match(new RegExp(userInput, 'i')))
        .map((fileLocation) => ({
          name: getRelativePath(fileLocation),
          value: fileLocation,
        }));
    },
  });

  return answers.pomLocation;
}

async function askForPomFileLocation(): Promise<string | Error> {
  interface HasPomFileAnswers {
    hasPomFile: boolean;
  }

  const hasPomFileAnswers: HasPomFileAnswers = await inquirer.prompt({
    type: 'confirm',
    name: 'hasPomFile',
    message: 'Have you correctly set up and configured the Java project?',
  });

  if (!hasPomFileAnswers.hasPomFile) {
    return new Error("The Java project wasn't configured.");
  }

  interface FilePathAnswers {
    pomLocation: string;
  }

  const answers: FilePathAnswers = await inquirer.prompt({
    type: 'input',
    name: 'pomLocation',
    message: 'Please provide a path to a pom config file you want to use:',
    async validate(input: unknown): Promise<boolean | string> {
      try {
        await fs.access(input as string);

        return true;
      } catch (e) {
        // eslint-disable-next-line no-empty
      }

      return 'Cannot find or read the file from provided path. Please correct the file path.';
    },
  });

  return answers.pomLocation;
}

export async function askForPomXmlFile(rooPomFileFound: boolean): Promise<string | Error> {
  if (!rooPomFileFound) {
    console.log("We couldn't find a pom.xml file in root directory of your project. ");
  }

  let pomLocation: string | Error;

  console.log('We are now looking for the pom.xml files in your project...');
  const fileLocations: string[] = await globWithOptions(POM_FILE_GLOB_PATTERN);

  // We have found some files that are looking like potential pom files
  if (fileLocations.length) {
    pomLocation = await askToSelectPomFile(fileLocations);
  } else {
    console.log("We couldn't locate any files that look like pom.xml files in this project.");
    pomLocation = await askForPomFileLocation();
  }

  return pomLocation;
}

export function getPomXml(pomContent: string): PomXml | Error {
  const xml = parseXmlContent(pomContent);

  if (xml instanceof Error) {
    return xml;
  }

  return xml as PomXml;
}

export function getScanFoldersConfig(pomXml: PomXml): string | null {
  const pluginsPath = ['project', 'build', 'plugins'];
  const InstructionsPath = ['configuration', 'instructions'];

  // We are using the above JSON Paths to access the XML nodes from the POM file. The "0" is pointing to the first element in the array.
  // This convention is enforced by the structure of the parsed XML file.
  const buildPluginsXmlPath = pluginsPath.flatMap((key) => [key, '0']);
  const instructionsXmlPath = InstructionsPath.flatMap((key) => [key, '0']);

  // Get array of build plugins
  const buildPlugins: Array<unknown> = get(pomXml, buildPluginsXmlPath).plugin;

  // Check if the build plugin contains scan folders setting
  const scanFoldersValues = buildPlugins
    .map<string | undefined>((buildPlugin) => {
      const instructions: ParsedXmlWithAttrs | undefined = get(buildPlugin, instructionsXmlPath);

      return instructions ? (instructions[SCAN_FOLDERS_KEY] as string) : undefined;
    })
    .filter(notEmpty);

  // TODO: We are going to assume we have a single value here. yolo.
  return Array.isArray(scanFoldersValues) && scanFoldersValues.length ? scanFoldersValues[0] : null;
}

export function getAbsoluteScanFoldersConfig(pomXml: PomXml, pomFile: string): string {
  const scanFolders = getScanFoldersConfig(pomXml);
  const projectDir = path.resolve(path.dirname(pomFile));

  // @ts-expect-error We should handle the null value
  return path.join(projectDir, 'target', 'classes', scanFolders);
}

const getProject = (pomXml: PomXml): ParsedXmlWithAttrs => {
  const project = pomXml['project'] as ParsedXmlWithAttrs[];

  return project[0];
};

export const getAtlassianPluginKeyFromPom = (pomXml: PomXml): string => {
  // Naively construct the full plugin key
  const { artifactId, groupId } = getProject(pomXml);

  return `${groupId}.${artifactId}`;
};

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined;
}
