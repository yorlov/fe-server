// eslint-disable-next-line node/no-unpublished-import
import type { WebpackOptionsNormalized, WebpackPluginInstance } from 'webpack';

export interface PluginInstanceWithName extends WebpackPluginInstance {
  pluginName: string;
}

export interface WebpackEffectiveConfig extends WebpackOptionsNormalized {
  plugins: PluginInstanceWithName[];
}

type XmlAttrs = { [key: string]: string };

export type ParsedXmlWithAttrs = {
  [key: string]: string | number | ParsedXmlWithAttrs | ParsedXmlWithAttrs[];
} & { attrs: XmlAttrs };

export type PomXml = ParsedXmlWithAttrs;

export type PropertyOf<TObj, KeyT = keyof TObj> = KeyT;

export type InquirerAutocompleteItem = {
  name: string;
  value: string;
};
