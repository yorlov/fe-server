import type { Arguments } from 'yargs';

type Handler<T> = (args: Arguments<T>) => void;

/**
 * This function will wrap a yargs handler command with try/catch.
 * The async will catch any error, display error message, and exist with error code
 */
export function getAsyncHandler<ArgumentsT>(handler: Handler<ArgumentsT>): Handler<ArgumentsT> {
  return async function asyncHandler(options: Arguments<ArgumentsT>) {
    try {
      await handler(options);
      process.exit(0);
    } catch (err) {
      const error = err as Error;

      console.log(error.message);
      process.exit(1);
    }
  };
}
