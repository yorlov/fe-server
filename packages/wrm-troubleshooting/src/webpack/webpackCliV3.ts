import type { ExecaChildProcess } from 'execa';
import execa from 'execa';
import { promises as fs } from 'fs';
import path from 'path';

import type { WebpackCliInfo } from './webpackCli';

export async function runWebpackBundleWithInjectedPluginCliV3(
  webpackCliInfo: WebpackCliInfo,
  webpackConfigFile: string,
  commandTimeout: number,
): Promise<Error | ExecaChildProcess> {
  const pluginPath = path.resolve(__dirname, '../webpackPlugin/WebpackRetrieveConfigPlugin.js');

  try {
    await fs.access(pluginPath);
  } catch (e) {
    return new Error(
      'We couldn\'t locate the "WebpackRetrieveConfigPlugin" plugin. Please try reinstalling the troubleshooting tool.',
    );
  }

  // TODO: Should we pass webpack bundling modes here?
  return execa('npx', ['webpack-cli', '--config', webpackConfigFile, '--plugin', pluginPath], {
    cwd: webpackCliInfo.workingDirectory,
    timeout: commandTimeout,
  });
}

export function runWebpackBundleCliV3(
  webpackCliInfo: WebpackCliInfo,
  webpackConfigFile: string,
  commandTimeout: number,
): ExecaChildProcess {
  // TODO: Should we pass webpack bundling modes here?
  return execa('npx', ['webpack-cli', '--config', webpackConfigFile], {
    cwd: webpackCliInfo.workingDirectory,
    timeout: commandTimeout,
  });
}
