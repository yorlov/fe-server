import type { ExecaChildProcess } from 'execa';
import execa from 'execa';
import { promises as fs } from 'fs';
import path from 'path';

import type { WebpackCliInfo } from './webpackCli';

export async function runWebpackBundleWithInjectedPluginCliV4(
  webpackCliInfo: WebpackCliInfo,
  webpackConfigFile: string,
  commandTimeout: number,
): Promise<Error | ExecaChildProcess> {
  const configPath = path.resolve(__dirname, '../webpackPlugin/webpack.config.js');

  try {
    await fs.access(configPath);
  } catch (e) {
    return new Error(
      'We couldn\'t locate the "WebpackRetrieveConfigPlugin" plugin. Please try reinstalling the troubleshooting tool.',
    );
  }

  // TODO: Should we pass webpack bundling modes here?
  return execa('npx', ['webpack-cli', 'build', '--config', webpackConfigFile, configPath, '--merge'], {
    cwd: webpackCliInfo.workingDirectory,
    timeout: commandTimeout,
  });
}

export function runWebpackBundleCliV4(
  webpackCliInfo: WebpackCliInfo,
  webpackConfigFile: string,
  commandTimeout: number,
): ExecaChildProcess {
  // TODO: Should we pass webpack bundling modes here?
  return execa('npx', ['webpack-cli', 'bundle', '--config', webpackConfigFile], {
    cwd: webpackCliInfo.workingDirectory,
    timeout: commandTimeout,
  });
}
