import chalk from 'chalk';
import type { ExecaError } from 'execa';
import { promises as fs } from 'fs';
import inquirer from 'inquirer';
import inquirerAutocompletePrompt from 'inquirer-autocomplete-prompt';

import { globWithOptions } from '../globWithOptions';
import { getRelativePath } from '../paths';
import { getTimeoutError } from '../steps/types';
import type { InquirerAutocompleteItem, WebpackEffectiveConfig } from '../types';
import type { WebpackCliInfo } from './webpackCli';
import { getWebpackCliInfo, runWebpackBundle, runWebpackBundleWithInjectedPlugin } from './webpackCli';
import { WebpackError } from './webpackError';

inquirer.registerPrompt('autocomplete', inquirerAutocompletePrompt);

const WEBPACK_CONFIG_ROOT_FILE_PATTERN = 'webpack.config.*';
const WEBPACK_CONFIG_FILE_GLOB_PATTERN = '**/webpack.*';

export async function findRootWebpackConfigFile(): Promise<string | undefined> {
  const files = await globWithOptions(WEBPACK_CONFIG_ROOT_FILE_PATTERN);

  if (files.length) {
    // There is always a one root file
    return files[0];
  }

  return undefined;
}

export async function confirmRootWebpackConfig(
  webpackConfigFile: string,
  rootWebpackConfigFileFound: boolean,
): Promise<string | undefined> {
  interface Answers {
    confirmRootWebpackConfig: boolean;
  }

  const answers: Answers = await inquirer.prompt({
    type: 'confirm',
    name: 'confirmRootWebpackConfig',
    message: `We have found ${rootWebpackConfigFileFound ? 'a root ' : ''}webpack config file at "${chalk.green(
      getRelativePath(webpackConfigFile),
    )}". Is it the file we should be using now?`,
  });

  if (answers.confirmRootWebpackConfig) {
    return webpackConfigFile;
  }

  return undefined;
}

async function askToSelectWebpackConfig(fileLocations: string[]): Promise<string> {
  interface Answers {
    webpackLocation: string;
  }

  const answers: Answers = await inquirer.prompt({
    // @ts-expect-error We are using inquirer plugin that is causing TS error
    type: 'autocomplete',
    name: 'webpackLocation',
    message: 'Select a webpack config file you want to use:',
    source: (answersSoFar: string[], input?: string): InquirerAutocompleteItem[] => {
      const userInput = input || '';

      return fileLocations
        .filter((fileLocation) => fileLocation.match(new RegExp(userInput, 'i')))
        .map<InquirerAutocompleteItem>((fileLocation) => ({
          name: getRelativePath(fileLocation),
          value: fileLocation,
        }));
    },
  });

  return answers.webpackLocation;
}

async function askForWebpackConfigLocation(): Promise<string | Error> {
  interface HasWebpackConfigAnswers {
    hasWebpackConfig: boolean;
  }

  const hasWebpackConfigAnswers: HasWebpackConfigAnswers = await inquirer.prompt({
    type: 'confirm',
    name: 'hasWebpackConfig',
    message: 'Have you installed and configured the webpack for this project?',
  });

  if (!hasWebpackConfigAnswers.hasWebpackConfig) {
    return new Error("The webpack wasn't configured for this project.");
  }

  interface FilePathAnswers {
    webpackLocation: string;
  }

  const answers: FilePathAnswers = await inquirer.prompt({
    type: 'input',
    name: 'webpackLocation',
    message: 'Please provide a path to a webpack config file you want to use:',
    async validate(input: unknown): Promise<boolean | string> {
      try {
        await fs.access(input as string);

        return true;
      } catch (e) {
        // eslint-disable-next-line no-empty
      }

      return 'Cannot find or read the file from provided path. Please correct the file path.';
    },
  });

  return answers.webpackLocation;
}

export async function askForWebpackConfig(rooWebpackConfigFileFound: boolean): Promise<string | Error> {
  if (!rooWebpackConfigFileFound) {
    console.log("We couldn't find a webpack config file in root directory of your project. ");
  }

  let webpackLocation: string | Error;

  console.log('We are now looking for the webpack config files in your project...');
  const fileLocations: string[] = await globWithOptions(WEBPACK_CONFIG_FILE_GLOB_PATTERN);

  // We have found some files that are looking like potential webpack configs
  if (fileLocations.length) {
    webpackLocation = await askToSelectWebpackConfig(fileLocations);
  } else {
    console.log("We couldn't locate any files that look like webpack config files in this project.");
    webpackLocation = await askForWebpackConfigLocation();
  }

  return webpackLocation;
}

export interface WebpackConfigResult {
  effectiveConfig: WebpackEffectiveConfig;
  cliInfo: WebpackCliInfo;
}

export async function retrieveEffectiveWebpackConfig(
  webpackConfigFile: string,
  commandTimeout: number,
): Promise<WebpackConfigResult | WebpackError | Error> {
  const webpackCliInfo = await getWebpackCliInfo(webpackConfigFile, commandTimeout);

  if (webpackCliInfo instanceof Error) {
    return webpackCliInfo;
  }

  console.log('👀 We will retrieve webpack configuration now. This might take some time...');

  let output: string;

  try {
    const result = await runWebpackBundleWithInjectedPlugin(webpackCliInfo, webpackConfigFile, commandTimeout);

    if (result instanceof Error) {
      return result;
    }

    ({ stdout: output } = result);
  } catch (err) {
    const error = err as Error;

    return new WebpackError(
      `Couldn't run webpack. Is the configuration "${getRelativePath(
        webpackConfigFile,
      )}" contains a valid webpack config? Try running a standalone webpack command to lear more e.g ${chalk.bold(
        'npx webpack',
      )}`,
      error.message,
    );
  }

  // Parse the output as JSON
  try {
    const jsonString = output;
    const effectiveConfig = JSON.parse(jsonString) as WebpackEffectiveConfig;

    return {
      effectiveConfig,
      cliInfo: webpackCliInfo,
    };
  } catch (e) {
    return new Error("  Couldn't parse webpack configuration. Something went wrong.");
  }
}

export async function bundleWebpack(
  webpackCliInfo: WebpackCliInfo,
  webpackConfigFile: string,
  commandTimeout: number,
): Promise<void | Error | WebpackError> {
  // TODO: Should we pass webpack bundling modes here?
  try {
    await runWebpackBundle(webpackCliInfo, webpackConfigFile, commandTimeout);
  } catch (err) {
    const error = err as ExecaError;

    if (error.timedOut) {
      return getTimeoutError('webpack', commandTimeout);
    }

    const errorMessage = error.stdout || error.message || error.shortMessage;

    return new WebpackError(`Couldn't bundle the code using the ${chalk.bold('webpack CLI')}.`, errorMessage);
  }
}
