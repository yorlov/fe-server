import WebpackRetrieveConfigPlugin from './WebpackRetrieveConfigPlugin';

module.exports = {
  plugins: [new WebpackRetrieveConfigPlugin()],
};
