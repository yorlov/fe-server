// eslint-disable-next-line node/no-unpublished-import
import type { Compiler, WebpackOptionsNormalized, WebpackPluginInstance } from 'webpack';

import type { PluginInstanceWithName, WebpackEffectiveConfig } from '../types';

function decoratePluginWithNames(config: WebpackOptionsNormalized): WebpackEffectiveConfig {
  return {
    ...config,
    plugins: config.plugins.map<PluginInstanceWithName>((plugin) => {
      const pluginName = plugin.constructor ? plugin.constructor.name : plugin.name;

      (plugin as PluginInstanceWithName).pluginName = pluginName;

      return plugin as PluginInstanceWithName;
    }),
  };
}

const unwantedWebpackPlugins = [
  /* prettier-ignore-start */
  'BundleAnalyzerPlugin' /* When we serialize the plugin it will generate circular data */,
  /* prettier-ignore-end */
];

function cleanUnwantedPlugins(config: WebpackEffectiveConfig): WebpackEffectiveConfig {
  return {
    ...config,
    plugins: config.plugins.map<PluginInstanceWithName>((plugin) => {
      const { pluginName } = plugin;

      if (!unwantedWebpackPlugins.includes(pluginName)) {
        return plugin;
      }

      // Clean-up config and only keep the plugin name.
      // We are doing that to know what webpack plugins are being used by the project and to keep type safety.
      const fakePlugin: PluginInstanceWithName = {
        pluginName,
        /* eslint-disable @typescript-eslint/no-empty-function */
        constructor() {},
        apply() {},
        /* eslint-enable @typescript-eslint/no-empty-function */
      };

      return fakePlugin;
    }),
  };
}

function normalizeConfig(config: WebpackOptionsNormalized): WebpackEffectiveConfig {
  const configWithDecoratedPlugins = decoratePluginWithNames(config);

  return cleanUnwantedPlugins(configWithDecoratedPlugins);
}

export default class WebpackRetrieveConfigPlugin implements WebpackPluginInstance {
  apply(compiler: Compiler): void {
    compiler.hooks.beforeRun.tapAsync('Retrieve Webpack Configuration', function (compiler) {
      const config = normalizeConfig(compiler.options);
      const serializedConfig = JSON.stringify(config);

      // Output the serialized webpack config to process stdout so the WRM troubleshooting tool can read it
      console.log(serializedConfig);

      // We need a timeout since there is an issue in Node related to flushing stdout
      // https://github.com/nodejs/node/issues/12921
      setTimeout(() => {
        process.exit(0);
      }, 50);
    });
  }
}

module.exports = WebpackRetrieveConfigPlugin;
