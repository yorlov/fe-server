import chalk from 'chalk';

import { FetchTimeoutError, fetchWithTimeout } from './fetchWithTimeout';
import type { IResourcesResponse, IWrmResource } from './wrmTypes';

interface IResourcesPayload {
  r: string[];
  c: string[];
  xr: string[];
  xc: string[];
}

const SUPER_BATCH_CONTEXT_KEY = '_super';

export interface SimplifiedWebResource {
  url: string;
  type: 'CSS' | 'JAVASCRIPT';
  content: string;
}

export async function fetchWebResources(
  applicationUrl: string,
  resourceKeys: string | string[],
  timeout: number,
): Promise<SimplifiedWebResource[] | Error> {
  const url = `${applicationUrl}/rest/webResources/1.0/resources`;

  process.env.DEBUG && console.debug(`Fetching web-resource from ${url}`);

  // https://hello.atlassian.net/wiki/spaces/SVRFE/pages/491199354/WRM+Front-end+API#RESOURCES
  const payload: IResourcesPayload = {
    r: Array.isArray(resourceKeys) ? resourceKeys : [resourceKeys],
    c: [],
    xr: [],
    xc: [SUPER_BATCH_CONTEXT_KEY], // Ignore super batch
  };

  let jsonResponse: IResourcesResponse;

  try {
    const response = await fetchWithTimeout(timeout, url, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    });

    if (!response.ok) {
      // TODO: What should be the error here?!
      throw new Error(response.status.toString());
    }

    jsonResponse = (await response.json()) as IResourcesResponse;
  } catch (err) {
    const error = err as Error;

    if (error.name === 'AbortError') {
      return new FetchTimeoutError(`Fetching web-resource for "${resourceKeys}" timed out`);
    }

    return Error(
      `Failed to load the web-resource for "${chalk.bold(resourceKeys)}" resource key.\nError: ${error.message}`,
    );
  }

  const { resources } = jsonResponse;
  const maybeContents = await loadWrmResources(applicationUrl, resources, timeout);

  return maybeContents.filter(notError);
}

async function loadWrmResources(
  applicationUrl: string,
  resources: IWrmResource[],
  timeout: number,
): Promise<(SimplifiedWebResource | Error | FetchTimeoutError)[]> {
  const simplifiedResourcesWithoutContent = resources.map<Omit<SimplifiedWebResource, 'content'>>((resource) => {
    const { url: urlPathname, resourceType } = resource;

    // We need to build a new absolute URL
    const url = new URL(applicationUrl);
    url.pathname = urlPathname;

    return {
      url: url.toString(),
      type: resourceType,
    };
  });

  const simplifiedResources: (SimplifiedWebResource | Error)[] = [];

  for (const resource of simplifiedResourcesWithoutContent) {
    const content = await loadWrmResource(resource.url, timeout);

    simplifiedResources.push(
      content instanceof Error
        ? content
        : {
            ...resource,
            content,
          },
    );
  }

  return simplifiedResources;
}

async function loadWrmResource(resourceUrl: string, timeout: number): Promise<string | Error> {
  try {
    const response = await fetchWithTimeout(timeout, resourceUrl, {
      method: 'get',
    });

    if (!response.ok) {
      // TODO: should we retry? Or based on error code? (do we only check if we know an instance is running?)
      // TODO: What should be the error here?!
      throw new Error(response.status.toString());
    }

    return await response.text();
  } catch (err) {
    const error = err as Error;

    if (error.name === 'AbortError') {
      return new FetchTimeoutError(`Loading web-resource "${resourceUrl}" timed out`);
    }

    return Error(`Can't load the web-resource for "${resourceUrl}" resource key. Error: ${error.message}`);
  }
}

function notError<TValue>(value: TValue | Error): value is TValue {
  return !(value instanceof Error);
}
