import * as Amplitude from '@amplitude/node';
import { promises as fs } from 'fs';
import os from 'os';
import path from 'path';

// Unique ID for WRM Troubleshooting Tool
const AMP_CLIENT_ID = '490d4e87c5c2b9c250de24d48ce09303';

// TODO: Fix me and detect local mode when we are running tool without installing it from NPM
const IS_DEV_MODE = process.env.NODE_ENV === 'test';

// Configure Amplitude
const client = Amplitude.init(AMP_CLIENT_ID);

const isObject = (value: unknown) => Object.prototype.toString.call(value) === '[object Object]';

let pkgVersion: string;

export async function trackEvent(eventName: string, customProperties: unknown = null): Promise<void> {
  // Don't send the analytics from within a dev mode
  if (IS_DEV_MODE) {
    return;
  }

  if (!pkgVersion) {
    // We need to read the package.json file manually. We can't import the package.json file using ESM syntax since if we do that
    // TSC would copy the package.json file into "dist" directory.
    // Having a copy of package.json file under "dist" directory would mess-up with the package distribution.
    try {
      const pkg = await fs.readFile(path.join(__dirname, '../../package.json'));
      const { version } = JSON.parse(pkg.toString());

      pkgVersion = version as string;
    } catch (e) {
      // eslint-disable-next-line no-empty
      pkgVersion = 'unknown';
    }
  }

  let eventProperties;

  if (isObject(customProperties)) {
    eventProperties = Object.assign({}, customProperties);
  }

  await client.logEvent({
    event_type: eventName,
    device_id: 'required-but-not-used',
    event_properties: eventProperties,

    // Non-required properties
    // https://developers.amplitude.com/docs/http-api-v2
    app_version: pkgVersion,
    platform: os.platform(),
    os_name: os.type(),
    os_version: os.release(),
  });
}
