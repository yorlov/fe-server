import { didFail } from '../steps';
import type { StepResultPayload, VerificationStepResult } from '../steps/types';
import { trackEvent } from './analytics';

export const TROUBLESHOOTING_COMMAND_RUN = 'TROUBLESHOOTING_COMMAND_RUN';
export const TROUBLESHOOTING_COMMAND_PASSED = 'TROUBLESHOOTING_COMMAND_PASSED';

const VERIFICATION_STEP = 'VERIFICATION_STEP';

export async function trackVerificationStepEvent<ResultPayloadT extends StepResultPayload, FailedErrorT extends Error>(
  stepName: string,
  validationResult: VerificationStepResult<ResultPayloadT, FailedErrorT>,
): Promise<void> {
  const eventData = {
    stepName,
  };

  if (didFail(validationResult)) {
    Object.assign(eventData, {
      stepResult: 'FAILED',
      stepError: validationResult.error,
    });
  } else {
    Object.assign(eventData, {
      stepResult: 'PASSED',
    });
  }

  await trackEvent(VERIFICATION_STEP, eventData);
}
