import * as fs from 'fs';
import * as path from 'path';
import type { Package } from 'update-notifier';
import updateNotifier from 'update-notifier';

export function checkForUpdate(): void {
  if (process.env.NODE_ENV === 'test') {
    return;
  }

  let packageVersion: string | null = null;
  let packageName: string | null = null;

  try {
    const pkg = fs.readFileSync(path.join(__dirname, '../package.json'));
    const { name, version } = JSON.parse(pkg.toString()) as Package;

    packageVersion = version;
    packageName = name;
  } catch (e) {
    console.log(e);
    // eslint-disable-next-line no-empty
  }

  if (!packageName || !packageVersion) {
    return;
  }

  const notifier = updateNotifier({
    packageName,
    packageVersion,
    updateCheckInterval: 0,
  });
  notifier.notify({
    defer: true,
  });
}
