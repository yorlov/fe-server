import chalk from 'chalk';

import { getRelativePath } from '../paths';
import { getScanFoldersConfig, SCAN_FOLDERS_KEY } from '../pom';
import type { PomXml } from '../types';
import type { StepInputPayload, StepResultPayload, VerificationStepWithInputPayload } from './types';
import { getFailedResult, getPassedResult } from './types';

interface VerifyPomConfigInputPayload extends StepInputPayload {
  pomFile: string;
  pomXml: PomXml;
}

type VerifyPomConfigResultPayload = StepResultPayload;

export const verifyPomConfigIsValid: VerificationStepWithInputPayload<
  VerifyPomConfigInputPayload,
  VerifyPomConfigResultPayload
> = async (options, payload) => {
  const { pomFile, pomXml } = payload;

  const scanFolders = getScanFoldersConfig(pomXml);

  if (!scanFolders) {
    const error = new Error(`The ${chalk.green(getRelativePath(pomFile))} file is missing the "${chalk.bold(
      SCAN_FOLDERS_KEY,
    )}" configuration.
📖 Check the guide on how to configure it:\nhttps://www.npmjs.com/package/atlassian-webresource-webpack-plugin#consuming-the-output-in-your-p2-plugin`);

    return getFailedResult(error);
  }

  return getPassedResult();
};
