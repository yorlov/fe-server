import validUrl from 'valid-url';

import type { TroubleshootingOptionsWithDefaultValues } from '../troubleshootingOptions';
import type { StepResultPayload, VerificationStep } from './types';
import { getFailedResult, getPassedResult } from './types';

type ValidateOptionsResultPayload = StepResultPayload;

export const validateOptions: VerificationStep<ValidateOptionsResultPayload> = async (
  options: TroubleshootingOptionsWithDefaultValues,
) => {
  const { applicationUrl } = options;

  if (applicationUrl) {
    const isValid = Boolean(validUrl.isWebUri(applicationUrl));

    if (!isValid) {
      const error = new Error('Provided application URL is not valid.');

      return getFailedResult(error);
    }
  }

  return getPassedResult();
};
