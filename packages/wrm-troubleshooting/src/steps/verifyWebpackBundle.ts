import { removeOldModuleDescriptors } from '../atlassianPluginXml';
import type { PomXml } from '../types';
import { bundleWebpack } from '../webpack/webpack';
import type { WebpackCliInfo } from '../webpack/webpackCli';
import type { WebpackError } from '../webpack/webpackError';
import { verifyGeneratedWebResources } from '../webpackWrmPlugin';
import type { StepInputPayload, StepResultPayload, VerificationStepWithInputPayload } from './types';
import { getFailedResult, getPassedResult } from './types';

interface RunWebpackInputPayload extends StepInputPayload {
  webpackConfigFile: string;
  webpackCliInfo: WebpackCliInfo;
  pomFile: string;
  pomXml: PomXml;
}

export interface RunWebpackResultPayload extends StepResultPayload {
  webpackConfigFile: string;
  pomFile: string;
  pomXml: PomXml;
}

export const verifyWebpackBundle: VerificationStepWithInputPayload<
  RunWebpackInputPayload,
  RunWebpackResultPayload,
  WebpackError | Error
> = async (options, payload) => {
  const { webpackConfigFile, webpackCliInfo, pomXml, pomFile } = payload;

  // Ensure we don't have any old generated module descriptors
  await removeOldModuleDescriptors(pomFile, pomXml);

  const preBundleTimestamp = Date.now();

  console.log('👀 We will run webpack bundle now. This might take some time...');
  const bundleResult = await bundleWebpack(webpackCliInfo, webpackConfigFile, options.timeout);

  if (bundleResult instanceof Error) {
    return getFailedResult(bundleResult);
  }

  // Verify if the XML files were created in the scanFolders dir
  const xmlResult = await verifyGeneratedWebResources(pomFile, pomXml, preBundleTimestamp);

  if (xmlResult instanceof Error) {
    return getFailedResult(xmlResult);
  }

  return getPassedResult();
};
