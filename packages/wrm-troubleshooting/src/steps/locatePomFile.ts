import path from 'path';

import { askForPomXmlFile, confirmRootPomFile, findRootPomXmlFile } from '../pom';
import type { StepResultPayload, VerificationStep } from './types';
import { getFailedResult, getPassedResult } from './types';

interface LocatePomResultPayload extends StepResultPayload {
  pomFile: string;
}

export const locatePomFile: VerificationStep<LocatePomResultPayload> = async (options) => {
  let pomFile: string | undefined | Error = options.pom ? path.resolve(options.pom) : await findRootPomXmlFile();
  const rootPomFileFound = options.pom ? false : Boolean(pomFile);

  // Skip asking for pom file if we have a root file
  if (!options.yes || !pomFile) {
    if (pomFile) {
      pomFile = await confirmRootPomFile(pomFile, rootPomFileFound);
    }

    if (!pomFile) {
      pomFile = await askForPomXmlFile(rootPomFileFound);
    }
  }

  if (pomFile instanceof Error) {
    return getFailedResult(pomFile);
  }

  return getPassedResult<LocatePomResultPayload>({
    pomFile,
  });
};
