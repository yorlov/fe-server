import type { ExecaError } from 'execa';
import execa from 'execa';
import fs, { promises as asyncFs } from 'fs';
import { lookpath } from 'lookpath';
import * as path from 'path';

import { MavenError } from '../maven/MavenError';
import type { StepInputPayload, StepResultPayload, VerificationStepWithInputPayload } from './types';
import { getCommandTimeoutResult, getFailedResult, getPassedResult } from './types';

interface EffectivePomInputPayload extends StepInputPayload {
  pomFile: string;
}

interface EffectivePomResultPayload extends StepResultPayload {
  pomConfig: string;
}

export const getEffectivePomConfig: VerificationStepWithInputPayload<
  EffectivePomInputPayload,
  EffectivePomResultPayload,
  MavenError | Error
> = async (options, payload) => {
  const { pomFile } = payload;

  console.log('👀 We will retrieve pom configuration now. This might take some time...');

  const pomFileWd = path.dirname(pomFile);
  const xmlConfigOutput = path.resolve(pomFileWd, '.temp-pom-config.xml');

  // We need to check if at least one of the following commands is available
  const mvnCommands = ['atlas-mvn', 'mvn'];
  let mvnCommand: string | undefined;

  for (const mvnCmd of mvnCommands) {
    mvnCommand = await lookpath(mvnCmd);

    if (mvnCommand) {
      break;
    }
  }

  // When we run integration tests locally with Jest we need to use a different maven command than `mvn`.
  // For some reason, using `mvn` command will cause this error:
  //
  //   Command failed with exit code 1: mvn help:effective-pom -Doutput=<<file-path>> --quiet
  //     mkdir: /.mvnvm: Read-only file system
  //
  // So for the integration tests we are running locally we need to use "atlas-mvn" from the Atlassian SDK.
  if (process.env.NODE_ENV === 'test' && !process.env.CI) {
    mvnCommand = 'atlas-mvn';
  }

  if (!mvnCommand) {
    return getFailedResult(
      new MavenError(
        `We can't run the "atlas-mvn" or "mvn" commands. Please ensure you have installed Atlassian SDK and one of those command can be executed.
Refer to the SDK Installation Guide: https://developer.atlassian.com/server/framework/atlassian-sdk/install-the-atlassian-sdk-on-a-linux-or-mac-system/`,
        '',
      ),
    );
  }

  try {
    await execa(mvnCommand, ['help:effective-pom', `-Doutput=${xmlConfigOutput}`, '--quiet'], {
      cwd: pomFileWd,
      timeout: options.timeout,
    });
  } catch (err) {
    const error = err as ExecaError;

    if (error.timedOut) {
      return getCommandTimeoutResult('mvn', options.timeout);
    }

    const errorMessage = error.stdout || error.message || error.shortMessage;

    return getFailedResult(new MavenError('Maven "help:effective-pom" command failed.', errorMessage));
  }

  // Verify if the effective pom file was created
  try {
    await asyncFs.access(xmlConfigOutput);
  } catch (err) {
    const error = err as Error;
    const errorMessage = error.message;

    const xmlFilename = path.basename(xmlConfigOutput);

    return getFailedResult(
      new MavenError(
        `Maven "help:effective-pom" command failed. We couldn't create the "${xmlFilename}" file with effective pom configuration.`,
        errorMessage,
      ),
    );
  }

  const pomConfig = fs.readFileSync(xmlConfigOutput, {
    encoding: 'utf-8',
  });

  // Remove the temporary file that we don't need anymore
  fs.unlinkSync(xmlConfigOutput);

  return getPassedResult<EffectivePomResultPayload>({
    pomConfig,
  });
};
