import chalk from 'chalk';
import inquirer from 'inquirer';

import type { WebpackEffectiveConfig } from '../types';
import { retrieveEffectiveWebpackConfig } from '../webpack/webpack';
import type { WebpackCliInfo } from '../webpack/webpackCli';
import type { WebpackError } from '../webpack/webpackError';
import type { StepInputPayload, StepResultPayload, VerificationStepWithInputPayload } from './types';
import { getFailedResult, getPassedResult } from './types';

interface GetEffectiveWebpackConfigInputPayload extends StepInputPayload {
  webpackConfigFile: string;
}

interface GetEffectiveWebpackConfigResultPayload extends StepResultPayload {
  webpackEffectiveConfig: WebpackEffectiveConfig;
  webpackCliInfo: WebpackCliInfo;
}

export const getEffectiveWebpackConfig: VerificationStepWithInputPayload<
  GetEffectiveWebpackConfigInputPayload,
  GetEffectiveWebpackConfigResultPayload,
  WebpackError | Error
> = async (options, payload) => {
  const { webpackConfigFile } = payload;

  if (!options.yes) {
    interface Answer {
      confirmRunningWebpack: boolean;
    }

    const answer: Answer = await inquirer.prompt({
      type: 'confirm',
      name: 'confirmRunningWebpack',
      message: `We need to run ${chalk.bold('webpack CLI')} to retrieve the configuration file. Is that fine?`,
    });

    // Silly boy!
    if (!answer.confirmRunningWebpack) {
      const error = new Error(
        `We can't proceed without running the ${chalk.bold(
          'webpack',
        )}. Please run the troubleshooting command again when you are ready to run webpack.`,
      );

      return getFailedResult(error);
    }
  }

  const webpackConfigResult = await retrieveEffectiveWebpackConfig(webpackConfigFile, options.timeout);

  if (webpackConfigResult instanceof Error) {
    return getFailedResult(webpackConfigResult);
  }

  return getPassedResult<GetEffectiveWebpackConfigResultPayload>({
    webpackCliInfo: webpackConfigResult.cliInfo,
    webpackEffectiveConfig: webpackConfigResult.effectiveConfig,
  });
};
