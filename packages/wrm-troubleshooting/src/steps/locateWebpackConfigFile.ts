import path from 'path';

import { askForWebpackConfig, confirmRootWebpackConfig, findRootWebpackConfigFile } from '../webpack/webpack';
import type { StepResultPayload, VerificationStep } from './types';
import { getFailedResult, getPassedResult } from './types';

interface LocateWebpackConfigResultPayload extends StepResultPayload {
  webpackConfigFile: string;
}

export const locateWebpackConfigFile: VerificationStep<LocateWebpackConfigResultPayload> = async (options) => {
  let webpackConfigFile: string | undefined | Error = options.webpack
    ? path.resolve(options.webpack)
    : await findRootWebpackConfigFile();

  const rootWebpackConfigFileFound = options.webpack ? false : Boolean(webpackConfigFile);

  // Skip asking for webpack config file if we have a root file
  if (!options.yes || !webpackConfigFile) {
    if (webpackConfigFile) {
      webpackConfigFile = await confirmRootWebpackConfig(webpackConfigFile, rootWebpackConfigFileFound);
    }

    if (!webpackConfigFile) {
      webpackConfigFile = await askForWebpackConfig(rootWebpackConfigFileFound);
    }
  }

  if (webpackConfigFile instanceof Error) {
    return getFailedResult(webpackConfigFile);
  }

  return getPassedResult<LocateWebpackConfigResultPayload>({
    webpackConfigFile,
  });
};
