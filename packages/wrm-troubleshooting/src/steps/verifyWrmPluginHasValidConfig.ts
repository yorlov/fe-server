import chalk from 'chalk';

import { getRelativePath } from '../paths';
import { getPomXml } from '../pom';
import type { PomXml, WebpackEffectiveConfig } from '../types';
import type { AtlassianWebpackWrmPluginInstanceWithOptions } from '../webpackWrmPlugin';
import { checkWrmPluginHasValidConfig } from '../webpackWrmPlugin';
import type { StepInputPayload, StepResultPayload, VerificationStepWithInputPayload } from './types';
import { getFailedResult, getPassedResult } from './types';

interface VerifyWrmPluginInputPayload extends StepInputPayload {
  pomFile: string;
  pomConfig: string;
  webpackEffectiveConfig: WebpackEffectiveConfig;
  wrmWebpackPlugin: AtlassianWebpackWrmPluginInstanceWithOptions;
}

interface VerifyWrmPluginResultPayload extends StepResultPayload {
  pomXml: PomXml;
}

export const verifyWrmPluginHasValidConfig: VerificationStepWithInputPayload<
  VerifyWrmPluginInputPayload,
  VerifyWrmPluginResultPayload
> = async (options, payload) => {
  const { pomConfig, pomFile, wrmWebpackPlugin } = payload;

  const pomXml = getPomXml(pomConfig);

  if (pomXml instanceof Error) {
    const error = new Error(
      `😔 We can't parse the XML from "${chalk.green(getRelativePath(pomFile))}" pom file. Bummer`,
    );

    return getFailedResult(error);
  }

  const result = await checkWrmPluginHasValidConfig(pomXml, wrmWebpackPlugin);

  if (result instanceof Error) {
    return getFailedResult(result);
  }

  return getPassedResult({
    pomXml,
  });
};
