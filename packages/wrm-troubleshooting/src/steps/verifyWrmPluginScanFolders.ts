import type { PomXml, WebpackEffectiveConfig } from '../types';
import type { AtlassianWebpackWrmPluginInstanceWithOptions } from '../webpackWrmPlugin';
import { checkWrmScanFoldersIsValid } from '../webpackWrmPlugin';
import type { StepInputPayload, StepResultPayload, VerificationStepWithInputPayload } from './types';
import { getFailedResult, getPassedResult } from './types';

interface VerifyWrmInputPayload extends StepInputPayload {
  pomFile: string;
  pomXml: PomXml;
  webpackEffectiveConfig: WebpackEffectiveConfig;
  wrmWebpackPlugin: AtlassianWebpackWrmPluginInstanceWithOptions;
}

type VerifyWrmResultPayload = StepResultPayload;

export const verifyWrmPluginScanFolders: VerificationStepWithInputPayload<
  VerifyWrmInputPayload,
  VerifyWrmResultPayload
> = async (options, payload) => {
  const { pomFile, pomXml, wrmWebpackPlugin } = payload;

  const result = await checkWrmScanFoldersIsValid(pomXml, pomFile, wrmWebpackPlugin);

  if (result instanceof Error) {
    return getFailedResult(result);
  }

  return getPassedResult();
};
