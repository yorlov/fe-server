import type { TroubleshootingOptionsWithDefaultValues } from '../troubleshootingOptions';

export enum VerificationStatus {
  FAILED,
  PASSED,
}

export type StepInputPayload = {
  [key: string]: unknown;
};

export type StepResultPayload = {
  [key: string]: unknown;
};

interface PassedVerificationStepResult<PayloadT extends StepResultPayload> {
  status: VerificationStatus.PASSED;
  payload?: PayloadT;
}

export interface FailedVerificationStepResult<ErrorT extends Error = Error> {
  status: VerificationStatus.FAILED;
  error: ErrorT;
}

export type VerificationStepResult<ResultPayloadT extends StepResultPayload, FailedErrorT extends Error = Error> =
  | PassedVerificationStepResult<ResultPayloadT>
  | FailedVerificationStepResult<FailedErrorT>;

export type VerificationStep<ResultPayloadT extends StepResultPayload, FailedErrorT extends Error = Error> = (
  options: TroubleshootingOptionsWithDefaultValues,
) => Promise<VerificationStepResult<ResultPayloadT, FailedErrorT>>;

export type VerificationStepWithInputPayload<
  InputPayloadT extends StepInputPayload,
  ResultPayloadT extends StepResultPayload,
  FailedErrorT extends Error = Error,
> = (
  options: TroubleshootingOptionsWithDefaultValues,
  payload: InputPayloadT,
) => Promise<VerificationStepResult<ResultPayloadT, FailedErrorT>>;

export function getFailedResult<ErrorT extends Error = Error>(error: ErrorT): FailedVerificationStepResult<ErrorT> {
  return {
    status: VerificationStatus.FAILED,
    error,
  };
}

export function getTimeoutError(subject: string, timeout: number): Error {
  const timeoutSeconds = Math.floor(timeout / 1000).toFixed(0);

  return new Error(
    `The ${subject} timed out after ${timeoutSeconds} seconds. Try setting the --timeout option (in seconds) to override this.`,
  );
}

export function getCommandTimeoutResult(command: string, timeout: number): FailedVerificationStepResult {
  return getFailedResult(getTimeoutError(`${command} command`, timeout));
}

export function getRestTimeoutResult(restResource: string, timeout: number): FailedVerificationStepResult {
  return getFailedResult(getTimeoutError(`${restResource} REST call`, timeout));
}

export function getPassedResult<ResultPayloadT extends StepResultPayload>(
  payload?: ResultPayloadT,
): PassedVerificationStepResult<ResultPayloadT> {
  return {
    status: VerificationStatus.PASSED,
    payload,
  };
}

export function didPass<ResultPayload extends StepResultPayload>(
  stepResult: VerificationStepResult<ResultPayload>,
): stepResult is PassedVerificationStepResult<ResultPayload> {
  return stepResult.status === VerificationStatus.PASSED;
}

export function didFail<ResultPayloadT extends StepResultPayload, FailedErrorT extends Error = Error>(
  stepResult: VerificationStepResult<ResultPayloadT, FailedErrorT>,
): stepResult is FailedVerificationStepResult<FailedErrorT> {
  return stepResult.status === VerificationStatus.FAILED;
}

export function getResultPayload<ResultPayload extends StepResultPayload>(
  stepResult: PassedVerificationStepResult<ResultPayload>,
): ResultPayload {
  return stepResult.payload as ResultPayload;
}
