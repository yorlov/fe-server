// Lists all the NPM dependencies and checks if they are deprecated.
import * as path from 'path';
import * as process from 'process';
import { $, chalk } from 'zx';

// Disable verbose output
$.verbose = false;

interface NpmPackage {
  name: string;
  version: string;
}

// `npm list` output types
type NpmListDependenciesLong = Record<string, NpmListDependencyLong>;

interface NpmListDependencyLong {
  version: string;
  private?: boolean;
  dependencies?: NpmListDependenciesLong;
}

interface NpmListResult {
  name: string;
  version: string;
  dependencies: NpmListDependenciesLong;
}

function getPackagesFromDependencies(dependencies: NpmListDependenciesLong): NpmPackage[] {
  return Object.entries(dependencies).flatMap<NpmPackage>(([pkgName, details]) => {
    // Skip the package in case it's not installed locally
    if (!details || !details.version) {
      return [];
    }

    const pkg: NpmPackage = {
      name: pkgName,
      version: details.version,
    };

    // This package has other dependencies
    if (details.dependencies) {
      return [pkg, ...getPackagesFromDependencies(details.dependencies)];
    }

    // This package is a private and we don't care about it
    if (details.private) {
      return [];
    }

    return [pkg];
  });
}

// Get list of dependencies
const deprecatedPackages: NpmPackage[] = [];

try {
  // Get all dependencies and flatten them
  console.log(chalk.bold('🕑 Getting list all dependencies in project...'));
  $.cwd = path.resolve('..');
  const { stdout: rawOutput } = await $`npm list --json --depth=1 --long`;
  const listed = JSON.parse(rawOutput) as NpmListResult;
  const allPackages = getPackagesFromDependencies(listed.dependencies);

  console.log(chalk.bold(`✅ Found ${allPackages.length} packages used as dependencies`));

  console.log(chalk.bold(`🕑 Checking if any dependecnies are deprecated...`));

  // Check if dependencies are deprecated
  for (const pkg of allPackages) {
    const pkgNameFormatted = chalk.bold(`${pkg.name}@${pkg.version}`);

    try {
      const { stdout: output } = await $`npm info ${pkg.name}@${pkg.version} --json`;

      const pkgInfo = JSON.parse(output) as { deprecated: boolean };

      if (pkgInfo.deprecated) {
        console.log(`  ❌ Package ${pkgNameFormatted} is deprecated`);
        deprecatedPackages.push(pkg);
      } else {
        console.log(`  ✅ Package ${pkgNameFormatted} is not deprecated`);
      }
    } catch (err) {
      const error = err as Error;
      console.log(`  ❌ Couldn't fetch the NPM information about the ${pkgNameFormatted} package`, error.message);
    }
  }

  // Log out deprecation results
  if (deprecatedPackages.length) {
    console.log('');

    if (deprecatedPackages.length === 1) {
      console.log(chalk.bold(`There is ${deprecatedPackages.length} deprecated package:`));
    } else {
      console.log(chalk.bold(`There are ${deprecatedPackages.length} deprecated packages:`));
    }

    for (const pkg of deprecatedPackages) {
      console.log(`  ${pkg.name}@${pkg.version}`);
    }

    console.log('');
    console.log(
      `Run ${chalk.bold(
        'npm why <<package-name>>@<<package-version>>',
      )} to learn why the package is included in the project`,
    );
  }
} catch (err) {
  const error = err as Error;

  console.log(error.message);
  console.log(error.stack);
  process.exit(1);
}
