export interface NpmLs {
  name: string;
  version: string;
  dependencies: { [key: string]: NpmLs };
}
