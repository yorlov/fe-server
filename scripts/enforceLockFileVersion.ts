// This check ensures that the lock file version is correct. NPM 7+ uses lock file v2.

import { promises as fs } from 'fs';
import * as process from 'process';

try {
  const lockFilePath = new URL('../package-lock.json', import.meta.url);
  const content = await fs.readFile(lockFilePath);
  const lockFile = JSON.parse(content.toString());

  if (lockFile.lockfileVersion < 2) {
    console.log(
      '❌ The package-lock.json file was created with an older version of NPM. Please ensure you are using NPM 7+ and run `npm install` again.',
    );
    process.exit(1);
  }

  console.log(`✅ The package-lock.json file was generated with the correct version of NPM.`);
} catch (err) {
  const error = err as Error;
  console.log('Error', error.message);
  process.exit(1);
}
