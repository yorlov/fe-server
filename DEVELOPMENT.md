# Development

All guides in the document requires having a proper Node and NPM versions installed in your local environment.

You can use the [`nvm`][nvm] command to install the required version of Node.

The monorepo is using:

- [`lerna` tooling][lerna]
- [Conventional Commits][conventional-commits]

You should get familiar with those tools before doing any changes in the repository or releases.

## Releasing

All packages in the monorepo are being published as independent versions into a public [NPM registry](https://registry.npmjs.org).

> ## WARNING! Don't try to release the changes from your local environment!

Use the Bitbucket Pipelines to prepare and perform the release from the `master` branch.

> ## WARNING! Before running a release read the next paragraphs!

### Pre-release step

Before we publish the packages to NPM we need bump the versions and update the `CHANGELOG.md` files first. This is done automatically when you run a release steps on Pipelines.

We will run a **Pre-release** step before publishing. This step will show you proposed versions that are based on the GIT commits' history. The commits are following the [Conventional Commits][conventional-commits] conventions.

This means that if you want to release, e.g. a new major version that is a breaking change, you need to follow the conventional commits and use e.g. this commit message:

```text
feat!: drop support for Node 12

BREAKING CHANGE: We are dropping support for Node 12. Please use Node 14.
```

## Release step

During the release step **lerna** will do all those steps:

- update changelog files
- update versions in `package.json` files
- commits the GIT changes
- create GIT tags
- push changes and tags back to a `master` branch
- deploy the new versions to public NPM

## Post-release step

As the **Post-release** step will run a smoke test to verify if the packages were successfully published into public NPM.

This check ensures we haven't accidentally published any packages into the private Atlassian repository at http://packages.atlassian.com page.

## Performing a release with Pipelines

To perform a release:

1. Merge all your changes you want to release into the `master` branch and ensure that the builds are not failing.
2. Go to [Pipelines](https://bitbucket.org/atlassianlabs/fe-server/addon/pipelines/home), from the branches' dropdown select the `master` branch.
3. Now, locate the **Pre-release** manual step and run it. The Pipeline will display you what versions will be released.
4. Next, if the proposed versions are correct, run the **Release** step.
5. Lastly, during the **Post-release** step, the Pipelines will verify if the packages were successfully published into public NPM.

## Quirks

Ensure to always keep "webpack" versions in sync across all packages. This is necessary as otherwise tests might start to fail as packages that rely on each other will resolve different versions of webpack which causes compatibility issues.

## TAV - [test-all-versions][tav]

This repository is using [`test-all-versions`][tav] NPM package to ensure webpack related modules are compatible with webpack versions 4 and 5.

Refer to the content of the `.tav.yml` file to see what webpack versions we are using to run integration tests for webpack.
The **tav** is supporting a [semver][semver] ranges to define what version of webpack we want to test.

We also have a [a **tav** fork][tav-fork] that adds support for the `latest` keyword in semver range.
Thanks to that, we can always run a test on a `latest` version of webpack or any other package when needed.

> ## WARNING! Avoiding maintaining the semver ranges will cause slowdowns in executions of the Bitbucket Pipelines.
>
> This is caused by the fact that `tav` will try to run tests on newer versions of webpack and keep running the tests on the old ones.

[nvm]: https://github.com/nvm-sh/nvm
[lerna]: https://github.com/lerna/lerna
[conventional-commits]: https://www.conventionalcommits.org/en/v1.0.0/
[tav]: https://www.npmjs.com/package/test-all-versions
[tav-fork]: https://github.com/atlassian-forks/test-all-versions/commit/005d26094443ddf38b8522a8f6a68d7e56a67aa8
[semver]: https://docs.npmjs.com/about-semantic-versioning
